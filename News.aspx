﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="News.aspx.vb" Inherits="News" %>

<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <%=fbUrl%>

  <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "MediaCentre" %>'>Media Centre</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">
           <asp:Literal ID="ltBredCum" runat="server"></asp:Literal>
                    </a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <asp:Literal ID="ltTitle" runat="server"></asp:Literal>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-12">
                    
                    <h3 class="borderedTitle">
                        <asp:Literal ID="ltTitle2" runat="server"></asp:Literal>
                    </h3>

                    <!-- Text Listing Section -->    
                   <%= News() %>
                    <!-- Text Listing Section -->

                </div>

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">
               <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="23" />
                <%--<uc1:StaticSEO runat="server" Visible="false" ID="StaticSEO1" SEOID="25" />--%>
                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                    <%-- <%= socialShare() %>--%>
                    <!-- Social Icons -->
                </div>

                <div class="col-md-6 col-sm-6">
                    
                    <!-- Pagination -->    
                  <%= PageList %>
                    <!-- Pagination -->

                </div>

            </div>
            <!-- Social and Pagination -->

        </section>

        

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

