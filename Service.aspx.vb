﻿Imports System.Data.SqlClient

Partial Class Service
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            DynamicSEO.PageID = Page.RouteData.Values("id")
            LoadContent(Page.RouteData.Values("id"))

        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List_Service where ServiceID=@ServiceID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ServiceID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString()
            hdnMasterID.Value = reader("MasterID").ToString()
            lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Service/ServiceEdit.aspx?sid=" & reader("ServiceID").ToString())

        End If
        cn.Close()
    End Sub
    Public Function GetServices() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Service where List_Service.Status=1  order by List_Service.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If reader("ServiceID").ToString() = Page.RouteData.Values("id") Then
                retstr += "<li class=""active""><a href=""" & domainName & "Service/" & reader("ServiceID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """><span></span>" & reader("Title").ToString() & "</a></li>"
            Else
                retstr += "<li><a href=""" & domainName & "Service/" & reader("ServiceID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """><span></span>" & reader("Title").ToString() & "</a></li>"
            End If

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function Gallery() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from CommonGallery where TableMasterID=@TableMasterID and TableName=@TableName"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int, 32).Value = hdnMasterID.Value
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 100).Value = "List_Service"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""content""><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""" width=""272""></div></div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
