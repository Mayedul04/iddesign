﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NewsDetails.aspx.vb" Inherits="NewsDetails" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "MediaCentre" %>'>Media Centre</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "News"%>'>News</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;"><%= title %></a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2><%= title %></h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <div class="imgHold">
                       
                        <asp:Image ID="ImgNews" runat="server" Width="555" />
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                   <asp:Literal ID="lblDetails" runat="server"></asp:Literal>

                </div>

            </div>

        </section>
           <uc1:DynamicSEO runat="server" ID="DynamicSEO" PageType="News" />
        <!-- Social Icons -->
        <asp:Literal ID="lblSocial" Visible="false" runat="server"></asp:Literal>
        <!-- Social Icons -->

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

