﻿Imports System.Data.SqlClient

Partial Class CustomControl_UserStyleGroups
    Inherits System.Web.UI.UserControl
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Function GetGroups() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM List WHERE  and Status='1' order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()


        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                While reader.Read()
                    M += "<li class=""col-md-3 col-sm-3"">"
                    M += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """></div>"
                    M += "<div class=""textContentContainer""><div class=""content""><div class=""discriptionTextContainer"">"
                    M += "<span class=""moreArrow""></span><h2>" & reader("Title").ToString() & "</h2>"
                    M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                    M += "<a class=""readMore"" href=""" & domainName & "StyleGroup/" & reader("ListID").ToString() & Utility.EncodeTitle(reader("Title"), "-") & """>Read More</a>"
                    M += "</div></div></div></li>"
                    
                End While

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
End Class
