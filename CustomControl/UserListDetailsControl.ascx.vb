﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomControl_UserListDetailsControl
    Inherits System.Web.UI.UserControl

    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim dvSql As DataView = DirectCast(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)

        'Dim drv As DataRowView = dvSql(0)
        'ltTitle.Text = drv("Title").ToString
        'ltDetails.Text = drv("BigDetails").ToString
        'Img.ImageUrl = "~/Admin/" & drv("BigImage").ToString



    End Sub

    Function GetBlogDetails() As String
        Dim M As String = ""
        Dim i As Integer = 0
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM List_NexaBlog WHERE Blogid=@Blogid and Status = '1' Order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("Blogid", Page.RouteData.Values("id"))

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                While reader.Read()
                    i = i + 1


                    Dim title As String = Utility.EncodeTitle(reader("Title"), "-")
                    Dim imgUrl As String = domainName & "Admin/" & reader("BigImage")
                    Dim desc As String = reader("SmallDetails").ToString.Replace("'", "")
                    Dim lnk As String = domainName & "Blogs-Details/" & reader("Blogid") & "/" & Utility.EncodeTitle(reader("Title"), "-")
                    Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
                    Dim googleurl As String = "https://plus.google.com/share?url=" & lnk






                    M += "<h1>" & reader("Title") & "</h1>"

                    M += " <ul class=""blogSection"">"
                    M += " <li>"
                    M += "  <article>                           "
                    M += " <div class=""post"">                             "
                    M += "<ul class=""postmetadata"">"
                    M += "<li class=""author"">                                  "
                    M += " <span>" & reader("BlogAuthor").ToString & "</span>"
                    M += "</li>"
                    M += " <li class=""date"">  "
                    M += Date.Parse(reader("BlogDate")).ToString("MMMM dd, yyyy")
                    M += " </li>"
                    M += " <li class="""">"
                    M += " Visited : " & GetVisitorCount()
                    M += " </li>"
                    M += "  </ul>"
                    M += "  </div>"

                    M += " <div class=""postDesc postDetail"">"


                    M += "<div class=""shareContainer"">"
                    M += "<div class=""shareRelativeDiv"">"
                    M += "<a href=""javascript:;"" class=""shareToggleBtn"">+</a>"
                    M += "<div class=""socialSharingLinks popupSocial"">"
                    M += "<ul class="""">"
                    M += " <li>"
                    M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png"" height=""22"" width=""60""> </a>"
                    M += "</li>   "
                    M += " <li>"
                    M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet.png"" height=""22"" width=""60""> </a>"
                    M += "</li>"
                    M += "<li>"

                    M += "<div class=""fb-like"" data-href=""" & lnk & """ data-width=""60"" data-layout=""button"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div>"


                    M += " </li>"
                    M += "<li>"

                    M += "<div class=""g-plusone"" data-href=""" & lnk & """ data-size=""tall"" data-annotation=""none""></div>"
                    M += "</li>"
                    M += " </ul>"

                    M += "</div>"
                    M += " </div>  "
                    M += " </div> "


                    If reader("BigImage").ToString <> "" Then
                        M += "  <div class=""postImg"">"
                        M += " <img src=""" & domainName & "Admin/" & reader("BigImage").ToString & """ alt=""" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                        M += " </div>"

                    End If

                    M += "   <p>"
                    M += reader("BigDetails")
                    M += "  </p>"

                    M += "  </div>"
                    M += " </article>"
                    M += "    </li>"
                    M += "  </ul>"




                    M += "  <div class=""socialSharingLinks padBtm40"">"
                    M += " <ul class=""pull-left"">"
                    M += "  <li>"
                    M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png"" height=""22"" width=""60""> </a>"
                    M += "  </li>                       "
                    M += "    <li>"
                    M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet.png"" height=""22"" width=""60""> </a>"

                    M += "  </li>"


                    M += "  <li>"
                    M += "<div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button"" data-action=""like"" data-show-faces=""true"" data-share=""false""></div>"
                    M += "  </li>"


                    M += "  <li>"
                    ' M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/googleplus.png"" height=""21"" width=""67""> </a>"
                    M += "<div class=""g-plusone"" data-href=""" & lnk & """ width=""60px"" data-size=""medium""></div>"
                    M += "  </li>"


                    M += "  </ul>"
                    M += Utility.showEditButton(Request, domainName & "Admin/A-Blog/BlogArticleEdit.aspx?aid=" + reader("Blogid").ToString())
                    M += " </div>  "


                End While




            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function GetVisitorCount() As String

        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT VisitorCount FROM List_NexaBlogVisitorCount WHERE Blogid=@Blogid"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("Blogid", Page.RouteData.Values("id"))


        Dim reader = cmd.ExecuteReader()
        If reader.HasRows = True Then
            reader.Read()
            M = reader("VisitorCount")
        End If
        reader.Close()
        con.Close()
        Return M

    End Function
End Class
