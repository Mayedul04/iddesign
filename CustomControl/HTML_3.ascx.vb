﻿
Partial Class F_HTML_HTML_2
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public H2_Class As String
    Public P_Class As String
    Public L_Class As String
    Public L_Title As String
    Public IMG_Class As String
    Dim domainName As String
    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property
    Public Property H2Class() As String
        Get
            Return H2_Class
        End Get

        Set(ByVal value As String)
            H2_Class = value
        End Set
    End Property

    Public Property PClass() As String
        Get
            Return P_Class
        End Get

        Set(ByVal value As String)
            P_Class = value
        End Set
    End Property

    Public Property LinkClass() As String
        Get
            Return L_Class
        End Get

        Set(ByVal value As String)
            L_Class = value
        End Set
    End Property


    Public Property LinkTitle() As String
        Get
            Return L_Title
        End Get

        Set(ByVal value As String)
            L_Title = value
        End Set
    End Property

    Public Property ImageClass() As String
        Get
            Return IMG_Class
        End Get

        Set(ByVal value As String)
            IMG_Class = value
        End Set
    End Property

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init


        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
    Public Function Content() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HtmlID=@HTMLID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.Int)
        cmd.Parameters("HTMLID").Value = HTMLID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            M += "<h2 class=""" & H2Class & """>" & reader("Title").ToString() & "</h2>"
            M += "<p class=""" & PClass & """>"
            M += reader("BigDetails").ToString()
            M += "<div class=""" & IMG_Class & """>"
            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """>"
            M += "</div>"
            M += Utility.showEditButton(Request, "./Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&BigImage=0&VideoLink=0&VideoCode=0")
            M += "</p>"
           

        End If
        conn.Close()
        Return M
    End Function
End Class
