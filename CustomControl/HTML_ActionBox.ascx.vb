﻿
Partial Class F_HTML_HTML_1
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public Ul_Class As String
    Public I_Class As String


    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property
    
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Function Content() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HtmlID=@HTMLID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.Int)
        cmd.Parameters("HTMLID").Value = HTMLID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            M += reader("Title").ToString() & "<br/>"
            M += "<span>" & reader("SmallDetails").ToString() & "</span>"
            M += "<br/>" & Utility.showEditButton(Request, domainName & "Admin/" & "A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&BigImage=0&VideoLink=0&VideoCode=0&ImageAltText=0&BigDetails=0")



        End If
        conn.Close()
        Return M
    End Function
End Class
