﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserBanner
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public section_name As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property SectionName() As String
        Get
            Return section_name
        End Get

        Set(ByVal value As String)
            section_name = value
        End Set
    End Property
    Function GetBanner() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM Banner WHERE SectionName=@SectionName and Status='1' order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("SectionName", SectionName)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                While reader.Read()
                    M += "<li>"
                    M += "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """>"
                    M += "<div class=""bannerTitleSection""><div class=""container""><div class=""contentContainer"">"
                    M += "<div class=""row""><div class=""col-md-10 col-md-offset-2""><p>" & reader("Title").ToString() & "</p></div></div>"
                    M += "</div></div></div>"
                    M += "<div class=""bannerContentSection""><section class=""container""><div class=""contentContainer"">"
                    M += "<ul class=""bannerImgListings"">" & GetGroups() & "</ul>"
                    M += "</div>" & Utility.showEditButton(Request, domainName & "Admin/A-Banner/TopBannerEdit.aspx?bannerId=" & reader("BannerID"))
                    M += Utility.showAddButton(Request, domainName & "Admin/A-Banner/TopBannerEdit.aspx") & "</section>"
                    M += "</div></li>"
                   
                End While

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    Public Function GetGroups() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM List WHERE  Status='1' order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()


        'Try<span class=""moreArrow""></span>
        Dim reader = cmd.ExecuteReader()
        If reader.HasRows = True Then
            While reader.Read()
                M += "<li class=""col-md-3 col-sm-3"">"
                M += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """></div>"
                M += "<div class=""textContentContainer""><div class=""content""><div class=""discriptionTextContainer"">"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                M += "<a class=""readMore"" href=""" & domainName & "StyleGroup/" & reader("ListID").ToString() & "/" & reader("Title").ToString() & """>Read More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-List/ListEdit.aspx?lid=" & reader("ListID")) & "</div></div>"
                M += "</div></li>"
            End While

        End If
        reader.Close()
        con.Close()
        Return M
        'Catch ex As Exception
        '    Return M
        'End Try

    End Function
End Class
