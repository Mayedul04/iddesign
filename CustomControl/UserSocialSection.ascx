﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserSocialSection.ascx.vb" Inherits="CustomControl_UserSocialSection" %>
 <div class="row">
            <div class="col-24 marginBtm10">
              <!-- facebook like box start here -->
                  <div class="facebookLikebox">
                    <div class="fb-like-box" data-href="https://www.facebook.com/Digital.Nexa" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
                  </div>
                <!-- facebook like box ends here -->
            </div>
            <div class="col-20 offset-1 marginBtm10">                  
                <h3 class="socialTitle ">Blog Feed</h3>
                  <div class="row">
                    <ul class="blogSmall">
                       <asp:Literal ID="ltBlogs" runat="server"></asp:Literal>                   
                  </ul>
                  </div>

                  <div class="row">
                   <ul class="socialIcons">
                      <li><a target="_blank"   href="http://www.facebook.com/Digital.Nexa"><img src='<%= domainName & "ui/media/dist/icons/fb-icon-new.png" %>' alt="Nexa Facebook"></a></li>
                      <li><a target="_blank"  href="https://twitter.com/DigitalNexa"><img src='<%= domainName & "ui/media/dist/icons/tweet-icon-new.png" %>' alt="Nexa Twitter"></a></li>
                      <li><a target="_blank"  href="http://www.linkedin.com/company/nexa-digital?trk=company_name"><img src='<%= domainName & "ui/media/dist/icons/linkedin-icon-new.png" %>' alt="Nexa linkedin"></a></li>
                     
                      <li><a target="_blank"  href="https://plus.google.com/117050596896711925493/"><img src='<%= domainName & "ui/media/dist/icons/google-plus-new.png" %>' alt="Nexa Google Plus"></a></li>
                     
                      <li><a target="_blank"  href="http://www.youtube.com/DigitalNexa/"><img src='<%= domainName & "ui/media/dist/icons/youtbe-icon.png" %>' alt="Nexa Youtube"></a></li>
                      <li><a target="_blank"  href="https://pinterest.com/digitalnexa"><img src='<%= domainName & "ui/media/dist/icons/pinterest-icon.png" %>' alt="Nexa Pinterest"></a></li>
                      <li><a target="_blank"  href="http://instagram.com/DigitalNexa/"><img src='<%= domainName & "ui/media/dist/icons/instagram-icon.png" %>' alt="Nexa Instagram"></a></li>
                      <li><a target="_blank"  href="https://www.rebelmouse.com/DigitalNexa/"><img src='<%= domainName & "ui/media/dist/icons/rebel-mouse.png" %>' alt="Nexa Rebel Mouse"></a></li>
                  </ul>
                  </div>
            </div>


                <div class="col-18 offset-2">
                    <h3 class="socialTitle">
                        <span><img src='<%= domainName & "ui/media/dist/icons/instagram-icon.png" %>' alt=""></span>
                        Nexa on Tour
                    </h3>
                     <div class="row aak_slider">
                        
                        <!-- Instagram feeds -->
                        <ul class="instagramFeeds aak_slider_list">
                            <!-- Slide -->
                            <asp:Literal ID="ltInstagramImage" runat="server"></asp:Literal>
                            <!-- Slide -->

                        </ul>
                        <!-- Instagram feeds -->
                        <div class="row">
                        <a class="prevArrow aak_left" href="javascript:;">< Previous</a>
                        <a class="nextArrow aak_right" href="javascript:;">next ></a>
                        </div>

                    </div>
                   
                    
                </div>
                <div class="col-22 offset-2">
                     <h3 class="socialTitle">TWITTER UPDATES</h3>
                    <div class="twitterFeed">
                     <a class="twitter-timeline" href="https://twitter.com/DigitalNexa" data-widget-id="427394250527895552">Tweets by @DigitalNexa</a>
                    <script>    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>
                 </div>

                   

                </div>
            </div>