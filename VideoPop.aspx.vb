﻿
Partial Class VideoPop
    Inherits System.Web.UI.Page
    Protected VideoTitle As String = ""
    Protected VideoImage As String = ""
    Protected VideoDetails As String = ""

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT *  FROM [VideoGallery] WHERE VideoGalleryID=@VideoGalleryID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("VideoGalleryID", Data.SqlDbType.Int)
        cmd.Parameters("VideoGalleryID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            VideoDetails = reader("VideoEmbedCode") & ""
            'VideoDetails &= Utility.showEditButton(Request, "Admin/A-VideoGallery/VideoGalleryItemEdit.aspx?vItemID=" & id)

        End If
        conn.Close()
    End Sub
End Class
