﻿Imports System.Data.SqlClient

Partial Class News
    Inherits System.Web.UI.Page
    Public domainName, PageList As String
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from List_News where Status=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        'If Page.RouteData.Values("category") = "General" Then
        '    cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 100).Value = "General News"
        'Else
        '    cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 100).Value = "Press Release"
        'End If
        totalRows = Math.Ceiling(cmd.ExecuteScalar / 5)
        cn.Close()
        Return totalRows
    End Function
    Public Function News() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "News/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 3, searchCriteria)
        End If
        Dim NW As String = "<ul class=""newsListings"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 5; Select * from ( select  ROW_NUMBER()   over(ORDER BY List_News.NewsID DESC) AS RowNum , NewsID ,Title, SmallDetails, SmallImage, BigImage, ReportDate, ImageAltText, Category, Link, FileUploaded from List_News where Status=1 "
        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        'If Page.RouteData.Values("category") = "General" Then
        '    cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 100).Value = "General News"
        'Else
        '    cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 100).Value = "Press Release"
        'End If
        Dim reader As SqlDataReader = cmd.ExecuteReader()

        While reader.Read
            NW = NW & "<li><div class=""row""><div class=""col-md-2 col-sm-4""><div class=""imgHold"">"
            NW += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ width=""165px"" alt=""News""/></div></div>"
            NW += "<div class=""col-md-10 col-sm-8""><h3>"
            If reader("Category") = "General News" Then
                NW += "<a  href=""" & domainName & "NewsDetails/" & reader("NewsID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & "</a>"
            Else
                If IsDBNull(reader("Link")) = False Then
                    NW += "<a target=""_blank"" href=""" & reader("Link") & """>" & reader("Title").ToString() & "</a>"
                ElseIf IsDBNull(reader("FileUploaded")) = False Then
                    NW += "<a target=""_blank"" href=""" & domainName & "Admin/" & reader("FileUploaded").ToString() & """>" & reader("Title").ToString() & "</a>"
                Else
                    NW += reader("Title").ToString()
                End If
            End If
            NW += "</h3><h6>" & reader("ReportDate").ToString() & " | " & reader("Category") & "</h6>"
            If Page.RouteData.Values("category") = "General" Then
                NW += "<p>" & reader("SmallDetails").ToString() & "</p>"
            Else
                NW += "<p>" & reader("SmallDetails").ToString() & "</p>"
            End If
            NW += "</div></div>" & Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID").ToString()) & "</li>"
        End While
        cn.Close()
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination right"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
    Public Function socialShare() As String
        Dim M As String = ""

        Dim title As String = "News"
        Dim imgUrl As String = ""
        Dim desc As String = ""
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk
        Dim linkinurl As String = "http://www.linkedin.com/shareArticle?mini=true&url=" & lnk & "& title=" & title
        Dim fblikecode As String = "http://www.facebook.com/plugins/like.php?href=" & lnk & "&width&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=1567479410148486"



        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIcons"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet-iner.png""> </a>"

        M += "  </li>"


        'M += "<li>"
        'M += " <a href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & linkinurl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')"" > "
        'M += "<img src=""" & domainName & "ui/media/dist/icons/linkdib-share.png"" alt="""">"
        'M += " </a>"

        'M += " </li>"

        M += "<li><div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button_count"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div></li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/gplus-count.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Page.RouteData.Values("category") = "General" Then
                ltBredCum.Text = "Latest News"
                ltTitle.Text = " <h2>Latest News</h2>"
                ltTitle2.Text = "Latest News"
            Else
                ltBredCum.Text = "Latest Press Release"
                ltTitle.Text = " <h2>Latest Press Release</h2>"
                ltTitle2.Text = "Latest Press Release"
            End If
        End If
        StaticSEO.SEOID = "25"
    End Sub
End Class
