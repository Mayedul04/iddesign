﻿Imports System.Data.SqlClient

Partial Class Enquiry
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Page.RouteData.Values("category").ToString() = "Product" Then
                pnlStyle.Visible = True
                LoadContent(Page.RouteData.Values("id"))
            Else
                lblCategory.Text = "Promotion"
                LoadPromoContent(Page.RouteData.Values("id"))
            End If

        End If
    End Sub
    Private Sub LoadPromoContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from SpecialOffer where SpecialOfferID=@SpecialOfferID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("SpecialOfferID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            txtProduct.Text = reader("Title").ToString()
            hdnCollectionID.Value = 0
            hdnProductID.Value = 0
            hdnStyleID.Value = 0
            hdnPromoID.Value = reader("SpecialOfferID").ToString()
            '' txtCollectionStyle.Text = GetCollection(reader("ListID").ToString()) & "(" & GetStylename(reader("StyleID").ToString()) & ")"
        End If
        cn.Close()
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List1_Child where ChildID=@ChildID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            txtProduct.Text = reader("Title").ToString()
            hdnCollectionID.Value = reader("ListID").ToString()
            hdnProductID.Value = reader("ChildID").ToString()
            hdnStyleID.Value = reader("StyleID").ToString()
            hdnPromoID.Value = 0
            txtCollectionStyle.Text = GetCollection(reader("ListID").ToString()) & "(" & GetStylename(reader("StyleID").ToString()) & ")"
        End If
        cn.Close()
    End Sub
    Protected Sub btnenq_Click(sender As Object, e As EventArgs) Handles btnenq.Click
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now
            Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">First Name</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail1.Text & "</td></tr>"
            If Page.RouteData.Values("category").ToString() = "Product" Then
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Product Name</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtProduct.Text & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Collection(Style)</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtCollectionStyle.Text & "</td></tr>"
            Else
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Promotion</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtProduct.Text & "</td></tr>"
                
            End If
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Message</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtmsg.Text & "</td></tr>"
            msg += "</table></td></tr></table>"
            msg = GetRegistrationInfoMessage()
            If sdsEnq.Insert > 0 Then
                If Page.RouteData.Values("category").ToString() = "Product" Then
                    Utility.SendHTTPRequestToSendgrid(txtName.Text, txtEmail1.Text, "headoffice@iddesign.ae;headoffice@iddesignuae.com", "zainu@iddesign.ae", "mayedul@digitalnexa.com", "Product Enquery", msg)
                    '  Utility.SendMail(txtName.Text, txtEmail1.Text, "mayedul.islam@wvss.net", "", "", "Product Enquery", msg)
                Else
                    ' Utility.SendMail(txtName.Text, txtEmail1.Text, "mayedul.islam@wvss.net", "", "", "Promotion Enquery", msg)
                    Utility.SendHTTPRequestToSendgrid(txtName.Text, txtEmail1.Text, "headoffice@iddesign.ae;headoffice@iddesignuae.com", "zainu@iddesign.ae", "mayedul@digitalnexa.com", "Product Enquery", msg)
                End If

                Response.Redirect(domainName & "Thanks")
            Else
            End If
        End If
    End Sub
    Private Function GetRegistrationInfoMessage() As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
"<html lang='en'>" & _
"<head>" & _
"    <meta charset='utf-8'>" & _
"    <title>Royal Rose</title>" & _
"    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" & _
"    <body style='margin:0px; padding:0px;'>" & _
"        <table bgcolor='#ac7e14' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>" & _
"            <tr>" & _
"                <td style='padding:2px 0;'>" & _
"                    <table bgcolor='#fff' width='610' cellspacing='0' cellpadding='0' border='0' align='center' style='background:#ffffff;'>" & _
"                        <tr>" & _
"                            <td align='center'>" & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>&nbsp;</p>" & _
"                                <table width='90%' cellspacing='0' cellpadding='0' align='center' border='1' style='border:1px solid #eee; border-collapse: collapse; margin:0 0 20px;'>" & _
"                                    <tr bgcolor='#eee'>" & _
"                                        <td colspan='2'>" & _
"                          <div style=""text-align: center;""><a href=""http://" & Request.Url.Host & """><img src=""http://" & Request.Url.Host & "/ui/media/dist/logo/logo.png"" width=""62px""  alt=''></a></div>" & _
"                                            <h3 style='font-family:arial; color:#490109; font-size:24px; text-align:center; margin:10px 0;'>Contact Details</h3>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                   <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtName.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                   <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Email Address</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtEmail1.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Product Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtProduct.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Collection(Style)</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtCollectionStyle.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Promotion</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtPhone.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                                             <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Message</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtmsg.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                </table>                                " & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>© Copyright " & Date.Now.Year & ". All rights reserved by ~<a href='http://" & Request.Url.Host & "'>IDdesign UAE</a>~.</p>" & _
"                            </td>" & _
"                        </tr>  " & _
"                    </table>" & _
"                </td>" & _
"            </tr>" & _
"        </table>" & _
"    </body>" & _
"</html>"



        Return retVal
    End Function
    Public Function GetStylename(ByVal id As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from List where ListID=@ListID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += reader("Title").ToString()
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function GetCollection(ByVal id As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from List1 where ListID=@ListID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += reader("Title").ToString()
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
