﻿
Partial Class F_SEO_DynamicSEO
    Inherits System.Web.UI.UserControl

    Private m_PageType As String = "HTML"
    Private m_PageID As String = "1"

    Public Property PageType() As String
        Get
            Return m_PageType
        End Get
        Set(value As String)
            m_PageType = Value
        End Set
    End Property

    Public Property PageID() As String
        Get
            Return m_PageID
        End Get
        Set(value As String)
            m_PageID = Value
        End Set
    End Property


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'm_PageID = Page.RouteData.Values("id")

        lblSEO.Text = Utility.SEOFromDB(Page, m_PageType, m_PageID, Request)
    End Sub
End Class
