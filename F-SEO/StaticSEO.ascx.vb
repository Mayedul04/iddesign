﻿
Partial Class F_SEO_StaticSEO
    Inherits System.Web.UI.UserControl
    Private m_SEOID As String = "1"

    Public Property SEOID() As String
        Get
            Return m_SEOID
        End Get
        Set(value As String)
            m_SEOID = value
        End Set
    End Property


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lblSEO.Text = Utility.SEOFromDB(Page, m_SEOID, Request)
    End Sub
End Class
