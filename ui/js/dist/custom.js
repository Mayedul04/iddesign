// 
// Custom Js for custom functunality
// ===================================================

$(document).ready(function(){

$('#enquirenow').fancybox({
    height : '305px',
    width : '600px'
});

// Flex Slider
// ===================================================


// For Main Slider with Paging
// ===================================================
$('.mainBannerSection').flexslider({
animation: "slide",
directionNav: true,
controlNav: false,


// options for Paging
start:function(slider){
    $(".slide-current-slide").text(slider.currentSlide+1);
    $(".slide-total-slides").text("/ "+slider.count)
},
before:function(slider){
    $(".slide-current-slide").text(slider.animatingTo+1)
}


});

// For Main Slider with Paging
// ===================================================


// For Produst List Slider
// ===================================================
$('.productPreviewSlider').flexslider({
animation: "slide",
directionNav: true,
controlNav: false
});

// For Produst List Slider
// ===================================================

 
$('.productPreviewSlider').each(function(){
  var phuntro = $(this).find('ul.slides li').length;
  if (phuntro <= 3) {
    $(this).find('.flex-direction-nav').hide();
  }
})



// For Product Detail
// ===================================================

$('.productThumbimgSection').flexslider({
  animation: "slide",
  controlNav: false,
  directionNav: true,
  animationLoop: true,
  slideshow: false,
  itemWidth: 141,
  asNavFor: '.productMainimgSection',
  useCSS: false
});
 
$('.productMainimgSection').flexslider({
  animation: "slide",
  controlNav: false,
  animationLoop: true,
  slideshow: false,
  sync: ".productThumbimgSection"
});

// For Product Detail
// ===================================================



// For New Arrival Slider
// ===================================================

if($(window).width() < 480) {

// Mobile

$('.newArrivalSlider').flexslider({
animation: "slide",
animationLoop: true,
itemWidth: 320,
maxItems: 1,
minItems: 1, 
directionNav: true,
controlNav: false,
slideshow: true,
move: 1,
useCSS: false,
pauseOnHover: true
});


} else if ($(window).width() < 1024 && $(window).width() >= 768) {

// tab

//alert("tab")

$('.newArrivalSlider').flexslider({
animation: "slide",
animationLoop: true,
itemWidth: 320,
maxItems: 3,
minItems: 3, 
directionNav: true,
controlNav: false,
slideshow: true,
move: 1,
useCSS: false,
pauseOnHover: true
});

} else {

// Destop

$('.newArrivalSlider').flexslider({
animation: "slide",
animationLoop: true,
itemWidth: 320,
maxItems: 4,
minItems: 4, 
directionNav: true,
controlNav: false,
slideshow: true,
move: 1,
useCSS: false,
pauseOnHover: true
});

}

// For New Arrival Slider
// ===================================================


// Flex Slider
// ===================================================


// Custom Styling For Select Box
// ===================================================
$("select").uniform();

// Custom Styling For Select Box
// ===================================================

$('.fancybox').fancybox();

$('.videoFancyIframe').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 570,
      'scrolling'            : 'no'

});



// For Location Tabing
// ===================================================

$(".tabContentContainer > div").hide();
$(".tabContentContainer div:first-child").show();

$("ul.tabNavigation li").click(function(){

    var $this = $(this)

    if($(this).attr("class") != "active")

    {

      var $index = $this.index(); $index++;
      $this.addClass("active").siblings().removeClass("active");

      $(".tabContentContainer > div").hide();
      $(".tabContentContainer > div.content-"+$index).fadeIn();

    } 

})

// For Location Tabing
// ===================================================



$("ul.bannerImgListings li").hover(function(){

	$(this).find(".discriptionTextContainer").animate({

		'bottom' : -2

	})

}, function(){

	$(this).find(".discriptionTextContainer").animate({

		'bottom' : -163

	})

})




$(".newArrivalSlider ul li").hover(function(){

	$(this).find(".discriptionTextContainer").animate({

		'bottom' : -2

	})

}, function(){

	$(this).find(".discriptionTextContainer").animate({

		'bottom' : -146

	})

})


$(".navbar ul.nav > li").hover(function(){

    $(this).find("ul.dropDownList").stop().slideDown('fast');

}, function(){

    $(this).find("ul.dropDownList").stop().slideUp('fast');

})




});
    $('#carousel').carousel();

    //SocialSlider and popupScreen
    var socialSlider = $('.socialSlider');
    var popupScreen = $('.popupScreen');

    $('.socialBtn').on('click', function(){
        popupScreen.fadeOut();
        socialSlider.fadeIn();       
    });

    $('.popupScreenToggle').on('click', function(){
        socialSlider.fadeOut();
        $(this).next('.popupScreen').fadeIn();       
    });

    $('.closeBtn').on('click', function(){
       socialSlider.fadeOut();
       popupScreen.fadeOut();       
    });