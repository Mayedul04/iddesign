﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Gallery.aspx.vb" Inherits="Gallery" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%=fbUrl%>
    <%= fbTitle %>
  <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "MediaCentre" %>'>Media Centre</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "Photogallery" %>'>Photo Gallery</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;"><%= title%></a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2><%= title %></h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">
                    
                <!-- IMage Block Listings -->
                <%= Gallery() %>
                <!-- IMage Block Listings -->

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">
                <uc1:DynamicSEO runat="server" ID="DynamicSEO" PageType="Gallery" />
                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                    <%-- <%= socialShare %>--%>
                    <!-- Social Icons -->
                </div>

                <div class="col-md-6 col-sm-6">
                    
                    <!-- Pagination -->    
                    <%= PageList %>
                    <!-- Pagination -->

                </div>

            </div>
            <!-- Social and Pagination -->

        </section>

        

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

