﻿
Partial Class Search
    Inherits System.Web.UI.Page
    Public domainName, PageList As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtSearch.Text = RouteData.Values("q")
            If txtSearch.Text = "" Then
                lblData.Text = "<h2>No Result Found</h2>"
                Exit Sub
            End If
            lblData.Text = ShowData()
            If lblData.Text.Trim.Length > 6 Then
                lblData.Text = "<h2>Search Result</h2><ul  class=""textListings"">" & lblData.Text & "</ul>"
            Else
                lblData.Text = "<h2>No Result Found</h2>"
            End If

        End If

    End Sub


    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Response.Redirect(Session("domainName") & "Search/" & txtSearch.Text.Trim, True)
    End Sub

    Public Function ShowData() As String
        Dim str As String = ""
        Dim rowCount As Integer = 0
        Dim inStream As IO.StreamReader
        Dim webRequest As Net.WebRequest
        Dim webresponse As Net.WebResponse
        Dim currentPage As String = 1
        If RouteData.Values("page") <> "" Then
            currentPage = RouteData.Values("page")
        End If
        If currentPage > 1 Then
            currentPage = ((CInt(currentPage) * 20) - 20).ToString()
        End If
        Dim searchCriteria As String = "http://www.google.com/search?q=" + txtSearch.Text + "&hl=en&start=" + currentPage + "&num=20&output=xml&client=google-csbe&cx=001915627491718564308:xlja2mpvgei"

        webRequest = Net.WebRequest.Create(searchCriteria)
        webresponse = webRequest.GetResponse()
        inStream = New IO.StreamReader(webresponse.GetResponseStream())
        Dim sb As String = inStream.ReadToEnd().ToString()


        Dim m1 As MatchCollection = Regex.Matches(sb, "<M>(.*?)</M>", RegexOptions.Singleline)
        Dim noOfRecord As Integer = 0
        For Each m As Match In m1
            Dim value As String = m.Groups(1).Value
            noOfRecord = value
            'noOfRec.Text = noOfRecord.ToString()
        Next

        If noOfRecord > 0 Then
            'pnlResultFound.Visible = True
            'pnlNotResult.Visible = False
            'pnlAllResult.Visible = True
            Dim time As MatchCollection = Regex.Matches(sb, "<TM>(.*?)</TM>", RegexOptions.Singleline)
            Dim totalTime As Double = 0
            For Each m As Match In time
                Dim value As String = m.Groups(1).Value
                totalTime = CDbl(value)
                'noOfRec.Text = noOfRec.Text + "Total Time: " + totalTime.ToString().Substring(0, 4)
            Next



            Dim m2 As MatchCollection = Regex.Matches(sb, "<U>(.*?)</U>", RegexOptions.Singleline)
            Dim allURL As New List(Of String)()
            For Each m As Match In m2
                Dim value As String = m.Groups(1).Value
                allURL.Add(value)

            Next


            Dim m3 As MatchCollection = Regex.Matches(sb, "<T>(.*?)</T>", RegexOptions.Singleline)
            Dim allTitle As New List(Of String)()
            For Each m As Match In m3
                Dim value As String = m.Groups(1).Value
                allTitle.Add(value)

            Next


            Dim m4 As MatchCollection = Regex.Matches(sb, "<S>(.*?)</S>", RegexOptions.Singleline)
            Dim allSub As New List(Of String)()
            For Each m As Match In m4
                Dim value As String = m.Groups(1).Value
                allSub.Add(value)

            Next

            Dim multiDimensionalArray1 As String(,) = New String(allURL.Count, 3) {}

            For i As Integer = 0 To allURL.Count - 1
                multiDimensionalArray1(i, 0) = allURL(i).ToString()
            Next

            For i As Integer = 0 To allTitle.Count - 1
                multiDimensionalArray1(i, 1) = allTitle(i).ToString()
            Next

            For i As Integer = 0 To allSub.Count - 1
                multiDimensionalArray1(i, 2) = allSub(i).ToString()
            Next

            For k As Integer = 0 To allURL.Count - 1
                Dim link = Server.HtmlDecode(multiDimensionalArray1(k, 0))
                'str += "<p><a target=""_blank"" href=""" & multiDimensionalArray1(k, 0).Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & """>" & multiDimensionalArray1(k, 1).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & "</a> - " & multiDimensionalArray1(k, 2).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&").Replace("<br>", " ") & "</p>"
                str &= "<li>" & _
                      "  <h2 ><a href=""" & link & """>" & multiDimensionalArray1(k, 1).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & "</a></h2>" & _
                      "  <p>" & Server.HtmlDecode(multiDimensionalArray1(k, 2).ToString()).Replace("<br>", " ") & "</p>" & _
                      "  <a href=""" & link & """ class=""button"">Read More</a>" & _
                      "</li>"
            Next

            Dim page As String = If(String.IsNullOrEmpty(RouteData.Values("page")), "1", RouteData.Values("page"))
            Dim iPage As Int32 = 0
            If Not Integer.TryParse(page, iPage) Then
                iPage = 1
            End If
            Dim paging As String = New Pager(iPage, CInt(Math.Floor(noOfRecord / 20) + 1), 9, Session("domainName") & "search/" + txtSearch.Text + "/").GetPager
            lbPagingBottom.Text = "&nbsp;" + paging
            'lbPagingTop.Text = "&nbsp;" + paging
        Else
            'pnlResultFound.Visible = False
            'pnlNotResult.Visible = True
            'pnlAllResult.Visible = False
        End If
        Return str
    End Function
End Class
