﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AboutUs.aspx.vb" Inherits="AboutUs" %>

<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">About Us</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>About Us</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <div class="imgHold">
                       
         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="10" />
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                   <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt"  HTMLID="10"/>

                </div>

            </div>

        </section>
<uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="13"/>
        <!-- Social Icons -->
        <asp:Literal ID="lblSocial" Visible="false" runat="server"></asp:Literal>
        <!-- Social Icons -->

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

