/****** Object:  Database [nexacms]    Script Date: 8/7/2014 1:25:53 PM ******/
CREATE DATABASE [nexacms] ON  PRIMARY 
( NAME = N'IdDesign', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\nexacms.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'nexacms_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\nexacms_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [nexacms] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [nexacms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [nexacms] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [nexacms] SET ARITHABORT OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [nexacms] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [nexacms] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [nexacms] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [nexacms] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [nexacms] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [nexacms] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [nexacms] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [nexacms] SET  DISABLE_BROKER 
GO
ALTER DATABASE [nexacms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [nexacms] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [nexacms] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [nexacms] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [nexacms] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [nexacms] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [nexacms] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [nexacms] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [nexacms] SET  MULTI_USER 
GO
ALTER DATABASE [nexacms] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [nexacms] SET DB_CHAINING OFF 
GO
/****** Object:  Table [dbo].[AdminPanelLogin]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminPanelLogin](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[PS_1] [nvarchar](100) NULL,
	[UN_1] [nvarchar](100) NULL,
	[Title] [nvarchar](300) NULL,
	[Email] [nvarchar](200) NULL,
	[Role] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_AdminPanelLogin_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AlertMessage]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertMessage](
	[AlertMessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageFrom] [nvarchar](200) NULL,
	[FromEmail] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[LastUpdated] [datetime] NULL,
	[HasRead] [bit] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_AlertMessage] PRIMARY KEY CLUSTERED 
(
	[AlertMessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Banner]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner](
	[BannerID] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [varchar](50) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](500) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[BigDetails] [nvarchar](max) NULL,
	[Lang] [nvarchar](10) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[BannerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BlogArticles]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BlogArticles](
	[ArticleID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](400) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_BlogArticles] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Career]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Career](
	[CareerID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[PublishedDate] [datetime] NULL,
	[Deadline] [datetime] NULL,
	[PublishedBy] [nvarchar](50) NULL,
	[JobCode] [nvarchar](100) NULL,
 CONSTRAINT [PK_Career] PRIMARY KEY CLUSTERED 
(
	[CareerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CareerApplicant]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerApplicant](
	[CareerApplicantID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Address] [nvarchar](max) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[CV] [nvarchar](500) NULL,
	[LastUpdated] [date] NULL,
	[CareerMasterID] [int] NULL,
	[CoverLetter] [nvarchar](500) NULL,
	[DesiredPosition] [nvarchar](500) NULL,
	[Subject] [nvarchar](500) NULL,
	[Photo] [nvarchar](500) NULL,
	[AttachOthers] [nvarchar](500) NULL,
	[Message] [nvarchar](max) NULL,
	[JobCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_CareerApplicant] PRIMARY KEY CLUSTERED 
(
	[CareerApplicantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommonGallery]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommonGallery](
	[CommonGalleryID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[TableName] [varchar](25) NULL,
	[TableMasterID] [int] NULL,
 CONSTRAINT [PK_CommonGallery] PRIMARY KEY CLUSTERED 
(
	[CommonGalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ContactID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](30) NULL,
	[Mobile] [nvarchar](30) NULL,
	[Address] [nvarchar](300) NULL,
	[ContactFor] [nvarchar](100) NULL,
	[Subject] [nvarchar](500) NULL,
	[Message] [nvarchar](max) NULL,
	[ContactDate] [datetime] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CountryList]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryList](
	[CountryCode] [nvarchar](20) NOT NULL,
	[CountryName] [nvarchar](50) NULL,
	[SordIndex] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_CountryList] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Gallery]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gallery](
	[GalleryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_Gallery] PRIMARY KEY CLUSTERED 
(
	[GalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GalleryItem]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GalleryItem](
	[GalleryItemID] [int] IDENTITY(1,1) NOT NULL,
	[GalleryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_GalleryItem] PRIMARY KEY CLUSTERED 
(
	[GalleryItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HTML]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HTML](
	[HtmlID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[LastUpdated] [datetime] NULL,
	[LinkTitle] [nvarchar](300) NULL,
	[VideoLink] [nvarchar](400) NULL,
	[VideoCode] [nvarchar](max) NULL,
	[SmallImageWidth] [smallint] NULL,
	[SmallImageHeight] [smallint] NULL,
	[BigImageWidth] [smallint] NULL,
	[BigImageHeight] [smallint] NULL,
	[VideoWidth] [smallint] NULL,
	[VideoHeight] [smallint] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_HTML] PRIMARY KEY CLUSTERED 
(
	[HtmlID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Instagram]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Instagram](
	[ImportID] [int] IDENTITY(1,1) NOT NULL,
	[ImageID] [int] NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[OriginalLink] [varchar](100) NULL,
 CONSTRAINT [PK_Instagram] PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InstagramImage]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InstagramImage](
	[ImageID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[OriginalLink] [varchar](100) NULL,
 CONSTRAINT [PK_InstagramImage] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Languages](
	[Lang] [varchar](10) NOT NULL,
	[LangFullName] [nvarchar](50) NULL,
	[SortIndex] [smallint] NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Lang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](400) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Event]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Event_Details]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[List_Event_Details](
	[EventDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Time] [nvarchar](200) NULL,
 CONSTRAINT [PK_List_Event_Details] PRIMARY KEY CLUSTERED 
(
	[EventDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[List_News]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[ReportDate] [datetime] NULL,
 CONSTRAINT [PK_List_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Partners]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Partners](
	[PartnerID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](30) NULL,
	[Mobile] [nvarchar](30) NULL,
	[Fax] [nvarchar](30) NULL,
	[Address] [nvarchar](500) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Partners] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Profile]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Profile](
	[ProfileID] [int] IDENTITY(1,1) NOT NULL,
	[PersonName] [nvarchar](300) NULL,
	[Designation] [nvarchar](300) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[LinkedIn] [nvarchar](500) NULL,
	[Facebook] [nvarchar](500) NULL,
	[Twitter] [nvarchar](500) NULL,
	[Mobile] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_List_Profile] PRIMARY KEY CLUSTERED 
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Service]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Service](
	[ServiceID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Service] PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Testimonial]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Testimonial](
	[TestimonialID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[WrittenBy] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Country] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Featured] [bit] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Testimonial] PRIMARY KEY CLUSTERED 
(
	[TestimonialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List1]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List1](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Link] [nvarchar](500) NULL,
	[Featured] [bit] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List1] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List1_Child]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List1_Child](
	[ChildID] [int] IDENTITY(1,1) NOT NULL,
	[ListID] [int] NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Link] [nvarchar](500) NULL,
	[Featured] [bit] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List1_Child] PRIMARY KEY CLUSTERED 
(
	[ChildID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsletterSubscription]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsletterSubscription](
	[NewSubID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](20) NULL,
	[Mobile] [nvarchar](20) NULL,
	[Comments] [nvarchar](max) NULL,
	[SubscriptionDate] [datetime] NULL,
 CONSTRAINT [PK_NewsletterSubscription] PRIMARY KEY CLUSTERED 
(
	[NewSubID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reg_Member_]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg_Member_](
	[RegID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](20) NULL,
	[Mobile] [nvarchar](20) NULL,
	[Organization] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Address] [nvarchar](500) NULL,
	[Comments] [nvarchar](max) NULL,
	[RegistrationDate] [datetime] NULL,
	[UN_1] [nvarchar](100) NULL,
	[PS_1] [nvarchar](100) NULL,
 CONSTRAINT [PK_Reg_Member_] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SEO]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SEO](
	[SEOID] [bigint] IDENTITY(1,1) NOT NULL,
	[PageType] [nvarchar](100) NULL,
	[PageID] [varchar](20) NULL,
	[SEOTitle] [nvarchar](200) NULL,
	[SEODescription] [nvarchar](max) NULL,
	[SEOKeyWord] [nvarchar](max) NULL,
	[SEORobot] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Lang] [nvarchar](10) NULL,
 CONSTRAINT [PK_SEO] PRIMARY KEY CLUSTERED 
(
	[SEOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Settings_Dimention]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_Dimention](
	[DimentionID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](100) NULL,
	[Category] [nvarchar](400) NULL,
	[SmallImageWidth] [smallint] NULL,
	[SmallImageHeight] [smallint] NULL,
	[BigImageWidth] [smallint] NULL,
	[BigImageHeight] [smallint] NULL,
	[VideoWidth] [smallint] NULL,
	[VideoHeight] [smallint] NULL,
 CONSTRAINT [PK_Settings_Dimention] PRIMARY KEY CLUSTERED 
(
	[DimentionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings_Module]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings_Module](
	[SModID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](100) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Settings_Module] PRIMARY KEY CLUSTERED 
(
	[SModID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SpecialOffer]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpecialOffer](
	[SpecialOfferID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [varchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_SpecialOffer] PRIMARY KEY CLUSTERED 
(
	[SpecialOfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoGallery]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoGallery](
	[VideoGalleryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [varchar](150) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[VideoOriginalURL] [varchar](150) NULL,
	[VideoImageURL] [varchar](150) NULL,
	[VideoEmbedCode] [varchar](1000) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_VideoGallery] PRIMARY KEY CLUSTERED 
(
	[VideoGalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ContactAndNewsletterCombineResult]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ContactAndNewsletterCombineResult]
AS
SELECT        COUNT(dbo.Contact.ContactID) AS ContactsInADay, COUNT(dbo.NewsletterSubscription.NewSubID) AS SubscriptionInADay, 
                         ISNULL(dbo.NewsletterSubscription.SubscriptionDate, dbo.Contact.ContactDate) AS theDate
FROM            dbo.Contact FULL OUTER JOIN
                         dbo.NewsletterSubscription ON dbo.Contact.ContactDate = dbo.NewsletterSubscription.SubscriptionDate
GROUP BY dbo.Contact.ContactID, dbo.Contact.ContactDate, dbo.NewsletterSubscription.NewSubID, dbo.NewsletterSubscription.SubscriptionDate

GO
/****** Object:  View [dbo].[View1]    Script Date: 8/7/2014 1:25:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View1]
AS
SELECT        GroupContacts.totalContact, GroupSubs.totalSubs, ISNULL(GroupContacts.ContactDate, GroupSubs.SubscriptionDate) AS Expr1
FROM            (SELECT        COUNT(ContactID) AS totalContact, ContactDate
                          FROM            dbo.Contact
                          GROUP BY ContactDate) AS GroupContacts FULL OUTER JOIN
                             (SELECT        COUNT(NewSubID) AS totalSubs, SubscriptionDate
                               FROM            dbo.NewsletterSubscription
                               GROUP BY SubscriptionDate) AS GroupSubs ON GroupContacts.ContactDate = GroupSubs.SubscriptionDate

GO
SET IDENTITY_INSERT [dbo].[AdminPanelLogin] ON 

INSERT [dbo].[AdminPanelLogin] ([UserID], [PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (1, N'12345', N'admin', N'Administrator01', N'admin@admin.com', N'Admin', CAST(0x0000A2FE01369FEC AS DateTime), N'1', 1)
INSERT [dbo].[AdminPanelLogin] ([UserID], [PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (2, N'admin', N'sa', N'SA WVSS', N'admin@wvss.net', N'admin', CAST(0x0000A08F0117D260 AS DateTime), N'admin', 1)
SET IDENTITY_INSERT [dbo].[AdminPanelLogin] OFF
SET IDENTITY_INSERT [dbo].[Banner] ON 

INSERT [dbo].[Banner] ([BannerID], [SectionName], [Title], [SmallDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [SortIndex], [Status], [LastUpdated], [BigDetails], [Lang]) VALUES (11, N'Home', N'Web Design and Development Company', N'Website Design and Development..', N'Content/Web-Design-and-Development-Small1210201315192.jpg', N'Content/Web-Design-and-Development-Big1210201315192.jpg', N'Web Design Company Dubai and Web Development Dubai', NULL, 1, 1, CAST(0x0000A2FD002B6EC0 AS DateTime), N'<p class="bannerDesc">Ranked No. 1 in the UAE for<strong> Web Design</strong>, <strong>Website Development</strong> and <strong>Search Engine Optimization (SEO)&nbsp;</strong>by&nbsp;<strong>TopSEOs.com,</strong>&nbsp;Nexa have been providing high quality website services since 2005. &nbsp;Our <strong>fully&nbsp;</strong><strong>in-house</strong> team of experts will provide you with a professional website for your business. &nbsp;From large international clients to start-up businesses with big ideas, Nexa can provide your organization with a full range of website design &amp; development services.</p>', N'en')
SET IDENTITY_INSERT [dbo].[Banner] OFF
SET IDENTITY_INSERT [dbo].[BlogArticles] ON 

INSERT [dbo].[BlogArticles] ([ArticleID], [Category], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, N'All', N'Lorem Ipsum', N'Lorem Ipsum Lorem Ipsum', N'<p>Lorem IpsumLorem IpsumLorem Ipsum</p>

<p>Lorem IpsumLorem IpsumLorem IpsumLorem Ipsum</p>
', N'Content/Lorem-Ipsum-Small782014131950.jpg', N'Content/Lorem-Ipsum-Big782014131950.jpg', N'Lorem Ipsum', N'http://lipsum.lipsum.com/', 0, 1, 1, CAST(0x0000A38000DBAE48 AS DateTime), 1, N'en')
SET IDENTITY_INSERT [dbo].[BlogArticles] OFF
SET IDENTITY_INSERT [dbo].[Career] ON 

INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (1, N'Career Opportunity 1', N'Career with us for the post of "Lorem imsum". There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.', N'<p>Responsibilities:</p>
<ul>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
    <li>Nam molestie consectetur magna, a facilisis augue luctus eget.</li>
    <li>Sed ullamcorper tellus sed eros hendrerit ut posuere mauris hendrerit.</li>
    <li>Maecenas sodales scelerisque urna, et euismod diam tincidunt a.</li>
    <li>Maecenas aliquet felis vitae dui laoreet ac pellentesque lectus eleifend.</li>
    <li>Fusce eleifend tellus at mauris auctor fermentum.</li>
</ul>
<p>Benefits:</p>
<ul>
    <li>Etiam ornare ante eu tortor aliquam in bibendum tellus hendrerit.</li>
    <li>Praesent mattis mattis arcu, dapibus iaculis urna dictum sed.</li>
    <li>Vestibulum lobortis aliquet lacus, convallis lobortis nulla placerat eu.</li>
    <li>Donec iaculis faucibus dolor, vel facilisis erat rhoncus id.</li>
    <li>Donec eget enim id turpis consequat vestibulum.</li>
</ul>
<p>&nbsp;</p>', N'Content/CareerSmall2372012103630.jpg', N'Content/CareerBig2372012103630.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A2FD00E5EF48 AS DateTime), 1, N'en', CAST(0x0000A2E300000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'test', N'12345')
INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (2, N'Career Opportunity 1', N'Career with us for the post of "Lorem imsum". There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.', N'<p>Responsibilities:</p>
<ul>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
    <li>Nam molestie consectetur magna, a facilisis augue luctus eget.</li>
    <li>Sed ullamcorper tellus sed eros hendrerit ut posuere mauris hendrerit.</li>
    <li>Maecenas sodales scelerisque urna, et euismod diam tincidunt a.</li>
    <li>Maecenas aliquet felis vitae dui laoreet ac pellentesque lectus eleifend.</li>
    <li>Fusce eleifend tellus at mauris auctor fermentum.</li>
</ul>
<p>Benefits:</p>
<ul>
    <li>Etiam ornare ante eu tortor aliquam in bibendum tellus hendrerit.</li>
    <li>Praesent mattis mattis arcu, dapibus iaculis urna dictum sed.</li>
    <li>Vestibulum lobortis aliquet lacus, convallis lobortis nulla placerat eu.</li>
    <li>Donec iaculis faucibus dolor, vel facilisis erat rhoncus id.</li>
    <li>Donec eget enim id turpis consequat vestibulum.</li>
</ul>
<p>&nbsp;</p>', N'Content/CareerSmall2372012103630.jpg', N'Content/CareerBig2372012103630.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A2FD00E61144 AS DateTime), 1, N'ar', CAST(0x0000A2E300000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'test', N'12345')
INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (3, N'Career Opportunity 2', N'this is a test career', N'<p>&nbsp;this is a test career</p>
', NULL, NULL, NULL, NULL, 1, 2, 1, CAST(0x0000A31400D60254 AS DateTime), 2, N'en', CAST(0x0000A2E400000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'Admin', N'23456')
SET IDENTITY_INSERT [dbo].[Career] OFF
SET IDENTITY_INSERT [dbo].[CareerApplicant] ON 

INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (2, N'Mazhar Abbas', NULL, N'mazhar.abbas376@gmail.com', N'00971566321591', N'Content/Mazhar-Abbas-CV163201464427.doc', CAST(0x4B380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Mazhar-Abbas-Photo163201464426.jpg', NULL, N'I''m an experienced web developer having plus 2 years professional experience. I have great expertise in PHP, Codeigniter, Wordpress, JQUERY, Javascript, Ajax and Mysql. I am very interested to work in your organisation. ', N'web developer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (3, N'Manimaran', NULL, N'manima198326@gmail.com', N'971502872971', N'Content/Manimaran-CV163201491850.doc', CAST(0x4B380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Manimaran-Photo163201491850.jpg', NULL, N'I have 7 Years Experience in Software Development . Currently Looking for a Job', N'Software Engineer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (4, N'Sadir Chandroth', NULL, N'sadirmuhammed@gmail.com', N'00971561952579', N'Content/Sadir-Chandroth-CV193201464333.doc', CAST(0x4E380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Sadir-Chandroth-Photo193201464333.jpg', N'Content/Sadir-Chandroth-OthersFile193201464333.doc', N'To succeed in an environment of growth and excellence and earn a job which provides me job   Satisfaction and self development and help me achieve personal as well as organization goals', N'Sales , Accountant , Cashier')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (5, N'Zuhaib Bader', NULL, N'zuhaibbader@hotmail.com', N'+971566169799', N'Content/Zuhaib-Bader-CV193201413158.pdf', CAST(0x4E380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Zuhaib-Bader-Photo193201413158.jpg', NULL, N'Dear Sir / Madam,

I am Looking for Job as an assistant accountant or in administration Department.

Currently i am working as an accountant in a cruising company in Abu Dhabi and wish to in you esteem Organisation.

Regards,

Zuhaib Bader', N'Finance & Accounts')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (6, N'Trevor Kuniski', NULL, N'tkuniski@gmail.com', N'+971567335763', N'Content/Trevor-Kuniski-CV233201493620.doc', CAST(0x52380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Trevor-Kuniski-Photo233201493620.jpg', NULL, NULL, N'1')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (7, N'Manimaran', NULL, N'manima198326@gmail.com', N'971502872971', N'Content/Manimaran-CV24320147330.doc', CAST(0x53380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Manimaran-Photo24320147330.jpg', NULL, N'I have 7 years experience in Software Development', N'Software Engineer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (8, N'NAVEED', NULL, N'ksiraji@gmail.com', N'00971569128463', N'Content/NAVEED-CV243201419340.docx', CAST(0x53380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/NAVEED-Photo243201419340.jpg', NULL, N'Hello Dear Hiring Manager, 
 
Accountant, with 5 years experience, looking for a suitable opening in finance / accounts. Can join immediately.

Accountant / Admin , B.COM, DICA, CPA (Computerized Professional Accountant) With Tally ERP.9, Peachtree, QuickBooks and Finalization of financial statements and analysis. Advanced expertise of Microsoft Office.
', N'Accountant/ Assistant Accountant')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (9, N'MR JANAK ANANDBHAI KIDEEYA', NULL, N'janak_kideeya@yahoo.com', N'261331938042', N'Content/MR-JANAK-ANANDBHAI-KIDEEYA-CV2532014124924.doc', CAST(0x54380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/MR-JANAK-ANANDBHAI-KIDEEYA-Photo2532014124924.jpg', NULL, N'i am willing to change my job ', N'any')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (10, N'Shilpa Rathi', NULL, N'shilpa305@gmail.com', N'00971504246430', N'Content/Shilpa-Rathi-CV273201414412.pdf', CAST(0x56380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Shilpa-Rathi-Photo273201414412.jpg', NULL, N' Kind Attention : HR Dept. / General Manager

Dear Sir / Madam ,

Greetings of the day !

I am interested in working with this reputed company & enclosing my CV (attached file) herewith for your perusal.  
Kindly go through it and consider me for the post of  Sales/ Business Coordinator or Commercial Assistant in your esteemed organization.        

I look forward to hearing from you soon.
 

Yours Sincerely, 
 

Shilpa Rathi

Mobile no. 050-4246430
                    
shilpa305@gmail.com

 



', N'0000')
SET IDENTITY_INSERT [dbo].[CareerApplicant] OFF
SET IDENTITY_INSERT [dbo].[CommonGallery] ON 

INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (12, N'remittances', N'Content/remittances.png', N'Content/remittances.png', N'remittances', NULL, 1, CAST(0x0000A3130101A828 AS DateTime), N'List', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (13, N'service-sample-1', N'Content/service-sample-1.jpg', N'Content/service-sample-1.jpg', N'service-sample-1', NULL, 1, CAST(0x0000A3130101A828 AS DateTime), N'List', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (14, N'Accessories-Test-Product-1-Blue-Small51201416616', N'Content/Accessories-Test-Product-1-Blue-Small51201416616.jpg', N'Content/Accessories-Test-Product-1-Blue-Small51201416616.jpg', N'Accessories-Test-Product-1-Blue-Small51201416616', NULL, 1, CAST(0x0000A32901002C78 AS DateTime), N'HTML', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (15, N'Accessories-Test-Product-1-Pink-Small51201416757', N'Content/Accessories-Test-Product-1-Pink-Small51201416757.jpg', N'Content/Accessories-Test-Product-1-Pink-Small51201416757.jpg', N'Accessories-Test-Product-1-Pink-Small51201416757', NULL, 1, CAST(0x0000A32900F4AB8C AS DateTime), N'HTML', 1)
SET IDENTITY_INSERT [dbo].[CommonGallery] OFF
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (8, N'shohel', NULL, N'shohelahmedmostafa@gmail.com', NULL, N'999', NULL, NULL, NULL, N'test', CAST(0x0000A25900152F70 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (9, N'shohel', NULL, N'shohelahmedmostafa@gmail.com', NULL, N'999', NULL, NULL, NULL, N'test', CAST(0x0000A25900158D30 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (10, N'shohel', NULL, N'shohel@wvss.net', NULL, N'999', NULL, NULL, NULL, N'test', CAST(0x0000A25900B9F3E8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (11, N'shohel', NULL, N'shohel@wvss.net', NULL, N'222', NULL, NULL, NULL, N'test', CAST(0x0000A25D00C52B00 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (12, N'shohel', NULL, N'shohel@wvss.net', NULL, N'111', NULL, NULL, NULL, N'test', CAST(0x0000A25D00C6A0D4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (13, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000B151C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (14, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000B4528 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (15, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000B6AA8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (16, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000B9730 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (17, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000BC3B8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (18, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000BDFD8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (19, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000BF3C4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (20, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000C08DC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (21, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'This is a test from new site.', CAST(0x0000A2C8000C1DF4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (22, N'Ramesh', NULL, N'Ramesh@digitalnexa.com', NULL, N'12', NULL, NULL, NULL, N'Test with iphone', CAST(0x0000A2C80065E974 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (23, N'Pharmg116', NULL, N'johng453@aol.com', NULL, N'wortpoei', NULL, NULL, NULL, N' Hello! edkeddk interesting edkeddk site! I''m really like it! Very, very edkeddk good! ', CAST(0x0000A2C9005029E0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (24, N'mohamed', NULL, N'madani2@hotmail.com', NULL, N'971509462926', NULL, NULL, NULL, N'I have a web site but nit happy with a few things. I need to add booking hotel by using a calendar and pay 10% online and the balance once in the hotel. What the cost to consult and add those things? What the cost for basic SEO?
thanks.
this is my website : www.comoresdestination.com ', CAST(0x0000A2C901040AB4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (25, N'rayees shaikh', NULL, N'mageneraltradingdubai@gmail.com', NULL, N'0555097860', NULL, NULL, NULL, N'I want to start up a marketing company on a small level which I  need bulk email software so that i can send on behalf of my customers. 
Please let me know how can you help me and what will be your charges.
Thanks', CAST(0x0000A2CA008FFC28 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (26, N'Ramesh', NULL, N'Ramesh@digitalnexa.com', NULL, N'055', NULL, NULL, NULL, N'Test', CAST(0x0000A2CB00164B08 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (27, N'Ed Lagus', NULL, N'eduardo.lagus@wowrak.org', NULL, N'0529025142', NULL, NULL, NULL, N'Develop an e-commerce website, that can be used in multi-platform device as as utilize the social media.

A website that is attractive similar with Yas Water world', CAST(0x0000A2CB002E2DE0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (28, N'hussain', NULL, N'hussainkc52@gmail.com', NULL, N'966553864413', NULL, NULL, NULL, N'youtube videodownloder
', CAST(0x0000A2CC0094983C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (29, N'sahir salim', NULL, N'sahirsalim@gmail.com', NULL, N'0502357535', NULL, NULL, NULL, N'I want the details of a website for our community.. please reply me about the following queries...

- charges per year
- charges for webpage designing
- e-mail access
- password protection
- minimum data capacity

with my regards
sahir salim
', CAST(0x0000A2CC016444B0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (30, N'sahir salim', NULL, N'sahirsalim@gmail.com', NULL, N'0502357535', NULL, NULL, NULL, N'I want the details of a website for our community.. please reply me about the following queries...

- charges per year
- charges for webpage designing
- e-mail access
- password protection
- minimum data capacity

with my regards
sahir salim
', CAST(0x0000A2CC01644708 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (31, N'muneera', NULL, N'muneeraarq@outlook.com', NULL, N'0562223322', NULL, NULL, NULL, N'hello, 
i need help on creating a website, a business website where im gonna sell dogs accessories ( collars, clothes..etc). ', CAST(0x0000A2CD00732300 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (32, N'abdalrhman', NULL, N'abdalrhmanhessin44@yahoo.com', NULL, N'201141362145', NULL, NULL, NULL, N'ineed to ranking my website', CAST(0x0000A2CD00DA2DE8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (33, N'AJMAL T.H', NULL, N'ajmalwky@gmail.com', NULL, N'0561241461', NULL, NULL, NULL, N'I am a PHP Programmer. I am looking for a new job in web development field. I have 1 year experience in web development. Please inform me that if there is any suitable job for me.', CAST(0x0000A2CE0025F1AC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (34, N'kownain', NULL, N'kownain.h3@gmail.com', NULL, N'0552284969', NULL, NULL, NULL, N'Dear Team

Greetings for the day from Kurban Tours
Hope you and your team doing well
Please advise, we are a tourism company in Dubai, we need to promote ourself via social media
How can you help us, can we have a presentation, do you give us a quotation
We are looking for a quote to promote us on FACEBOOK, TWITTER & LINKEDIN
', CAST(0x0000A2CE0049CF50 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (35, N'Tauvanger Ali', NULL, N'touvangerali@gmail.com', NULL, N'+971555955102', NULL, NULL, NULL, N'Dear Sir & Madam, 
 
I am currently employed as an art director and web designer with Conqueror Agency and am seeking an alternative position in a similar capacity. 
 
My job entails formulating creative plans and developing concepts for either an individual advertisement or entire campaigns for print, brochures and web.

You can visit my portfolio at: ali.coderefiner.com

I would be grateful if you would consider for any client requirements that broadly match my skills and experience.
 
I look forward to hearing from you and would be very grateful if you could contact me prior to making any application on my behalf.
 
Thank you for your time and consideration.
 
Yours faithfully,
 
 
Tauvanger Ali
055-5955102', CAST(0x0000A2CE0068C388 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (36, N'Abror', NULL, N'abror@kpi.com', NULL, N' +44 (0) 20 7148 4282', NULL, NULL, NULL, N'kpi.com site seo', CAST(0x0000A2CE00844554 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (37, N'Abror', NULL, N'abror@kpi.com', NULL, N' +44 (0) 20 7148 4282', NULL, NULL, NULL, N'kpi.com seo', CAST(0x0000A2CE008655D8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (38, N'Amit Vyas', NULL, N'vyas.am@gmail.com', NULL, N'1502549157', NULL, NULL, NULL, N'test ', CAST(0x0000A2CE0098628C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (39, N'Amit Vyas', NULL, N'vyas.am@gmail.com', NULL, N'1502549157', NULL, NULL, NULL, N'website fixes', CAST(0x0000A2CE0099EEA4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (40, N'prasadh', NULL, N'durgarao835@gmail.com', NULL, N'0557339376', NULL, NULL, NULL, N'i want to make money by sellin products', CAST(0x0000A2CE00B65D28 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (41, N'Faris Halteh', NULL, N'faris.halteh@lau.edu', NULL, N'+46722781478', NULL, NULL, NULL, N'To whom it may concern,

Hope this email finds you well.
I am currently doing my masters degree in Computer Science at Uppsala University in Sweden. And as I was looking up for software companies in Dubai, I came across your website, which seemed to be quite appealing. After reading about Nexa and the projects and services that you have been involved in, I got interested in doing an internship at your company to gain more experience and at the same time benefit the company that I''m working at. As a result, I am writing to ask if you offer summer internships / summer jobs for students.

Thank you so much,
Faris Halteh.', CAST(0x0000A2CE010C5174 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (42, N'Fahad Ahmed Khan', NULL, N'faahad.ahmed@yahoo.com', NULL, N'00923132210415', NULL, NULL, NULL, N'Want to work in your company as a Graphic / Web Designer.', CAST(0x0000A2CF0067F9F8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (43, N'Fahad Ahmed Khan', NULL, N'faahad.ahmed@yahoo.com', NULL, N'00923132210415', NULL, NULL, NULL, N'Want to work in your company as a Graphic / Web Designer.', CAST(0x0000A2CF0067FB24 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (44, N'Vibhav Shukla', NULL, N'vibhavhytech@gmail.com', NULL, N'+971503781746', NULL, NULL, NULL, N'I want make webside for our business', CAST(0x0000A2CF00AF8534 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (45, N'mubashar ahmad', NULL, N'mubashar_pieces424@yahoo.com', NULL, N'0092 3315054839', NULL, NULL, NULL, N'I''m web & graphic designer.', CAST(0x0000A2CF00B2E288 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (46, N'Erieny Sourial', NULL, N'esourial@altayer.com', NULL, N'042011077', NULL, NULL, NULL, N'Hello

I am contacting you from Al Tayer Group, and my enquiry is regarding email responsive design.

We are looking for an agency that can design and develop email responsive design, which has proven very challenging in this market. We need an agency that is skilled and aware of email best practice for desktop and mobile.

Could you kindly provide examples of email responsive design work that you had done for your clients. The work needs to demonstrate layout and design competencies, code best practice (ie. Alt Tags, etc), and so on.

Thank you.

Erieny Sourial
Assistant Customer Communications Manager
Al Tayer Group', CAST(0x0000A2D000399C0C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (47, N'Mohd Riyaz ', NULL, N'riyaz@syslabme.com', NULL, N'00971-556088964', NULL, NULL, NULL, N'Hi,

How much will it cost me to make my website www.syslabme.com to come in first 2 or 3 pages during google search for around 15-20 keywords.

rgds 

riyaz', CAST(0x0000A2D00090AAC4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (48, N'Sultan Musallam Askar Albraiki', NULL, N'albraiki1981@gmail.com', NULL, N'00971502866337', NULL, NULL, NULL, N'fully functioning buisness website for providing manpower informtion and services with cv''s and order cpabilities with trying to keep it always on top of online searches if possible', CAST(0x0000A2D0017A36A8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (49, N'lzQdzzgQJaQRBSaOadJ', NULL, N'lDjvgpyRCoXpiBCG', NULL, N'BvSCsGHCGF', NULL, NULL, NULL, N'sverige.txt;1', CAST(0x0000A2D10030C2D0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (50, N'YulTYulYNNatDI', NULL, N'JCOmNAHbZgwokmzwniC', NULL, N'XrqLRopnOVA', NULL, NULL, NULL, N'sverige.txt;1', CAST(0x0000A2D10030C8AC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (51, N'aIOLGVvXaou', NULL, N'yFDMUOzy', NULL, N'SxxJhMHFoSCUXWaz', NULL, NULL, NULL, N'sverige.txt;1', CAST(0x0000A2D100311730 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (52, N'miADREEQdXxlW', NULL, N'RWhbJFOI', NULL, N'zXPftubsuT', NULL, NULL, NULL, N'sverige.txt;1', CAST(0x0000A2D100316104 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (53, N'Medo', NULL, N'futureemarket@gmail.com', NULL, N'01148086090', NULL, NULL, NULL, N'Basic Seo Steps to ant website', CAST(0x0000A2D1003623C4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (54, N'Zafar Habib', NULL, N'm.zafar6@gmail.com', NULL, N'971562001904', NULL, NULL, NULL, N'


Respected Sir,



Application for Job


I am Zafar Habib currently live in UAE on Visit Visa looking for a Job. I have Completed MBA (Banking & Finance) and also I have 7 years experience in Accounts Department. 
Due to my abilities and work experience, I would be pleased to discuss my application with you and I am available for an interview at your convenience.  I can be contacted at the phone numbers listed above.

Please find the attach CV in Doc Format


Mail ID: m.zafar6@gmail.com
	 
Contact: +971-56-200-1904
	   


Yours sincerely



ZAFAR HABIB

Enc.
', CAST(0x0000A2D2003069C0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (55, N'MOHAMMED SHAFI SHAIKH', NULL, N'mshafisshaikh@gmail.com', NULL, N'00971552612830', NULL, NULL, NULL, N'Dear Gentlemen

I am looking for site exactly like dubizzle.com which is classified site wherein one can log and post an free advertise. Google adsense ads appear on same. 

Name can be www.free-classifieds.com with 3 months web hosting paid, Google adsense monetised, 100 ads created, SEO done and Search Engine submitted.

This will be covering the middle east with each state ads searchable like dubai.free-classifieds.com, It will cover all items as available in dubizzle.com

Please inform the Initial development, hosting and seo cost for same. Also, It has to generate ( Google adsense ) good returns ( 100 % whatever revenue generated ) part of which will be given to maintain the search engine and traffic for valued customers and users of website. This will be paid as site running cost which can be quoted. 

Please think over, give offer and idea as how you can achieve?

With warm regards

MOHAMMED SHAFI SHAIKH
Email : mshafisshaikh@gmail.com
Cell    : 00 971 55 2612830
Skype :  shahenazshafishaikh', CAST(0x0000A2D2004ACD9C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (56, N'sadham', NULL, N'sadham.pv@gmail.com', NULL, N'0528743184', NULL, NULL, NULL, N'i am a web designing completed candidate but i have no experience and i forget more things about web designing. so i humbly request you to arrange a part time training without salary', CAST(0x0000A2D200716CB8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (57, N'ahmed', NULL, N'smraaa7@gmail.com', NULL, N'01065457315', NULL, NULL, NULL, N'Trusted woman', CAST(0x0000A2D200D27F80 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (58, N'Nishad', NULL, N'cool_heart@hotmail.com', NULL, N'0554647478', NULL, NULL, NULL, N'Help my website show up in the first page of google search', CAST(0x0000A2D300311BE0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (59, N'<SCRIPT>ALERT(''MAHESH'')</SCRIPT>', NULL, N'HELLO@YAHOO.COM', NULL, N'3312125635', NULL, NULL, NULL, N'<SCRIPT>ALERT(''MAHESH'')</SCRIPT>', CAST(0x0000A2D3005E38B4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (60, N'Sheetal', NULL, N'rosely_pais@yahoo.co.in', NULL, N'0558671585', NULL, NULL, NULL, N'Website development', CAST(0x0000A2D30157C7D0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (61, N'shohel', NULL, N'shohel@wvss.net', NULL, N'0528084486', NULL, NULL, NULL, N'test', CAST(0x0000A2D301781814 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (62, N'Dipika', NULL, N'dipika@digitalnexa.com', NULL, N'+971 (4) 432 9464', NULL, NULL, NULL, N'Test', CAST(0x0000A2D30180081C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (63, N'laith', NULL, N'laith.khaleq@gmail.com', NULL, N'0504507145', NULL, NULL, NULL, N'website development', CAST(0x0000A2D500040E48 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (64, N'Shoaib ', NULL, N'khanshoaib998@gmail.com', NULL, N'055-7900554', NULL, NULL, NULL, N'hi, hope this mail will find your fine. i have one inquiry for the web mail ids. their is some techincal problems occurs every 02-03 weeks. Will you host it, what will be the procudre for the hosting to get it and kindly send the quote for that ', CAST(0x0000A2D5000B63A0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (65, N'Kayleigh Sealey', NULL, N'kayleigh.sealey@gameplan.ae', NULL, N'0561147914', NULL, NULL, NULL, N'Changes and regular updates made to current wesbite. ', CAST(0x0000A2D5004B0E10 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (66, N'Mian Majid', NULL, N'mbmajid07@gmail.com', NULL, N'0561197093', NULL, NULL, NULL, N'need to develop a web site and company softwear', CAST(0x0000A2D5004F6500 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (67, N'Aqib Rabani', NULL, N'aqib125@ymail.com', NULL, N'+923009645520', NULL, NULL, NULL, N'i am a advertiser i need a job', CAST(0x0000A2D50060A6F8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (68, N'Noor', NULL, N'Nalhajali@ucs.ae', NULL, N'043714310', NULL, NULL, NULL, N'Want to ask if you have ecommerce solution  if so need more details', CAST(0x0000A2D5008DA7AC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (69, N'mukesh pandey', NULL, N'mukesh@leadsdubai.com', NULL, N'971503047470', NULL, NULL, NULL, N'hey amit
i am in ppc business.
how about extending email marketing results for your clients by doing remarketing campaigns and get more to your clients ROI?

thanks

Mukesh', CAST(0x0000A2D6000964B0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (70, N'Ramesh', NULL, N'ramesh@digitalnexa.com', NULL, N'055', NULL, NULL, NULL, N'Testing firefox browser', CAST(0x0000A2D6002A8208 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2D800538F68 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (72, N'Halim Loutfi', NULL, N'a.h.loutfi@gmail.com', NULL, N'+33789455772', NULL, NULL, NULL, N'Hello,
I am very interested by the work your agency delivers and would like to know if you have job openings in digital marketing.
I have majored in Visual communication and Branding in Paris, France.
I will be more than pleased to send you my CV.
Thank you for the time you have accorded me and waiting for your response.
Best regards', CAST(0x0000A2D8007D9B14 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (73, N'Kaori Takeda', NULL, N'K_oritak@hotmail.com', NULL, N'0553676203', NULL, NULL, NULL, N'I want to design the web for my new concierge business and make a good SEO positioning', CAST(0x0000A2D8007F722C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (74, N'Imran Asghar', NULL, N'orient1161@yahoo.com', NULL, N'03336006764', NULL, NULL, NULL, N'marketing job', CAST(0x0000A2D800D5E760 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (75, N'zaryab', NULL, N'zaryab.ali@nlptechforma.com', NULL, N'0559994911', NULL, NULL, NULL, N'hi,
we went to relaunch the website current website is www.nlptechforma.com and we want to retain the same content and move to www.sitespower.com layout and desing.
thanks', CAST(0x0000A2D8016688C4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (76, N'zaryab', NULL, N'zaryab.ali@nlptechforma.com', NULL, N'0559994911', NULL, NULL, NULL, N'hi,
we went to relaunch the website current website is www.nlptechforma.com and we want to retain the same content and move to www.sitespower.com layout and desing.
thanks', CAST(0x0000A2D801679070 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (77, N'Priyanka Sharma', NULL, N'priyanka@gowebbaby.com', NULL, N'718 717 8666', NULL, NULL, NULL, N'we are looking web development projects', CAST(0x0000A2D900029BF8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (78, N'Ankur', NULL, N'Ankur@hexagoninfosot.com', NULL, N'+919924544888', NULL, NULL, NULL, N'We are a software, web and mobile development company from Ahmedabad, India. We are working with team of young professionals developing for Mobile apps. We develop applications for iPhone, iPad, Android, Tablets, web design, web development and SEO.
 We also work as consultants on contract and full time for your web & mobile apps development requirements.
 It will be an honor to work for your esteemed organization.
', CAST(0x0000A2D90008E29C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (79, N'sagar', NULL, N'sagargohel1995@gmail.com', NULL, N'8511675544', NULL, NULL, NULL, N'job releted details.', CAST(0x0000A2D9004C5E64 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (80, N'Hakima', NULL, N'hakima.harrabi@hotmail.fr', NULL, N'055832774', NULL, NULL, NULL, N'i need responsive  dynamic website for real state company
can you send me quotation as soon as posssible', CAST(0x0000A2D901875798 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (81, N'Jerold Vaz', NULL, N'jeroldvaz@gmail.com', NULL, N'+97455530164', NULL, NULL, NULL, N'Hi,
I am looking for a company to design and develop websites for our clients. We are looking for a company with whom we can have a long time relationship and to whom we can source all our clients’ design works. 
We right now need a website designed and developed for our client. You can contact me over mail (jeroldv@bpo-plus.com) or skype (jerold.vaz) and I will share the details of the project and you can give me a quotation on the same.
Looking forward to your reply. 

P.S Please send your Company profile to me via mail.

Regards,
Jerold Vaz
Manager
BPO+
', CAST(0x0000A2DB002EECE4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (82, N'mahmoud alhor', NULL, N'mahmoud_hor@hotmail.com', NULL, N'+971561852799', NULL, NULL, NULL, N'Hello Dear Mr/Ms

I would like to enhance my career as an Web developer by applying for the 
position now available. 

Waiting for your reply.

Thank you', CAST(0x0000A2DB002FD99C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (83, N'Royston Christopher Lobo', NULL, N'rolobo@mixedbag-marketing.com', NULL, N'0509541406', NULL, NULL, NULL, N'Hi there,

I need to meet / discuss a fairly important project with somebody in your management team. The project will comprise of 4 individual websites, each with customized software/functionality/application.

Please do give me a call to discuss as I need to get back to the client with a pricing and timeline strategy for implementation by Tuesday. 

Kind Regards,
Royston', CAST(0x0000A2DB0051318C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (84, N'Manvi', NULL, N'Manviarora8787@gmail.com', NULL, N'0561637120', NULL, NULL, NULL, N'job vacancy??', CAST(0x0000A2DB005BF824 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (85, N'SHAHOW AFANDI', NULL, N'INFO@VANACOMPANY.COM', NULL, N'+9647504462664', NULL, NULL, NULL, N'WE NEED TO GET HOST SERVICE FOR WWW.VANAFURNITURE.COM', CAST(0x0000A2DB0070EF54 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (86, N'Kalpna', NULL, N'kalpna@7gmedia.com', NULL, N'+971553629838', NULL, NULL, NULL, N'Need help to Arabize my website www.7gmedia.com', CAST(0x0000A2DC000AA49C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (87, N'Philip Weidenbruch', NULL, N'p.weidenbruch@gmail.com', NULL, N'0502280428', NULL, NULL, NULL, N'To whom it may concern,

I am looking to develop a website with member logins, inboxes and posting options. Kind of similar to foodonclick.com but a little bit more complex. Please let me know your price ranges, and maybe it is possible to set up a meeting to discuss further details.

Best regards,

Philip', CAST(0x0000A2DC0036C900 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (88, N'sooraj ao', NULL, N'soorajao@gmail.com', NULL, N'+91 9400412858', NULL, NULL, NULL, N'looking for a web designer job in your firm ', CAST(0x0000A2DC007F09E0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (89, N'Kesavan', NULL, N'bmkesavan@gmail.com', NULL, N'0557338031', NULL, NULL, NULL, N'tamil Kesavan', CAST(0x0000A2DC00CA8744 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (90, N'AL Bakri Pet Food Trading', NULL, N'moin.albakri@gmail.com', NULL, N'065453370', NULL, NULL, NULL, N'I need an e-commerce website for our online pet store which should be responsive on all platform.', CAST(0x0000A2DE0030CC30 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (91, N'd', NULL, N'A@aa.xx', NULL, N'd', NULL, NULL, NULL, N'f', CAST(0x0000A2DE00A80B88 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (92, N'd', NULL, N'A@aa.xx', NULL, N'd', NULL, NULL, NULL, N'f', CAST(0x0000A2DE00A80CB4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (93, N'd', NULL, N'A@aa.xx', NULL, N'd', NULL, NULL, NULL, N'f', CAST(0x0000A2DE00A80CB4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (94, N'Michael lee', NULL, N'Michaelcl3323@gmail.com', NULL, N'0562079362', NULL, NULL, NULL, N'Greetings,

  My company already has a current up and running website (heartlandUAE.com) but would like to improve on it. We would like to structure it similar to our U.S. affiliates current website (heartlandrvs.com). Your help in this matter is greatly appreciated.', CAST(0x0000A2DE00AD22A8 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (95, N'Ahmad Qadir', NULL, N'ahmadqadir2001@gmail.com', NULL, N'none', NULL, NULL, NULL, N'Hi, i''m creating a brochure for school on Johann Gutenberg so can i please get help to make a website for my project, oh btw i''m 12 years old', CAST(0x0000A2DE014E3DA0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (96, N'Karolis', NULL, N'karolis.snarskis@gmail.com', NULL, N'0037061780186', NULL, NULL, NULL, N'Hello,
I am Karolis and I own one of the biggest racing/rally car classifieds website online. Rally24.com and Racecarsforsale.com I need your help to lounch them to the .ae market. I need SEO services and you to help my set the site for .ae users. Can you help to lounch my sites in .es? 

Karolis', CAST(0x0000A2DF00334ADC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (97, N'Khaled Elgohary', NULL, N'khaled.elgohary@gmail.com', NULL, N'07866262545', NULL, NULL, NULL, N'fine tuning and redevelopment of work on my website', CAST(0x0000A2DF004943DC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (98, N'Shashikant Rane', NULL, N'shashikantrane2013@gmail.com', NULL, N'985648525', NULL, NULL, NULL, N'I want to create 5 page html website with contact form. i will provide the images for the same. How much it will cost? don''t call on above number it is fake number as i do not want your marketing people to call me again and again . If i like your quotation affordable then i will call on your number.', CAST(0x0000A2E0003E21DC AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (99, N'Arun', NULL, N'principal@royalacademyajman.com', NULL, N'0558655229', NULL, NULL, NULL, N'School website', CAST(0x0000A2E001604A54 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (100, N'THANI', NULL, N'thani.alromaithi@hotmail.com', NULL, N'+971506611554', NULL, NULL, NULL, N'I NEED TO MAKE WEBSITE FOR A COMPANY.', CAST(0x0000A2E1002D1248 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (101, N'mohammed eltayeb mustafa', NULL, N'm.eltayeb@katiasd.com', NULL, N'00249912395019', NULL, NULL, NULL, N'Dear Sir
 katia international company the owner for  new newspaper will published soon in Sudan looking for  your services so for that send your rate price for website design and hosting and all online marketing  .

Mohamed Eltayeb Mustafa
Deputy General  Manager
Katia Internatioanl Company
 Tel         :-00249-183-230111
FAX        :- 00249-183-230110
Mobile :- 00249912395019
', CAST(0x0000A2E2002FF364 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (102, N'Ajirin', NULL, N'info@traveldeals.ae', NULL, N'971 505981324', NULL, NULL, NULL, N'
Please sent to details of seo and your pricing', CAST(0x0000A2E300186BF4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (103, N'Bernard Richardson', NULL, N'contactBRP@gmail.com', NULL, N'056 6557499', NULL, NULL, NULL, N'Hi there, I need SEO on my small business website. Can you please call and advise me on your costs and services.
Regards Bernie
www.brichardsonphotography.com', CAST(0x0000A2E300A9DCC4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (104, N'Shoaib', NULL, N'saleinfo.ae@gmail.com', NULL, N'971509912374', NULL, NULL, NULL, N'Hi Admin,

Can you please send me your company''s short profile detail, so I can understand your company in a better way.

If you can send this detail in an Email template form it will be highly appreciated.

Thank,
Shoaib', CAST(0x0000A2E40073995C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (105, N'sabiha siddiqui', NULL, N'grsabihasiddiqui@yahoo.com', NULL, N'0300', NULL, NULL, NULL, N'Hi, I am professional web designer over 8years. I want to work with you.  Please contact me for more details. My skype I''d is sabihasiddiqui. Thanks', CAST(0x0000A2E400B4459C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (106, N'ABDU SHALIH', NULL, N'shalihkkl@gmail.com', NULL, N'0567766273', NULL, NULL, NULL, N'I looking for a Web&Graphic Job


My name is Abdu Shalih, I have 4th year experience in Web & Graphic Designing field .
My Portfolio:www.shalih.in
0567766273
shalihkkl@gmail.com', CAST(0x0000A2E4014C5044 AS DateTime))
GO
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (107, N'Aroubrego', NULL, N'toptanlar@gmail.com', NULL, NULL, NULL, NULL, NULL, N'Handful Of Strategies To Play With seo And Actually Earn Money From It!', CAST(0x0000A2E500437844 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (108, N'mohamed ameen', NULL, N'ameenph1@gmail.com', NULL, N'9995689296', NULL, NULL, NULL, N'web designer', CAST(0x0000A2E500539094 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (109, N'Jan', NULL, N'kilikku@gmail.com', NULL, N'0506361408', NULL, NULL, NULL, N'I''m interested in email marketing and I have 50 000 contacts. What kind of pricing plans you have?', CAST(0x0000A2E500640320 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (110, N'Dilip Nair', NULL, N'cfo@pangulf-me.com', NULL, N'+97455616870', NULL, NULL, NULL, N'To develop a website for the car Rental Company PANGULF ', CAST(0x0000A2E60008C67C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (111, N'Richelle Santosidad', NULL, N'richelle.santosidad@yahoo.com', NULL, N'0504361770', NULL, NULL, NULL, N'Hi,

I would like to inquire for your e-mail marketing rates. 

Thank you.

Regards,
Richelle Santosidad', CAST(0x0000A2E60066E1E4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (112, N'Muhammad Tauseef', NULL, N'itrustallah1@gmail.com', NULL, N'04 4308242', NULL, NULL, NULL, N'Dear Sir i am Graphic & Designer and Also Animator and Know 3d max. i want to apply for job. you have any job so plz tell me. i am waiting for your positive respones. ', CAST(0x0000A2E7002030A0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (113, N'foziljoni shohzamon', NULL, N'farhori1@email.com', NULL, N'+971527458735', NULL, NULL, NULL, N'tajikistan', CAST(0x0000A2E700CBAB10 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (114, N'Mikael Kjaer', NULL, N'mikaelk84@gmail.com', NULL, N'+97156178823', NULL, NULL, NULL, N'I need a basic website to show my lightning business. Just a small about, contact and a products page (no sales). I can make all product descriptions myself.

Can you give me an approximate price?', CAST(0x0000A2E90037DA0C AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (115, N'Ramzad Kumble', NULL, N'kumble@astreaminfotech.com', NULL, N'+91-9633056168', NULL, NULL, NULL, N'Hello Sir,
We are Mobile application Development Company based in India. Looking forward for partnership.', CAST(0x0000A2E90041E9D4 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (116, N'Sarah', NULL, N'sarah.lamri@gmail.com', NULL, N'00917567591890', NULL, NULL, NULL, N'Hello,

I would like to have a quotation for an E-commerce website Design, development and hosting.
Regards
', CAST(0x0000A2E90080AFC0 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (117, N'hamid', NULL, N'hamid@sysllc.com', NULL, N'0528863140', NULL, NULL, NULL, N'email marketing
', CAST(0x0000A2E900A34B20 AS DateTime))
INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (118, N'Debashis', NULL, N'debashis.chowdhury@wvss.net', N'2334343', NULL, NULL, NULL, NULL, N'Test message', CAST(0x0000A337010EED6C AS DateTime))
SET IDENTITY_INSERT [dbo].[Contact] OFF
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AD', N'ANDORRA', 23, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AE', N'UNITED ARAB EMIRATES', 244, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AF', N'AFGHANISTAN', 19, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AG', N'ANTIGUA AND BARBUDA', 27, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AI', N'ANGUILLA', 25, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AL', N'ALBANIA', 20, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AM', N'ARMENIA', 29, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AN', N'NETHERLANDS ANTILLES', 172, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AO', N'ANGOLA', 24, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AQ', N'ANTARCTICA', 26, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AR', N'ARGENTINA', 28, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AS', N'AMERICAN SAMOA', 22, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AT', N'AUSTRIA', 32, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AU', N'AUSTRALIA', 31, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AW', N'ARUBA', 30, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AZ', N'AZERBAIJAN', 33, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BA', N'BOSNIA AND HERZEGOWINA', 45, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BB', N'BARBADOS', 37, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BD', N'BANGLADESH', 36, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BE', N'BELGIUM', 39, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BF', N'BURKINA FASO', 52, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BG', N'BULGARIA', 51, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BH', N'BAHRAIN', 35, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BI', N'BURUNDI', 53, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BJ', N'BENIN', 41, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BM', N'BERMUDA', 42, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BN', N'BRUNEI DARUSSALAM', 50, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BO', N'BOLIVIA', 44, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BR', N'BRAZIL', 48, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BS', N'BAHAMAS', 34, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BT', N'BHUTAN', 43, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BV', N'BOUVET ISLAND', 47, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BW', N'BOTSWANA', 46, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BY', N'BELARUS', 38, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BZ', N'BELIZE', 40, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CA', N'CANADA', 56, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CC', N'COCOS (KEELING); ISLANDS', 64, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CD', N'CONGO, THE DRC', 68, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CF', N'CENTRAL AFRICAN REPUBLIC', 59, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CG', N'CONGO', 67, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CH', N'SWITZERLAND', 227, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CI', N'COTE D''IVOIRE', 71, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CK', N'COOK ISLANDS', 69, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CL', N'CHILE', 61, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CM', N'CAMEROON', 55, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CN', N'CHINA', 62, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CO', N'COLOMBIA', 65, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CR', N'COSTA RICA', 70, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CU', N'CUBA', 73, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CV', N'CAPE VERDE', 57, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CX', N'CHRISTMAS ISLAND', 63, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CY', N'CYPRUS', 74, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CZ', N'CZECH REPUBLIC', 75, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DE', N'GERMANY', 100, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DJ', N'DJIBOUTI', 77, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DK', N'DENMARK', 76, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DM', N'DOMINICA', 78, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DO', N'DOMINICAN REPUBLIC', 79, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DZ', N'ALGERIA', 21, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EC', N'ECUADOR', 81, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EE', N'ESTONIA', 86, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EG', N'EGYPT', 82, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EH', N'WESTERN SAHARA', 256, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ER', N'ERITREA', 85, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ES', N'SPAIN', 218, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ET', N'ETHIOPIA', 87, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FI', N'FINLAND', 91, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FJ', N'FIJI', 90, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FK', N'FALKLAND ISLANDS (MALVINAS);', 88, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FM', N'MICRONESIA, FEDERATED STATES OF', 159, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FO', N'FAROE ISLANDS', 89, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FR', N'FRANCE', 92, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FX', N'FRANCE, METROPOLITAN', 93, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GA', N'GABON', 97, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GB', N'UNITED KINGDOM', 245, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GD', N'GRENADA', 105, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GE', N'GEORGIA', 99, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GF', N'FRENCH GUIANA', 94, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GH', N'GHANA', 101, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GI', N'GIBRALTAR', 102, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GL', N'GREENLAND', 104, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GM', N'GAMBIA', 98, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GN', N'GUINEA', 109, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GP', N'GUADELOUPE', 106, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GQ', N'EQUATORIAL GUINEA', 84, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GR', N'GREECE', 103, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GS', N'SOUTH GEORGIA AND SOUTH S.S.', 217, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GT', N'GUATEMALA', 108, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GU', N'GUAM', 107, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GW', N'GUINEA-BISSAU', 110, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GY', N'GUYANA', 111, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HK', N'HONG KONG', 116, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HM', N'HEARD AND MC DONALD ISLANDS', 113, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HN', N'HONDURAS', 115, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HR', N'CROATIA (local name: Hrvatska);', 72, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HT', N'HAITI', 112, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HU', N'HUNGARY', 117, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ID', N'INDONESIA', 120, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IE', N'IRELAND', 123, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IL', N'ISRAEL', 124, 0)
GO
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IN', N'INDIA', 119, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IO', N'BRITISH INDIAN OCEAN TERRITORY', 49, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IQ', N'IRAQ', 122, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IR', N'IRAN (ISLAMIC REPUBLIC OF);', 121, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IS', N'ICELAND', 118, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IT', N'ITALY', 125, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JM', N'JAMAICA', 126, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JO', N'JORDAN', 128, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JP', N'JAPAN', 127, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KE', N'KENYA', 130, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KG', N'KYRGYZSTAN', 135, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KH', N'CAMBODIA', 54, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KI', N'KIRIBATI', 131, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KM', N'COMOROS', 66, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KN', N'SAINT KITTS AND NEVIS', 199, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KP', N'KOREA, D.P.R.O.', 132, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KR', N'KOREA, REPUBLIC OF', 133, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KW', N'KUWAIT', 134, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KY', N'CAYMAN ISLANDS', 58, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KZ', N'KAZAKHSTAN', 129, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LA', N'LAOS', 136, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LB', N'LEBANON', 138, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LC', N'SAINT LUCIA', 200, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LI', N'LIECHTENSTEIN', 142, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LK', N'SRI LANKA', 219, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LR', N'LIBERIA', 140, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LS', N'LESOTHO', 139, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LT', N'LITHUANIA', 143, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LU', N'LUXEMBOURG', 144, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LV', N'LATVIA', 137, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LY', N'LIBYAN ARAB JAMAHIRIYA', 141, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MA', N'MOROCCO', 165, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MC', N'MONACO', 161, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MD', N'MOLDOVA, REPUBLIC OF', 160, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ME', N'MONTENEGRO', 163, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MG', N'MADAGASCAR', 147, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MH', N'MARSHALL ISLANDS', 153, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MK', N'MACEDONIA', 146, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ML', N'MALI', 151, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MM', N'MYANMAR (Burma);', 167, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MN', N'MONGOLIA', 162, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MO', N'MACAU', 145, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MP', N'NORTHERN MARIANA ISLANDS', 180, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MQ', N'MARTINIQUE', 154, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MR', N'MAURITANIA', 155, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MS', N'MONTSERRAT', 164, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MT', N'MALTA', 152, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MU', N'MAURITIUS', 156, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MV', N'MALDIVES', 150, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MW', N'MALAWI', 148, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MX', N'MEXICO', 158, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MY', N'MALAYSIA', 149, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MZ', N'MOZAMBIQUE', 166, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NA', N'NAMIBIA', 168, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NC', N'NEW CALEDONIA', 173, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NE', N'NIGER', 176, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NF', N'NORFOLK ISLAND', 179, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NG', N'NIGERIA', 177, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NI', N'NICARAGUA', 175, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NL', N'NETHERLANDS', 171, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NO', N'NORWAY', 181, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NP', N'NEPAL', 170, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NR', N'NAURU', 169, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NU', N'NIUE', 178, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NZ', N'NEW ZEALAND', 174, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'OM', N'OMAN', 182, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PA', N'PANAMA', 185, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PE', N'PERU', 188, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PF', N'FRENCH POLYNESIA', 95, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PG', N'PAPUA NEW GUINEA', 186, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PH', N'PHILIPPINES', 189, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PK', N'PAKISTAN', 183, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PL', N'POLAND', 191, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PM', N'ST. PIERRE AND MIQUELON', 221, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PN', N'PITCAIRN', 190, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PR', N'PUERTO RICO', 193, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PT', N'PORTUGAL', 192, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PW', N'PALAU', 184, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PY', N'PARAGUAY', 187, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'QA', N'QATAR', 194, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RE', N'REUNION', 195, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RO', N'ROMANIA', 196, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RS', N'SERBIA', 207, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RU', N'RUSSIAN FEDERATION', 197, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RW', N'RWANDA', 198, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SA', N'SAUDI ARABIA', 205, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SB', N'SOLOMON ISLANDS', 213, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SC', N'SEYCHELLES', 208, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SD', N'SUDAN', 222, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SE', N'SWEDEN', 226, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SG', N'SINGAPORE', 210, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SH', N'ST. HELENA', 220, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SI', N'SLOVENIA', 212, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SJ', N'SVALBARD AND JAN MAYEN ISLANDS', 224, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SK', N'SLOVAKIA (Slovak Republic);', 211, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SL', N'SIERRA LEONE', 209, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SM', N'SAN MARINO', 203, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SN', N'SENEGAL', 206, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SO', N'SOMALIA', 214, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SR', N'SURINAME', 223, 1)
GO
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SS', N'SOUTH SUDAN', 216, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ST', N'SAO TOME AND PRINCIPE', 204, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SV', N'EL SALVADOR', 83, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SY', N'SYRIAN ARAB REPUBLIC', 228, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SZ', N'SWAZILAND', 225, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TC', N'TURKS AND CAICOS ISLANDS', 240, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TD', N'CHAD', 60, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TF', N'FRENCH SOUTHERN TERRITORIES', 96, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TG', N'TOGO', 233, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TH', N'THAILAND', 232, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TJ', N'TAJIKISTAN', 230, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TK', N'TOKELAU', 234, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TM', N'TURKMENISTAN', 239, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TN', N'TUNISIA', 237, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TO', N'TONGA', 235, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TP', N'EAST TIMOR', 80, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TR', N'TURKEY', 238, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TT', N'TRINIDAD AND TOBAGO', 236, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TV', N'TUVALU', 241, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TW', N'TAIWAN, PROVINCE OF CHINA', 229, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TZ', N'TANZANIA, UNITED REPUBLIC OF', 231, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UA', N'UKRAINE', 243, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UG', N'UGANDA', 242, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UM', N'U.S. MINOR ISLANDS', 247, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'US', N'UNITED STATES', 246, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UY', N'URUGUAY', 248, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UZ', N'UZBEKISTAN', 249, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VA', N'HOLY SEE (VATICAN CITY STATE);', 114, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VC', N'SAINT VINCENT AND THE GRENADINES', 201, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VE', N'VENEZUELA', 251, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VG', N'VIRGIN ISLANDS (BRITISH);', 253, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VI', N'VIRGIN ISLANDS (U.S.);', 254, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VN', N'VIET NAM', 252, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VU', N'VANUATU', 250, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'WF', N'WALLIS AND FUTUNA ISLANDS', 255, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'WS', N'SAMOA', 202, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'YE', N'YEMEN', 257, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'YT', N'MAYOTTE', 157, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZA', N'SOUTH AFRICA', 215, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZM', N'ZAMBIA', 258, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZW', N'ZIMBABWE', 259, 1)
SET IDENTITY_INSERT [dbo].[Instagram] ON 

INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (41, 1, N'Finally!! :) #DNP#diaryngpanget', N'Content/InstaThumb2142014122547168.jpg', N'Content/InstaBig2142014122547895.jpg', 1, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage8.ak.instagram.com/d7a088aac92d11e38ea224be059598b0_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (42, 2, N'Shout out for shout out!ðŸ“¢ðŸ“¢ðŸ“¢ Follow @susieee_xo #sfs #s4s #shoutout #diaryngpanget #dnp #fab4 #cinegang #jadine #yandre', N'Content/InstaThumb2142014122548637.jpg', N'Content/InstaBig2142014122549391.jpg', 2, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage2.ak.instagram.com/89f88cb6c92c11e39ee80002c9c8e732_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (43, 3, N'Will watch Diary ng Panget with Tita @florachua3 Ate @steph_hanie and @marcderrick_8!!!! :)))))) #DNP #Movie #DiaryNgPanget #happykiddo', N'Content/InstaThumb2142014122550208.jpg', N'Content/InstaBig2142014122550963.jpg', 3, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/69463aaec92c11e39a720002c9c705fa_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (44, 4, N'#DNP # ST2 #ROCKETEER ðŸ’‹ðŸŽµðŸŽ¶ðŸ’¿ðŸ˜—', N'Content/InstaThumb2142014122551613.jpg', N'Content/InstaBig2142014122552550.jpg', 4, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/72a4cbcec92711e389340002c954cdde_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (45, 5, N'Tapos na manuod ng #dnp ganda! Gwapo ni @mjcayabyab6', N'Content/InstaThumb21420141225530.jpg', N'Content/InstaBig2142014122553674.jpg', 5, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-b.ak.instagram.com/hphotos-ak-ash/914349_507726032667273_270245080_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (46, 6, N'Shout out for Shout out? #sfs #dnp #diaryngpanget #shoutout #s4s #bored', N'Content/InstaThumb2142014122554285.jpg', N'Content/InstaBig2142014122554439.jpg', 6, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-b.ak.instagram.com/hphotos-ak-ash/10175205_689253741130921_69870160_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (48, 8, N'It all started with ate Denny! An amazing writer! Love you!ðŸ‘ You didn''t just made the cast proud but also to all of the supporters of Diary Ng Panget! We love you ate Denny! Good luck in everything you do!â¤â¤â¤ #denny #diaryngpangetthemovie #diaryngpanget #dnp #dnpcast #jadine #yandre #teameyoss #teamlorhad #nadinelustre #jamesreid #yassipressman #andreparas #proud #fangirl #fab4 #cinegang', N'Content/InstaThumb2142014122555376.jpg', N'Content/InstaBig2142014122555534.jpg', 8, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/800ca780c92911e3a14f0002c955b9d6_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (49, 9, N'Ako si Maxine :) isa akong royalty from england ðŸ’Ž direct descendant ni Queen Elizabeth.

#DNP is â¤', N'Content/InstaThumb2142014122555863.jpg', N'Content/InstaBig2142014122556480.jpg', 9, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage4.ak.instagram.com/655c594ec92911e399990002c9443240_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (50, 10, N'busy ung mag-nanay  hahaha
#luvu 
#DNP', N'Content/InstaThumb214201412255741.jpg', N'Content/InstaBig2142014122557660.jpg', 10, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/10e49f02c92911e3b6690002c9121700_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (51, 11, N'Now watching :) Movie Marathon as always ! #DNP', N'Content/InstaThumb2142014122558230.jpg', N'Content/InstaBig2142014122558498.jpg', 11, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/6444b2c6c92511e3a50d0002c954e1a8_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (52, 12, N'Mwahahahaha atlast kita na jud ko. #DNP #JAMESREID ðŸ˜ðŸ˜˜', N'Content/InstaThumb214201412255952.jpg', N'Content/InstaBig2142014122559326.jpg', 12, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/2011b584c92711e394e90002c9cf0568_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (53, 13, N'Waaaaaaaaaaaaaahhhh â™¡
I like youuuuuuu na James Reid â™¥â™¥â™¥
#crossSandford #sokilig #whysoHot #DnP', N'Content/InstaThumb2142014122559741.jpg', N'Content/InstaBig21420141226044.jpg', 13, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/2bc057d8c92611e3aa640002c9dbdc7e_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (54, 14, N'Diary Ng Panget Castâœ¨ #diaryngpanget #dnp #andreparas #yassipressman #nadinelustre #jamesreid #teameyoss #teamlorhad #jadine #yandre #fab4 #cinegang', N'Content/InstaThumb214201412260343.jpg', N'Content/InstaBig214201412260639.jpg', 14, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-e.ak.instagram.com/hphotos-ak-prn/10249304_778537588825964_955313542_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (56, 16, N'Heller
#DNP#afternoonðŸ˜‰', N'Content/InstaThumb214201412261442.jpg', N'Content/InstaBig214201412261757.jpg', 16, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/46feaf1ac92411e38ee224be059cdf80_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (57, 17, N'So what happened to #100happydaysofkea? I don''t know either. I guess I got too caught up with my recent #DnP fever. ðŸ’” the important thing is I''m fairly happy and very thankful of all God''s blessings to me. Everyday will be a happy day, regardless if I''m able to post it out here or not. Loving life because it''s worth living â™¥', N'Content/InstaThumb214201412262133.jpg', N'Content/InstaBig214201412262419.jpg', 17, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage8.ak.instagram.com/5dee9100c92311e3a3b00002c9e04a24_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (58, 18, N'Girl''s ideal guy: handsome and funnyðŸ˜»ðŸ˜½ #bromance #diaryngpanget #dnp #jamesreid #andreparas', N'Content/InstaThumb214201412262952.jpg', N'Content/InstaBig214201412263108.jpg', 18, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/8b86df82c92311e3a2190002c9e185c6_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (59, 19, N'Watching Diary Ng Panget together with @zairabotz Melodz & Rae :) #DiaryNgPanget #movie #DNP #marathon', N'Content/InstaThumb214201412263308.jpg', N'Content/InstaBig214201412263468.jpg', 19, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/cb7e1412c92211e3bbf70002c9e26c1a_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (60, 20, N'Andre Paras is the awesomest actor ever!ðŸ‘Š #andreparas #basketball #feastofthebeast #jamesreid #dnp', N'Content/InstaThumb214201412263965.jpg', N'Content/InstaBig214201412264256.jpg', 20, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/2a05d03ec92211e394dc0002c950d154_8.jpg')
SET IDENTITY_INSERT [dbo].[Instagram] OFF
SET IDENTITY_INSERT [dbo].[InstagramImage] ON 

INSERT [dbo].[InstagramImage] ([ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (1, N'Lory! @yassipressman #YassiPressman #DNP', N'Content/InstaThumb2142014122554681.jpg', N'Content/InstaBig2142014122554933.jpg', 7, 1, CAST(0x0000A31400000000 AS DateTime), NULL)
INSERT [dbo].[InstagramImage] ([ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (2, N'Look How sweet they are â™¥
-Admin Aviary
 #JaDinenatics#reidxjames#nadzlustre#dnp#diaryngpanget#jadineloveteam#Jadine#love#follow4follow#followback#f4f#alwaysfollowback#followme#followsback#couples', N'Content/InstaThumb214201412260954.jpg', N'Content/InstaBig214201412261109.jpg', 15, 1, CAST(0x0000A31400000000 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[InstagramImage] OFF
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'ar', N'Arabic', 2)
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'en', N'English', 1)
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'tr', N'Turkish', 3)
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'zh', N'Chines', 4)
SET IDENTITY_INSERT [dbo].[List] ON 

INSERT [dbo].[List] ([ListID], [Category], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, N'All', N'test nexa', N'test nexa test nexa test nexa test nexa ', N'<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>

<p>&nbsp;</p>

<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>

<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>
', NULL, NULL, N'test nexa ', N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A31300CB78AC AS DateTime), 1, N'en')
SET IDENTITY_INSERT [dbo].[List] OFF
SET IDENTITY_INSERT [dbo].[List_Event] ON 

INSERT [dbo].[List_Event] ([EventID], [CategoryID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, NULL, N'Private Label Middle East', N'www.privatelabelmiddleeast.com
01 - 03 Oct', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C0101817C AS DateTime), 2, N'en')
INSERT [dbo].[List_Event] ([EventID], [CategoryID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (6, NULL, N'Private Label Middle East', N'www.privatelabelmiddleeast.com
01 - 03 Oct', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C0100C4D0 AS DateTime), 2, N'ar')
SET IDENTITY_INSERT [dbo].[List_Event] OFF
SET IDENTITY_INSERT [dbo].[List_Event_Details] ON 

INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (1, 9, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (2, 2, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A26B00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (3, 3, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (4, 4, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (5, 5, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (6, 6, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (7, 1, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[List_Event_Details] OFF
SET IDENTITY_INSERT [dbo].[List_News] ON 

INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (2, N'News 01', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
', N'Content/News-01-Small1542014124216.jpg', N'Content/News-01-Big1542014124216.jpg', NULL, NULL, 0, 1, 1, CAST(0x0000A30E00D15CE0 AS DateTime), 1, N'en', CAST(0x0000A2BF00000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (3, N'News 02', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-02-Small2812014192157.jpg', N'Content/News-02-Big2812014192157.jpg', NULL, NULL, 0, 2, 1, CAST(0x0000A2FD00CFAC74 AS DateTime), 2, N'en', CAST(0x0000A2C100000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (4, N'News 03', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>', N'Content/News-03-Small2812014192230.jpg', N'Content/News-03-Big2812014192230.jpg', NULL, NULL, 1, 3, 1, CAST(0x0000A2FD00CD5CA8 AS DateTime), 3, N'en', CAST(0x0000A2C000000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (5, N'News 03', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-03-Small2812014192230.jpg', N'Content/News-03-Big2812014192230.jpg', NULL, NULL, 0, 3, 1, CAST(0x0000A2FD00CF8118 AS DateTime), 3, N'ar', CAST(0x0000A2EC00000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (6, N'News 02', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-02-Small2812014192157.jpg', N'Content/News-02-Big2812014192157.jpg', NULL, NULL, 0, 2, 1, CAST(0x0000A2C600071BEC AS DateTime), 2, N'ar', NULL)
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (7, N'News 01', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-01-Small2812014192118.jpg', N'Content/News-01-Big2812014192118.jpg', NULL, NULL, 0, 1, 1, CAST(0x0000A2C6000727A4 AS DateTime), 1, N'ar', NULL)
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (8, N'News 03', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-03-Small2812014192230.jpg', N'Content/News-03-Big2812014192230.jpg', NULL, NULL, 0, 3, 1, CAST(0x0000A2C60022AF4C AS DateTime), 3, N'tr', CAST(0x0000A2C000000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (9, N'News 02', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-02-Small2812014192157.jpg', N'Content/News-02-Big2812014192157.jpg', NULL, NULL, 0, 2, 1, CAST(0x0000A2C60022BE88 AS DateTime), 2, N'tr', CAST(0x0000A2C200000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (10, N'News 01', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-01-Small2812014192118.jpg', N'Content/News-01-Big2812014192118.jpg', NULL, NULL, 0, 1, 1, CAST(0x0000A2C60022C914 AS DateTime), 1, N'tr', CAST(0x0000A2BF00000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (11, N'News 03', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-03-Small2812014192230.jpg', N'Content/News-03-Big2812014192230.jpg', NULL, NULL, 0, 3, 1, CAST(0x0000A2C60022D97C AS DateTime), 3, N'zh', CAST(0x0000A2C000000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (12, N'News 02', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-02-Small2812014192157.jpg', N'Content/News-02-Big2812014192157.jpg', NULL, NULL, 0, 2, 1, CAST(0x0000A2C60022E084 AS DateTime), 2, N'zh', CAST(0x0000A2C200000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (13, N'News 01', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-01-Small2812014192118.jpg', N'Content/News-01-Big2812014192118.jpg', NULL, NULL, 0, 1, 1, CAST(0x0000A2C60022E8B8 AS DateTime), 1, N'zh', CAST(0x0000A2BF00000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (15, N'News 03', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>', N'Content/News-03-Small2812014192230.jpg', N'Content/News-03-Big2812014192230.jpg', NULL, NULL, 1, 4, 1, CAST(0x0000A2FD00CE958C AS DateTime), 3, N'ar', CAST(0x0000A2C000000000 AS DateTime))
INSERT [dbo].[List_News] ([NewsID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [ReportDate]) VALUES (16, N'News 02', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
', N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', N'Content/News-02-Small2812014192157.jpg', N'Content/News-02-Big2812014192157.jpg', NULL, NULL, 0, 2, 1, CAST(0x0000A2FD00CFCC18 AS DateTime), 2, N'ar', CAST(0x0000A2C100000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[List_News] OFF
SET IDENTITY_INSERT [dbo].[List_Partners] ON 

INSERT [dbo].[List_Partners] ([PartnerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Email], [Phone], [Mobile], [Fax], [Address], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, N'Rhythmlink', N'With a large portfolio of partners and brands, we have exclusive relationships with American and European suppliers and manufacturers and are thus able to source reliable products at great value. Our business model is based on earning the exclusive right to represent and develop our manufacturers’ brands in our active markets and creating room for their success.', NULL, N'Content/Rhythmlink-Small2952014152230.jpg', NULL, N'Rhythmlink', N'sasa@ss.com', NULL, NULL, NULL, NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A33A00FD68A8 AS DateTime), 1, N'en')
SET IDENTITY_INSERT [dbo].[List_Partners] OFF
SET IDENTITY_INSERT [dbo].[List_Service] ON 

INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (2, N'Test Service 1', N'Contrary to popular belief', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.<img alt="" src="http://localhost:14431/ApolloCMSNew/userfiles/Service2412012161639.jpg" /></p>
', N'Content/Test-Service-1-Small1542014151811.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00FC2FC4 AS DateTime), 2, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (3, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C3819C AS DateTime), 3, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (4, N'Test Service 1', N'Contrary to popular belief,', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C36DB0 AS DateTime), 4, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (5, N'Test Service 1', N'Contrary to popular belief,  ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C33B4C AS DateTime), 5, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (6, N'Test Service 1', N'Contrary to popular belief,', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C32D3C AS DateTime), 6, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (7, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A09E0109B0CC AS DateTime), 7, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (8, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A30E00C35190 AS DateTime), 8, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (10, N'Test Service 1.', N'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in ', N'<p><img alt="" src="http://localhost:21946/NexaCms/Admin/Content/UserFiles/Accessories-Test-Product-1-Big5120141598.jpg" style="height:538px; width:552px" />&nbsp; Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.<img alt="" src="http://localhost:14431/ApolloCMSNew/userfiles/Service2412012161639.jpg" /></p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A30E00C35D48 AS DateTime), 10, N'en')
SET IDENTITY_INSERT [dbo].[List_Service] OFF
SET IDENTITY_INSERT [dbo].[List_Testimonial] ON 

INSERT [dbo].[List_Testimonial] ([TestimonialID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [WrittenBy], [Designation], [Country], [SortIndex], [Featured], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (2, NULL, N'1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt urna neque, volutpat ultrices leo molestie nec.', NULL, NULL, NULL, NULL, N'Adreano', N'Tester', N'UAE', 1, 0, 1, CAST(0x0000A31700DA604C AS DateTime), 2, N'en')
INSERT [dbo].[List_Testimonial] ([TestimonialID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [WrittenBy], [Designation], [Country], [SortIndex], [Featured], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (5, NULL, N'List_TestimonialList_TestimonialList_TestimonialList_Testimonial', NULL, NULL, NULL, NULL, N'WVSS', N'Tester', N'UAE', 2, 0, 1, CAST(0x0000A31700DB0DBC AS DateTime), 3, N'en')
SET IDENTITY_INSERT [dbo].[List_Testimonial] OFF
SET IDENTITY_INSERT [dbo].[List1] ON 

INSERT [dbo].[List1] ([ListID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [Link], [Featured], [MasterID], [Lang]) VALUES (5, N'list 1', N'test nexa test nexa test nexa ', N'<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>

<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>
', NULL, NULL, N'test nexa ', 1, 1, CAST(0x0000A313012283E0 AS DateTime), N'http://www.wvss.net', 1, 1, N'en')
SET IDENTITY_INSERT [dbo].[List1] OFF
SET IDENTITY_INSERT [dbo].[List1_Child] ON 

INSERT [dbo].[List1_Child] ([ChildID], [ListID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [Link], [Featured], [MasterID], [Lang]) VALUES (1, 1, N'List1_Child Item 11', N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book1', N'<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.1</p>', N'Content/List1ChSmall187201294017.jpg', N'Content/List1ChBig187201294017.jpg', NULL, 2, 1, CAST(0x0000A092009F900C AS DateTime), N'http://www.wvss.net1', 0, NULL, NULL)
INSERT [dbo].[List1_Child] ([ChildID], [ListID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [Link], [Featured], [MasterID], [Lang]) VALUES (2, 5, N'test nexa', N'test nexa test nexa test nexa test nexa test nexa ', N'<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>

<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>

<p>test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;test nexa&nbsp;</p>
', NULL, NULL, N'test nexa ', 1, 1, CAST(0x0000A31301309890 AS DateTime), N'http://www.wvss.net', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[List1_Child] OFF
SET IDENTITY_INSERT [dbo].[NewsletterSubscription] ON 

INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (1, N'Debashis', N'debashis.chowdhury@wvss.net', NULL, NULL, NULL, CAST(0x0000A19F01064C70 AS DateTime))
INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (2, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A30E00000000 AS DateTime))
INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (3, NULL, N'debashis.chowdhury1@wvss.net', NULL, NULL, NULL, CAST(0x0000A30E00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[NewsletterSubscription] OFF
SET IDENTITY_INSERT [dbo].[Reg_Member_] ON 

INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (1, N'saiful', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09C00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (2, N'debasis', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09C00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (3, N'mayedul', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (4, N'rajib roy', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (6, N'ferry', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (7, N'francis', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09E00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Reg_Member_] OFF
SET IDENTITY_INSERT [dbo].[Settings_Dimention] ON 

INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (1, N'portfolio', N'nexa', 352, 199, 618, 349, 600, 442)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (2, N'News', N'nexa', 184, 118, 184, 118, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (3, N'Banner', N'Home', 415, 200, 730, 351, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (4, N'Blog', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (5, N'Career', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (6, N'Event', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (7, N'Gallery', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (8, N'GalleryItem', N'nexa', 108, 67, 532, 399, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (9, N'Hotel', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (10, N'Hotel', N'Image', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (11, N'List', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (12, N'List1', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (13, N'List1Child', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (14, N'List_Outlet', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (15, N'List_OutletBanner', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (16, N'List_OutletGallery', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (17, N'List_OutletGalleryItem', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (18, N'Profile', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (19, N'Service', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (20, N'SpecialOffer', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (21, N'Testimonial', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (22, N'Video', N'', NULL, NULL, NULL, NULL, 400, 600)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (23, N'client', N'nexa', 148, 101, 440, 300, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (24, N'casestudy', N'nexa', 245, 100, 1152, 469, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (25, N'casestudydetails', N'nexa', 130, 100, 452, 350, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (26, N'Webservice', N'nexa', 334, 176, 570, 300, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (27, N'SocialMedia', N'nexa', 334, 176, 570, 300, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Settings_Dimention] OFF
SET IDENTITY_INSERT [dbo].[Settings_Module] ON 

INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (1, N'HTML', 1, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (2, N'Banner', 2, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (3, N'Gallery', 3, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (4, N'News', 4, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (5, N'Profile', 5, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (6, N'Event', 6, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (7, N'Service', 7, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (8, N'Testimonial', 8, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (9, N'List', 9, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (10, N'List1', 10, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (11, N'SEO', 11, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (12, N'Contact', 12, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (13, N'NewsSub', 13, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (14, N'RegistrationForm', 14, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (16, N'SpecialOffer', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (17, N'Blog', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (18, N'FAQ', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (19, N'Career', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (20, N'NewsletterSubscription', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (21, N'DBBackup', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (23, N'Login', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (24, N'VideoGallery', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (25, N'GoogleAnalytics', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (26, N'SEOReport', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (27, N'AlertMessage', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (29, N'Hotel', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (30, N'Outlet', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (31, N'Portfolio', 15, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (32, N'WebServices', 16, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (33, N'SocialMedia', 17, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (34, N'CompanyProduct', 18, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (35, N'Facebook', 19, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (36, N'CaseStudy', 20, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (37, N'Client', 21, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (38, N'Management', 22, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (39, N'InstagramGallery', 23, 1)
SET IDENTITY_INSERT [dbo].[Settings_Module] OFF
SET IDENTITY_INSERT [dbo].[SpecialOffer] ON 

INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (1, N'test special offer', N'test special offer test special offer test special offer', N'<p>&nbsp;test special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offer</p>', N'Content/sp-offer-Small1092012104244.jpg', N'Content/sp-offer-Big1092012104322.jpg', N'sp offer', NULL, 1, 1, 1, CAST(0x0000A0C800B0B4B8 AS DateTime), CAST(0x0000A0C200000000 AS DateTime), CAST(0x0000A0D200000000 AS DateTime), NULL, NULL)
INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (4, N'Oceanic Khorfakkan Resort & Spa Factsheet (English)', N'Oceanic Khorfakkan Resort & Spa Factsheet (English)Oceanic Khorfakkan Resort & Spa Factsheet (English)', N'<p>ss</p>
', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A37F0113B02C AS DateTime), NULL, NULL, 1, N'en')
INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (5, N'Oceanic Khorfakkan Resort & Spa Factsheet (arabic)', N'Oceanic Khorfakkan Resort & Spa Factsheet (English)Oceanic Khorfakkan Resort & Spa Factsheet (English)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C010BEB80 AS DateTime), NULL, NULL, 1, N'ar')
SET IDENTITY_INSERT [dbo].[SpecialOffer] OFF
SET IDENTITY_INSERT [dbo].[VideoGallery] ON 

INSERT [dbo].[VideoGallery] ([VideoGalleryID], [CategoryID], [Title], [SmallDetails], [VideoOriginalURL], [VideoImageURL], [VideoEmbedCode], [Featured], [SortIndex], [Status], [LastUpdated]) VALUES (1, 0, N'Test video', N'Test video', N'http://www.youtube.com/watch?feature=player_embedded&v=JqSz7Esa098', N'http://img.youtube.com/vi/JqSz7Esa098/0.jpg', N'<iframe title="YouTube video player" src="http://www.youtube.com/embed/JqSz7Esa098?wmode=opaque" allowfullscreen="" width="447" frameborder="0" height="335"></iframe>', 1, 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[VideoGallery] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[10] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Contact"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "NewsletterSubscription"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 135
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2580
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ContactAndNewsletterCombineResult'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ContactAndNewsletterCombineResult'
GO
ALTER DATABASE [nexacms] SET  READ_WRITE 
GO
