﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="StyleGroups.aspx.vb" Inherits="StyleGroups" %>

<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Style Groups</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>STYLE GROUPS</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">

        <h2 class="titleCenter">
            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="8" />
        </h2>

    </section>
    <!-- Inner Main Wrapper Section -->


    <!-- Style Group Section -->
    <section class="styleGroupsSection">

        <section class="container">

            <div class="contentContainer">

                <ul class="bannerImgListings">

                   <%= GetGroups() %>

                </ul>

            </div>

        </section>

    </section>
    <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="34"/>
    <!-- Style Group Section -->
</asp:Content>

