﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Src="~/CustomControl/UserBanner.ascx" TagPrefix="uc1" TagName="UserBanner" %>
<%@ Register Src="~/CustomControl/HTML_ActionBox.ascx" TagPrefix="uc1" TagName="HTML_ActionBox" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Main Banner Section -->
    <section class="mainBannerSection">

        <!-- Paging Container -->
        <div class="pagingContainer">
            <span class="slide-current-slide">1</span>
            <span class="slide-total-slides">2</span>
        </div>
        <!-- Paging Container -->

        <ul class="slides">
           <uc1:UserBanner runat="server" ID="UserBanner" SectionName="Home" />
           
        </ul>

    </section>
    <!-- Main Banner Section -->


    <!-- Call to Action Box Container -->
    <%--<section class="callToActionBoxContainer">

        <section class="container">

            <ul class="callToActionList">

                <li>
                    <h2>
                        <uc1:HTML_ActionBox runat="server" ID="HTML_ActionBox" HTMLID="2" />
                    </h2>
                    <i class="cartIcon"></i>
                </li>

                <li>
                    <h2>
                         <uc1:HTML_ActionBox runat="server" ID="HTML_ActionBox1" HTMLID="3" />
                        
                    </h2>
                    <i class="locateIcon"></i>
                </li>

            </ul>

        </section>        

    </section>--%>
    <!-- Call to Action Box Container -->


    <!-- Sale and About Section -->
    <section class="saleNaboutSection">

        <section class="container">

            <!-- Sale Section -->
            <article class="saleSection">

               <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3"  HTMLID="23"/>
                
            </article>
            <!-- Sale Section -->

            <!-- About Section -->
            <article class="aboutSection">
                <div class="content">
                    
                    <h2>About IDdesign</h2>
                    
                    <p>
                               <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="10" />
                    </p>

                    <a class="readMore" href='<%= domainName & "AboutUs"%>'>+ Read More</a>

                    <h6>
                         <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="21" />
                    </h6>

                </div>
            </article>
            <!-- About Section -->

        </section>

    </section>
    <!-- Sale and About Section -->


    <!-- ID Design Collection Section -->
    <section class="idDesignCollectionContainer">

          <section class="container">

                <div class="collectionConatiner">

                    <h2>IDdesign Collections</h2>

                    <p>
                           <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" HTMLID="7" />
                    </p>

                    <!-- Collection Listings -->
                    <ul class="collectionListings">

                        <%= getCollections() %>

                    </ul>
                    <!-- Collection Listings -->

                </div>
              
          </section>  

    </section>
    <!-- ID Design Collection Section -->


    <!-- Shop Every Day Section -->
    <section class="shopEverydaySection">

            <%--<section class="container">

                <!-- Bordered Title -->
                <div class="borderedTilte">
                    <h2>Shop every day Values</h2>
                </div>
                <!-- Bordered Title -->

                <!-- Shop Every day Listings -->
                <div class="row">

                    <ul class="shopEverydayListing">

                        <li class="col-md-4 col-sm-4">
                           <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="18" />
                        </li>

                        <li class="col-md-4 col-sm-4">
                             <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="19" />
                        </li>

                        <li class="col-md-4 col-sm-4">
                             <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" HTMLID="20" />
                        </li>

                    </ul>

                </div>
                <!-- Shop Every day Listings -->

            </section>--%>

    </section>
    <!-- Shop Every Day Section -->


    <!-- New Arrival Slider Section -->
    <section class="newArrivalSliderContainer">


        <div class="container">

            <!-- Bordered Title -->
            <div class="borderedTilte">
                <h2>NEW ARRIVALS</h2>
            </div>
            <!-- Bordered Title -->

            <!-- New Arrival Slider -->
            <div class="newArrivalSlider">

                <ul class="slides">

                   <%=NewArrival() %>

                </ul>

            </div>
            <!-- New Arrival Slider -->

            <!-- Bordered Title -->
            <div class="borderedTilte small">
                <h2><a href='<%= domainName & "NewArrivals" %>'>View all</a></h2>
            </div>
            <!-- Bordered Title -->
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="31" />
        </div>


    </section>

    <!-- New Arrival Slider Section -->
      <!-- Assistance and Lets Talk Section -->
        <section class="assistanceNletsTalkSectionContainer">

            <section class="container">

                <div class="row">

                    <!-- Assistance Section -->
                    <div class="col-md-8">
                        <article class="assistanceSection">

                            <div class="assistanceFormContainer">

                                <div class="content">

                                    <div class="row">

                                        <!-- left Panel -->
                                        <div class="col-md-6">
                                            <h2>Interior Design Assistance</h2>
                                            <p>IDdesign’s interior design assistance is available at all our stores. Just ask for a designer, and you will be guided to one.</p>
                                        </div>
                                        <!-- left Panel -->

                                        <!-- Right Panel -->
                                        <div class="col-md-6">

                                            <div class="row marginBtm15">

                                                <div class="col-md-6">
                                                    <!-- Normal Input -->
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                                            Enabled="True" TargetControlID="txtName" WatermarkText="Full Name">
                                                        </cc1:TextBoxWatermarkExtender>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtName" ValidationGroup="form2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <!-- Normal Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Normal Input -->
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txtEmail1" class="form-control" runat="server"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                                            Enabled="True" TargetControlID="txtEmail1" WatermarkText="Email Address">
                                                        </cc1:TextBoxWatermarkExtender>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail1" ValidationGroup="form2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail1"
                                                            ValidationGroup="form2" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                    </div>
                                                    <!-- Normal Input -->
                                                </div>

                                            </div>

                                            <div class="row marginBtm15">

                                                <div class="col-md-12">
                                                    <!--marginBtm15 Normal Input -->
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txtmsg" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server"
                                                            Enabled="True" TargetControlID="txtmsg" WatermarkText="Type your message for Us">
                                                        </cc1:TextBoxWatermarkExtender>

                                                    </div>
                                                    <!-- Normal Input -->
                                                </div>

                                            </div>
                                            <div class="row">
                                                <uc1:MyCaptcha runat="server" vgroup="form2" ID="MyCaptcha" />
                                                &nbsp;&nbsp;
                                                    <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>

                                            </div>
                                            <asp:Button ID="btnSend" class="sendButton" ValidationGroup="form2" runat="server" Text="send" />
                                            <asp:SqlDataSource ID="sdsDAssistance" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                InsertCommand="INSERT INTO [Contact] ([FullName], [Email], [ContactFor], [Message], [ContactDate]) VALUES (@FullName, @Email, @ContactFor, @Message, @ContactDate)">

                                                <InsertParameters>
                                                    <asp:ControlParameter ControlID="txtName" Name="FullName" PropertyName="Text" Type="String" />
                                                    <asp:ControlParameter ControlID="txtEmail1" Name="Email" PropertyName="Text" Type="String" />
                                                    <asp:Parameter Name="ContactFor" Type="String" DefaultValue="Interior" />
                                                    <asp:ControlParameter ControlID="txtmsg" Name="Message" PropertyName="Text" Type="String" />
                                                    <asp:ControlParameter ControlID="hdnDate" Name="ContactDate" PropertyName="Value" Type="DateTime" />
                                                </InsertParameters>

                                            </asp:SqlDataSource>
                                        </div>
                                        <!-- Right Panel -->

                                    </div>

                                </div>

                            </div>

                        </article>
                    </div>
                    <!-- Assistance Section -->

                    <!-- Lets Talk Style -->
                    <div class="col-md-4">

                        <article class="letsTalkSection">

                            <h2>Let’s Talk Style!</h2>
                            <p>
                                Connect with us to get the latest design news & 
                       enter contests.
                            </p>

                            <!-- Normal Input -->
                            <div class="form-group">
                                <asp:TextBox ID="txtEmail" class="form-control" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                    Enabled="True" TargetControlID="txtEmail" WatermarkText="Signup via Mobile...">
                                </cc1:TextBoxWatermarkExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEmail" ValidationGroup="form1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                
                                <asp:Label ID="lblmsg" runat="server" Visible="false" Text=""></asp:Label>
                            </div>
                            <!-- Normal Input -->
                            <asp:HiddenField ID="hdnDate" runat="server" />
                            <asp:SqlDataSource ID="sdsNews" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                                InsertCommand="INSERT INTO [NewsletterSubscription] ([Mobile], [SubscriptionDate]) VALUES (@Mobile, @SubscriptionDate)">

                                <InsertParameters>
                                    <asp:ControlParameter ControlID="txtEmail" Name="Mobile" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="hdnDate" Name="SubscriptionDate" PropertyName="Value" Type="DateTime" />
                                </InsertParameters>

                            </asp:SqlDataSource>
                            <asp:Button ID="btnNewsLetter" ValidationGroup="form1" class="subscribeBttn" runat="server" Text="Sign Up" />

                        </article>

                    </div>
                    <!-- Lets Talk Style -->

                </div>

            </section>

        </section>
        <!-- Assistance and Lets Talk Section -->
</asp:Content>

