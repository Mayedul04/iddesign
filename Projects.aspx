﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Projects.aspx.vb" Inherits="Projects" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">Project Division</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Project Division</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <div class="imgHold">
         <asp:Image ID="ImgProject" runat="server" />
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    
            <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
                    <h2>Our Services</h2>

                    <!-- Service Listings -->
                    <ul class="serviceListings">
                       <%= GetProjects() %>
                    </ul>
                    <!-- Service Listings -->

                </div>

            </div>

        </section>

        <!-- Social Icons -->
        <%--  <asp:Literal ID="lblSocial" runat="server"></asp:Literal>--%>
        <!-- Social Icons -->
     <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

