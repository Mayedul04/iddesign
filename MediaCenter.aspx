﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MediaCenter.aspx.vb" Inherits="MediaCenter" %>

<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%=fbUrl%>

    <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Media Centre</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Media Centre</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">

        <section class="innerMainContentWrapper">

            <div class="row">

                <!-- IMage Block Listings -->
                <ul class="imageBlockListings">

                    <li class="col-md-6 col-sm-6">
                        <div class="content">
                            <div class="imgHold">
                                <a href='<%= domainName & "PhotoGallery" %>'>
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="13" />
                                </a>
                            </div>
                            <h2><a href='<%= domainName & "PhotoGallery" %>'>Photo Gallery <span>
                                <img src="<%= domainName & "ui/media/dist/icons/title-arrow.png"%>" alt=""></span></a></h2>
                        </div>
                    </li>

                    <li class="col-md-6 col-sm-6">
                        <div class="content">
                            <div class="imgHold">
                                <a href='<%= domainName & "VideoGallery" %>'>
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="14" />
                                </a>
                            </div>
                            <h2><a href='<%= domainName & "VideoGallery" %>'>Video Gallery <span>
                                <img src="<%= domainName & "ui/media/dist/icons/title-arrow.png"%>" alt=""></span></a></h2>
                        </div>
                    </li>
                    <% If getNewsNumber() > 0 Then%>
                    <li class="col-md-6 col-sm-6">
                        <div class="content">
                            <div class="imgHold">
                                <a href='<%= domainName & "News" %>'>
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="15" />
                                </a>
                            </div>
                            <h2><a href='<%= domainName & "News" %>'>News <span>
                                <img src="<%= domainName & "ui/media/dist/icons/title-arrow.png"%>" alt=""></span></a></h2>
                        </div>
                    </li>
                    <% End If%>
                     <% If getTestimonialNumber() > 0 Then%>
                    <li class="col-md-6 col-sm-6">
                        <div class="content">
                            <div class="imgHold">
                                <a href='<%= domainName & "Testimonials" %>'>
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="16" />
                                </a>
                            </div>
                            <h2><a href='<%= domainName & "Testimonials" %>'>Testimonials <span>
                                <img src="<%=domainName & "ui/media/dist/icons/title-arrow.png" %>" alt=""></span></a></h2>
                        </div>
                    </li>
                    <% End If %>
                    <%--<li class="col-md-6 norMargin col-sm-6">
                        <div class="content">
                            <div class="imgHold">
                                <a href='<%= domainName & "News/Press" %>'>
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage4" HTMLID="17" />
                                </a>
                            </div>
                            <h2><a href='<%= domainName & "News/Press" %>'>Press Release <span>
                                <img src="<%=domainName & "ui/media/dist/icons/title-arrow.png" %>" alt=""></span></a></h2>
                        </div>
                    </li>--%>

                </ul>
                <!-- IMage Block Listings -->

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="36" />
                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                   <%-- <%= socialShare() %>--%>
                    <!-- Social Icons -->
                </div>



            </div>
            <!-- Social and Pagination -->

        </section>



    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

