﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Promotion.aspx.vb" Inherits="Promotion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="448722205295374" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "Promotions" %>'>Promotions</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;"><%= title %></a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2><%= title %></h2>

<asp:HiddenField ID="hdnMasterID" runat="server" />
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-6 col-sm-6">
                    
                    <!-- Product Detail Slider Section -->
                    <div class="productDetailsSliderSection">

                        <!-- Product Main Image Section -->
                        <div class="productMainimgSection">
                            
                            <ul class="slides">

                               <%=Gallery() %>

                            </ul>

                        </div>
                        <!-- Product Main Image Section -->

                        <!-- Product Main Image Section -->
                        <div class="productThumbimgSection">
                            
                            <ul class="slides">

                                <%= Gallery() %>

                            </ul>

                        </div>
                        <!-- Product Main Image Section -->

                    </div>
                    <!-- Product Detail Slider Section -->

                </div>

                <div class="col-md-6 col-sm-6">
                    
                    <h2><%= title %></h2>

                    <asp:Literal ID="lblDetails" runat="server"></asp:Literal>

                    <div class="row">
                       <%-- <div class="col-md-5">
                            <a href='<%= domainName & "Enquiry/Promo/" & Page.RouteData.Values("id") %>' class="readMoreBttn" id="enquirenow" data-fancybox-type="iframe">enquire now <span><img alt="" src='<%= domainName & "ui/media/dist/icons/readmore-white-arrow.png" %>'></span></a>
                        </div>--%>

                        <div class="col-md-7">

                            <!-- Social Icons -->
                              <asp:Literal ID="lblSocial" Visible="false" runat="server"></asp:Literal>
                            <!-- Social Icons -->

                        </div>
                    </div>

                </div>

            </div>

        </section>

        <!-- You May Also Like -->
        <section class="youMayLikContentWrapper">
            
            <h2>You may also like</h2>

            <div class="row">
                <!-- You May Like Listing -->
                <ul class="youMayLikeListing">
                    
                    <%= OtherPromotions() %>

                </ul>
                <!-- You May Like Listing -->
            </div>

        </section>
        <!-- You May Also Like -->


    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

