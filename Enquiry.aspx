﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Enquiry.aspx.vb" Inherits="Enquiry" %>

<%@ Register Src="~/MyCaptcha1.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ID Design</title>

    <!-- Bootstrap -->
    <link href="<%= domainName & "ui/style/css/bootstrap.css" %>" rel="stylesheet">
    <link href="<%= domainName & "ui/style/css/jquery.fancybox.css" %>" rel="stylesheet">
    <link rel="shortcut icon" href="<%= domainName & "ui/media/dist/favicon.ico" %>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="innerMainContentWrapper" style="margin: 0px; padding: 0px;">
            <asp:HiddenField ID="hdnStyleID" runat="server" />
            <asp:HiddenField ID="hdnPromoID" runat="server" />
            <asp:HiddenField ID="hdnCollectionID" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnProductID" runat="server" />
            <div class="contactFormContainer">
                <h2>ENQUIRE NOW</h2>

                <div class="form-horizontal">



                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">
                            Name
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtName" ValidationGroup="form2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </label>
                        <div class="col-xs-10">
                            <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">
                            Email
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail1" ValidationGroup="form2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail1"
                                ValidationGroup="form2" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </label>
                        <div class="col-xs-10">
                            <asp:TextBox ID="txtEmail1" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">
                            Phone
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtPhone" ValidationGroup="form2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </label>
                        <div class="col-xs-10">
                            <asp:TextBox ID="txtPhone" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">
                            
                        <asp:Label ID="lblCategory" runat="server" Text="Product"></asp:Label>
                        </label>
                        <div class="col-xs-10">
                            <asp:TextBox ID="txtProduct" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Panel ID="pnlStyle" Visible="false" runat="server">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-xs-2 control-label">
                                Style & Collection
                        
                            </label>
                            <div class="col-xs-10">
                                <asp:TextBox ID="txtCollectionStyle" ReadOnly="true" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">
                            Message
                        </label>
                        <div class="col-xs-10">
                            <asp:TextBox ID="txtmsg" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-2 control-label">&nbsp</label>

                        <div class="col-xs-10">
                            <uc1:MyCaptcha runat="server" vgroup="form2" ID="MyCaptcha" />
                            <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                        </div>
                    </div>

                    <div class="col-xs-5 col-xs-offset-2">
                        <asp:Button ID="btnenq" class="sendEnquiryBttn" ValidationGroup="form2" runat="server" Text="Send Enquiry" />

                    </div>
                    <asp:SqlDataSource ID="sdsEnq" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        InsertCommand="INSERT INTO [ProductEnquiry] ([ProductID], [StyleID],[PromoID], [CollectionID], [Name], [Email], [Phone], [Message], [Date1]) VALUES (@ProductID, @StyleID, @PromoID, @CollectionID, @Name, @Email, @Phone, @Messsage, @Date1)">

                        <InsertParameters>
                            <asp:ControlParameter ControlID="hdnProductID" Name="ProductID" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="hdnStyleID" Name="StyleID" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="hdnCollectionID" Name="CollectionID" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="txtName" Name="Name" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtEmail1" Name="Email" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtmsg" Name="Messsage" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="Date1" PropertyName="Value" />
                            <asp:ControlParameter ControlID="hdnPromoID" Name="PromoID" PropertyName="Value" />
                        </InsertParameters>

                    </asp:SqlDataSource>
                </div>
            </div>
    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='<%= domainName & "ui/js/dist/jquery-ui-1.10.4.custom.min.js" %>'></script>
    <script src='<%= domainName & "ui/js/std/uniform.min.js" %>'></script>
    <script src='<%= domainName & "ui/js/std/bootstrap.js" %>'></script>
    <script src='<%= domainName & "ui/js/std/crossbrowser-min.js" %>'></script>
    <script src='<%= domainName & "ui/js/dist/jquery.flexslider.js" %>'></script>
    <script src='<%= domainName & "ui/js/dist/jquery.fancybox.js" %>'></script>
    <script src='<%= domainName & "ui/js/dist/custom.js" %>'></script>
    <script type="text/javascript">

        $('.sendEnquiryBttn').click(function () {
            if (Page_ClientValidate("form2")) {
                window.opener.location.href = '<%= domainName + "Thanks" %>';
                parent.jQuery.fancybox.close();
            }
            //if (document.getElementById('_ispostback').value == 'True') {
            //    parent.jQuery.fancybox.close();
            //}
        });
    </script>
</body>

</html>

