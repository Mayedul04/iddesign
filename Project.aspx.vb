﻿Imports System.Data.SqlClient

Partial Class Project
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List_Project where ProjectID=@ProjectID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ProjectID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString()
            hdnMasterID.Value = reader("MasterID").ToString()
            lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Service/ProjectEdit.aspx?sid=" & reader("ProjectID").ToString())
            hdnID.Value = reader("ServiceID").ToString()
            DynamicSEO.PageType = "List_Project"
            DynamicSEO.PageID = reader("ProjectID")
        End If
        cn.Close()
    End Sub
    Public Function GetProjects() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Project where List_Project.Status=1 and ServiceID=@ServiceID order by List_Project.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ServiceID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If reader("ProjectID").ToString() = Page.RouteData.Values("id") Then
                retstr += "<li class=""active""><a href=""" & domainName & "Project/" & reader("ProjectID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """><span></span>" & reader("Title").ToString() & "</a></li>"
            Else
                retstr += "<li><a href=""" & domainName & "Project/" & reader("ProjectID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """><span></span>" & reader("Title").ToString() & "</a></li>"
            End If

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function Gallery() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from CommonGallery where TableMasterID=@TableMasterID and TableName=@TableName"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int, 32).Value = hdnMasterID.Value
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 100).Value = "List_Project"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""content""><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""" width=""272""></div></div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
