﻿
Partial Class _Default
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function getCollections() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 4.* FROM List1 where List1.Status=1  order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li class=""col-md-3 col-sm-3""><div class=""imgHold"">"
            retstr += "<a class=""view"" href=""" & domainName & "Collection/" & reader("ListID") & "/" & reader("Title") & """><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """></div>"
            retstr += "<h3>" & reader("Title").ToString() & "</h3>View all collections</a>"
            retstr += Utility.showEditButton(Request, domainName & "Admin/A-List1/List1Edit.aspx?l1id=" & reader("ListID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function NewArrival() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List1_Child where List1_Child.Status=1 and List1_Child.NewArrival=1 order by List1_Child.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><div class=""content""><div class=""imgHold"">"
            retstr += "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """ width=""274""></div>"
            retstr += "<div class=""textContentContainer""><div class=""content""><div class=""discriptionTextContainer""><span class=""moreArrow""></span><h2>" & reader("Title").ToString() & "</h2>"
            '    <p>" & reader("SmallDetails").ToString() & "</p><a class=""readMore"" href=""" & domainName & "Product/" & reader("ChildID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>Read More</a>
            retstr += Utility.showEditButton(Request, domainName & "Admin/A-List1/List1ChildEdit.aspx?l1cid=" & reader("ChildID")) & "</div></div></div></div></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If (MyCaptcha.IsValid) Then
            hdnDate.Value = Date.Now()
            Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">First Name</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail1.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Message</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtmsg.Text & "</td></tr>"
            msg += "</table></td></tr></table>"
            If sdsDAssistance.Insert() > 0 Then
                Utility.SendHTTPRequestToSendgrid("IdDesign", txtEmail1.Text, "zainumry@gmail.com", "", "mayedul@digitalnexa.com", "INTERIOR DESIGN ASSISTANCE", msg)
                Response.Redirect(domainName & "Thanks")
            End If

        End If
    End Sub
    Protected Sub btnNewsLetter_Click(sender As Object, e As EventArgs) Handles btnNewsLetter.Click
        If IsExist("NewsletterSubscription", txtEmail.Text) = False Then
            hdnDate.Value = Date.Now()
            Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Mobile </td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"
            msg += "</table></td></tr></table>"
            'headoffice@iddesign.ae
            If sdsNews.Insert > 0 Then
                Utility.SendHTTPRequestToSendgrid("IdDesign", txtEmail.Text, "zainumry@gmail.com", "", "mayedul@digitalnexa.com", "Newsletter Subscription", msg)
                Response.Redirect(domainName & "Thanks")
            Else
                lblmsg.Visible = True
                lblmsg.Text = "Sorry! Something wrong."
                lblmsg.ForeColor = Drawing.Color.Red
            End If
        Else
            Response.Redirect(domainName & "Thanks/0")
        End If
    End Sub
    Public Function IsExist(ByVal tablename As String, ByVal emailid As String) As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM " & tablename & " where Mobile=@Mobile"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Mobile", Data.SqlDbType.NVarChar, 100).Value = emailid
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return True
        Else
            conn.Close()
            Return False
        End If
    End Function
End Class
