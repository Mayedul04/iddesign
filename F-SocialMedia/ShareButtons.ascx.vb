﻿
Partial Class Controls_ShareButtons
    Inherits System.Web.UI.UserControl

    Public Property URL() As String
        Get
            Return m_URL
        End Get
        Set(value As String)
            m_URL = value
        End Set
    End Property
    Private m_URL As String

    Public Property Image() As String
        Get
            Return m_Image
        End Get
        Set(value As String)
            m_Image = "http://" & Request.Url.Host & value
        End Set
    End Property
    Private m_Image As String

    Public Property ShareTitle() As String
        Get
            Return m_ShareTitle
        End Get
        Set(value As String)
            m_ShareTitle = Server.UrlEncode(value)
        End Set
    End Property
    Private m_ShareTitle As String

    Public Property SmallDescription() As String
        Get
            Return m_SmallDescription
        End Get
        Set(value As String)
            m_SmallDescription = Server.UrlEncode(value)
        End Set
    End Property
    Private m_SmallDescription As String

End Class
