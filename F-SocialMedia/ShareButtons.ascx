﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShareButtons.ascx.vb" Inherits="Controls_ShareButtons" %>


                <ul class="social-share">
                  
                  <li>
                    <a target="_blank" href="javascript:;" class="facebook" rel="nofollow"  onclick="MM_openBrWindow('https://www.facebook.com/sharer.php?s=100&p[url]=<%= URL %>&p[images][0]=<%= Image %>&p[title]=<%= ( ShareTitle) %>&p[summary]=<%= ( SmallDescription ) %>','TEXTEDITOR','scrollbars=yes,resizable=yes,width=800,height=600')"  >
                      <%--<img src="ui/media/dist/icons/facebook.png" height="30" width="30" alt="" class="first">
                      <img src="ui/media/dist/icons/facebook-hover.png" height="30" width="30" alt="" class="second">--%>
                    </a>
                  </li>
                  <li>
                    <a  target="_blank" href="javascript:;" class="twitter" rel="nofollow"  onclick="MM_openBrWindow('https://twitter.com/share?url=<%=  Server.UrlEncode( URL) %>&text=<%= ShareTitle %>','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"  >
                      <%--<img src="ui/media/dist/icons/twitter.png" height="30" width="30" alt="" class="first">
                      <img src="ui/media/dist/icons/twitter-hover.png" height="30" width="30" alt="" class="second">--%>
                    </a>
                  </li>
                  <%--<li>
                    <a href="javascript:;">
                    
                      <img src="ui/media/dist/icons/email.png" height="30" width="30" alt="" class="first">
                      <img src="ui/media/dist/icons/email-hover.png" height="30" width="30" alt="" class="second">
                    </a>
                  </li>--%>
                  <%--<li>
                    <a href="javascript:;" rel="nofollow"  class="linkedin"  onclick="MM_openBrWindow('http://www.linkedin.com/shareArticle?mini=true&url=<%= URL %>&title=<%= ( ShareTitle ) %>&summary=<%= ( SmallDescription) %>','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')" > 
                    <img src="ui/media/dist/icons/linked.png" height="30" width="30" alt="" class="first" />
                      <img src="ui/media/dist/icons/linked-hover.png" height="30" width="30" alt="" class="second" />
                    </a>

                  </li>--%>
                  <li>
                      <a  href="javascript:;" rel="nofollow" class="pinterest" onclick="MM_openBrWindow('//pinterest.com/pin/create/button/?url=<%=  URL %>&media=<%= Image %>&description=<%= ( SmallDescription) %>','TEXTEDITOR','scrollbars=yes,resizable=yes,width=800,height=600')"   data-pin-do="buttonPin" data-pin-config="none">
                          <%--<img src="ui/media/dist/icons/pintrest.png" height="30" width="30" alt="" class="first">
                          <img src="ui/media/dist/icons/pintrest-hover.png" height="30" width="30" alt="" class="second">--%>
                      </a>
                  </li>
                  
                  
                </ul>
