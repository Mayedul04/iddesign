﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="HTML.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1 class="page-title">Fixed Text</h1>
   <div class="btn-toolbar">
    
    <a href="../A-HTML/AllHTML.aspx" data-toggle="modal" class="btn">Back</a>
        <div class="btn-group">
    </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Add Fixed Text"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
   </div>
   <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
    </div>


    <!-- content -->
    <div class="well">
           <div id="myTabContent" class="tab-content">
                <asp:Panel ID="pnlTitle" runat="server">
                        <p>
                            <label>
                                Title:</label>
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSmallImage" runat="server">
                        <p>
                            <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                            <label>
                                Upload Small Image :(Width=<%= Request.QueryString("SmallImageWidth")%>; Height=<%= Request.QueryString("SmallImageHeight")%>)
                            </label>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigImage" runat="server">
                        <p>
                            <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                            <label>
                                Upload Big Image :(Width=<%= Request.QueryString("BigImageWidth")%>; Height=<%= Request.QueryString("BigImageHeight")%>)</label>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlImageAltText" runat="server">
                        <p>
                            <label>
                                Image Alt Text</label>
                            <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSmallDetails" runat="server">
                        <p>
                            <label>
                                Small Details :</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                                Rows="4"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigDetails" runat="server">
                        <p>
                            <label>
                                Details:</label>
                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                                {
                                                    filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                                    "extraPlugins": "imagebrowser",
                                                    "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                                }
                                            ); 
            
                            </script>
                        </p>
                    </asp:Panel>
                    <p  style="display:none;">
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                        </asp:SqlDataSource>
                    </p>
                <asp:HiddenField ID="hdnUpdateDate" runat="server" />
                <asp:HiddenField ID="hdnMasterID" runat="server" />
         </div> 

         <div class="btn-toolbar">
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
            
            <div class="btn-group">
            </div>
        </div>
        <%
            If hdnMasterID.Value <> "" Then
                Response.Write("<a href=""../A-CommonGallery/AllCommonGallery.aspx?TableName=HTML&TableMasterID=" & hdnMasterID.Value & "&BigImageWidth=205&BigImageHeight=126&t=" & Server.UrlEncode( txtTitle.Text) & """>Add/Edit Images</a>")
            End If
            %>

        <asp:ListView ID="ListView1" runat="server" DataKeyNames="CommonGalleryID" 
                DataSourceID="SqlDataSourceGallery">
                
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="CommonGalleryIDLabel" runat="server" 
                                Text='<%# Eval("CommonGalleryID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img src='<%# "../" & Eval("SmallImage")  %>' width="120px" />
                            
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" 
                                Checked='<%# Eval("Status") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" 
                                Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TableNameLabel" runat="server" Text='<%# Eval("TableName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TableMasterIDLabel" runat="server" 
                                Text='<%# Eval("TableMasterID") %>' />
                        </td>
                        <td>
                            <a href='<%#  "../A-CommonGallery/CommonGallery.aspx?cgid=" & Eval("CommonGalleryID") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp;
                            <a href='<%# "#" & Eval("CommonGalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("CommonGalleryID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                                
                                <table ID="itemPlaceholderContainer"  border="0" style="" class="table">
                                    <thead>
                                    <tr id="Tr1" runat="server" style="">
                                        <th id="Th1" runat="server">
                                            Common Gallery ID</th>
                                        <th id="Th2" runat="server">
                                            Title</th>
                                        <th id="Th3" runat="server">
                                            Image</th>
                                        <th id="Th4" runat="server">
                                            Sort Order</th>
                                        <th id="Th5" runat="server">
                                            Status</th>
                                        <th id="Th6" runat="server">
                                            Last Updated</th>
                                        <th id="Th7" runat="server">
                                            TableName</th>
                                        <th id="Th8" runat="server">
                                            TableMasterID</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    
                                    <tr ID="itemPlaceholder" runat="server">
                                    </tr>
                                    <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1" runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                            </Fields>
                                        </asp:DataPager>
                                    </div>
                                    
                                </table>
                            
                </LayoutTemplate>
            </asp:ListView>
            

            <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        
                DeleteCommand="DELETE FROM [CommonGallery] WHERE [CommonGalleryID] = @CommonGalleryID" 
                SelectCommand="SELECT * FROM [CommonGallery] WHERE ([TableName] like @TableName and TableMasterID=@TableMasterID )" 
                InsertCommand="INSERT INTO [CommonGallery] ([Title], [SmallImage], [BigImage], [ImageAltText], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (@Title, @SmallImage, @BigImage, @ImageAltText,1, @LastUpdated, @TableName, @TableMasterID)" 
                UpdateCommand="UPDATE [CommonGallery] SET [Title] = @Title, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText,  [Status] = @Status, [LastUpdated] = @LastUpdated, [TableName] = @TableName, [TableMasterID] = @TableMasterID, [Lang] = @Lang WHERE [CommonGalleryID] = @CommonGalleryID">
                <DeleteParameters>
                    <asp:Parameter Name="CommonGalleryID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    
                    <asp:ControlParameter ControlID="hdnTitle" Name="Title" PropertyName="Value" />
                    <asp:ControlParameter ControlID="hdnImageName" Name="SmallImage" 
                        PropertyName="Value" />
                    <asp:ControlParameter ControlID="hdnImageName" Name="BigImage" 
                        PropertyName="Value" />
                    <asp:ControlParameter ControlID="hdnTitle" Name="ImageAltText" 
                        PropertyName="Value" />
                    <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated" 
                        PropertyName="Value" />
                    <asp:CookieParameter CookieName="TableName" Name="TableName" />
                    <asp:CookieParameter CookieName="TableMasterID" Name="TableMasterID" />
                    
                </InsertParameters>
                <SelectParameters>
                    <asp:Parameter DefaultValue="HTML" Name="TableName" Type="String" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="TableMasterID" 
                        PropertyName="Value" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="BigImage" Type="String" />
                    <asp:Parameter Name="ImageAltText" Type="String" />
                    <asp:Parameter Name="SortIndex" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="LastUpdated" Type="DateTime" />
                    <asp:Parameter Name="TableName" Type="String" />
                    <asp:Parameter Name="TableMasterID" Type="Int32" />
                    <asp:Parameter Name="Lang" Type="String" />
                    <asp:Parameter Name="CommonGalleryID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            DeleteCommand="DELETE FROM [HTML] WHERE [HtmlID] = @HtmlID" 
            InsertCommand="INSERT INTO HTML(Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText, LastUpdated, MasterID, Lang) VALUES (@Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @ImageAltText,@LastUpdated, @MasterID, @Lang)" 
            SelectCommand="SELECT * FROM [HTML]"                         

        UpdateCommand="UPDATE HTML SET Title = @Title, SmallDetails = @SmallDetails, BigDetails = @BigDetails, SmallImage = @SmallImage, BigImage = @BigImage, ImageAltText = @ImageAltText, LastUpdated = @LastUpdated, MasterID = @MasterID, Lang = @Lang WHERE (HtmlID = @HtmlID)">
            <DeleteParameters>
                <asp:Parameter Name="HtmlID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />         
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />       
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" 
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" 
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
                <asp:QueryStringParameter Name="HtmlID" QueryStringField="hid" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
</asp:Content>

