﻿Imports System.Data.SqlClient
Imports System.Drawing.Imaging

Partial Class GalleryItem
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "263", smallImageHeight As String = "215", bigImageWidth As String = "Any", bigImageHeight As String = "Any"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Utility.GetDimentionSetting("GalleryItem", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")

        If IsPostBack = False Then

            If Request.QueryString("galleryItemId") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM GalleryItem where GalleryItemID=" & Request.QueryString("galleryItemId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()
                txtTitle.Text = reader("Title").ToString()
                txtSmallDetails.Text = reader("SmallDetails").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                imgBigImage.ImageUrl = "~/Admin/" + reader("BigImage").ToString()
                hdnBigImage.Value = reader("BigImage").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                lbLastUpdated.Text = reader("LastUpdated").ToString()
                sqlConn.Close()
                imgSmallImage.Visible = True
                imgBigImage.Visible = True
                rfvSmallImage.Enabled = False
                rfvBigImage.Enabled = False
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
            End If

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            lbLastUpdated.Text = DateTime.Now()
            If fuSmallImage.FileName <> "" Then
                hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
                imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
                imgSmallImage.Visible = True
            End If
            If fuBigImage.FileName <> "" Then
                hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
                imgBigImage.ImageUrl = "~/Admin/" + hdnBigImage.Value
                imgBigImage.Visible = True
            End If

            Dim a As Integer
            If Request.QueryString("galleryItemId") <> "" Then
                a = SqlDataSourceGalleryItem.Update()
            Else
                a = SqlDataSourceGalleryItem.Insert()
                Response.Redirect("AllGalleryItem.aspx?galleryId=" + Request.QueryString("galleryId") + "")
            End If

            If a > 0 Then
                divSuccess.Visible = True
                divError.Visible = False
            Else
                divSuccess.Visible = False
                divError.Visible = True
            End If
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
        End Try
    End Sub



End Class
