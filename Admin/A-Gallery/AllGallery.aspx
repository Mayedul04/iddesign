﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllGallery.aspx.vb"
    Inherits="AllGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Gallery</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Gallery Album"></asp:Label></h2>
        <div>

         <p style="display:none;">
        <Label>Language</Label>
                    <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" AutoPostBack ="true"  CssClass ="input-xlarge"   DataTextField="LangFullName" DataValueField="Lang"> </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLang" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                    </asp:SqlDataSource>
         </p>


         <p>
           <Label> Section </Label>
            <asp:DropDownList ID="ddCategory" CssClass ="input-xlarge"  runat="server" AutoPostBack="True">
               <%-- <asp:ListItem Text="About Us" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Service" Value="1" ></asp:ListItem>
                <asp:ListItem Text="Customers" Value="2" ></asp:ListItem>
                <asp:ListItem Text="Sustainability" Value="3"></asp:ListItem>--%>
                <asp:ListItem Text="Media Center" Selected="True" Value="4"></asp:ListItem>
               <%-- <asp:ListItem Text="Contact us" Value="5"></asp:ListItem>  
                <asp:ListItem Text="Login" Value="6"></asp:ListItem>  --%>

            </asp:DropDownList>
         </p>


          <div class="well">


                      <%--<table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                            <th >
                                                Title</th>
                                                                                       
                                            <th >
                                                Sort Order</th>
                                            
                                            <th >
                                                Master ID</th>                                    
                                         
                                            <th >
                                                Lang</th>

                                            <th >
                                                Date </th>

                                            <th style="width: 100px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="GalleryID" 
                    DataSourceID="SqlDataSourceGallery">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            <td>
                                <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                            </td>
                          
                            <td>
                                <asp:Label ID="SortIndex" runat="server" Text='<%# Eval("Sortindex") %>' />
                            </td>
                          
                            <td>
                                <asp:Label ID="lblMasterID" runat="server" 
                                    Text='<%# Eval("MasterID") %>' />
                            </td>
                           
                           
                            <td>
                                <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                            </td>

                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy") %>' />
                            </td>
                           

                             <td>
                             <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                            &nbsp
                              <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID")& "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign" ></i> </a>
                            &nbsp
                            <a href='<%# "AllGalleryItem.aspx?galleryId=" & Eval("GalleryID") %>' title="View/Add Gallery Item"><i class="icon-list"></i> </a>
                            &nbsp
                                <a href='<%# "#" & Eval("GalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("GalleryID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="5"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                                
                                           
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> --%>


                          <asp:ListView ID="ListView1" runat="server" DataKeyNames="GalleryID" 
        DataSourceID="SqlDataSourceGallery">
    
        <EmptyDataTemplate>
            No data was returned.
        </EmptyDataTemplate>
 
        <ItemTemplate>
            <li class="thumbnail">
							<img class="grayscale" src="http://placehold.it/140x140/eee" alt="Sample Image 1">
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' />
                            <br />
                             <br />
							   <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                           <%-- &nbsp
                              <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID")& "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign" ></i> </a>--%>
                            &nbsp
                            <a href='<%# "AllGalleryItem.aspx?galleryId=" & Eval("GalleryID") %>' title="View/Add Gallery Item"><i class="icon-list"></i> </a>
                            &nbsp
                                <a href='<%# "#" & Eval("GalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("GalleryID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
           </li>





        </ItemTemplate>
        <LayoutTemplate>
            <ul ID="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                <li runat="server" id="itemPlaceholder" />
            </ul>
            <div style="">
            </div>
        </LayoutTemplate>

    </asp:ListView>

            </div> 



    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Gallery] WHERE [GalleryID] = @GalleryID" 
                SelectCommand="SELECT Gallery.* FROM [Gallery] INNER JOIN Languages ON Gallery.Lang = Languages.Lang  where Gallery.Lang=@Lang and Gallery.CategoryID=@CategoryID ORDER BY Gallery.MasterID DESC, Languages.SortIndex" 
                ProviderName="System.Data.SqlClient">
        <DeleteParameters>
            <asp:Parameter Name="GalleryID" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="ddCategory" Name="CategoryID" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    
        </div>
       


  
    <!-- Eof content -->



</asp:Content>
