﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllContact.aspx.vb" Inherits="Admin_A_Contact_AllContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">Contact us report</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnDownload"  class="btn btn-primary"><i class="icon-save"></i> Download </button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Contacts"></asp:Label></h2>
        <div>

           <div class="well">
           
          <table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                            <th >
                                                Name</th>
                                                                                       
                                            <th >
                                                Contact Info</th>
                                            <th >
                                                Message</th>
                                            <th >
                                                Date </th>

                                            <th style="width: 20px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                     <asp:ListView ID="GridView1" runat="server" DataKeyNames="ContactID" 
                    DataSourceID="sdsContact">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            <td>
                                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' /><br />
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>' /><br />
                                <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile") %>' /><br />
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>' />

                            </td>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" Text='<%# Eval("Message") %>' Width="250px" />
                            </td>
                          

                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# Convert.ToDateTime(Eval("ContactDate")).Tostring("MMM dd, yyyy") %>' />
                            </td>
                           

                             <td>

                                <a href='<%# "#" & Eval("ContactID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("ContactID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="25"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                                
                                           
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 
                                
          </div> 
    <!-- Eof content -->
         
            <asp:SqlDataSource ID="sdsContact" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Contact] WHERE [ContactID] = @ContactID" InsertCommand="INSERT INTO [Contact] ([FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (@FullName, @Designation, @Email, @Phone, @Mobile, @Address, @ContactFor, @Subject, @Message, @ContactDate)"
                ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [ContactID], [FullName], [Designation], [Email], ('''' + [Phone]) as Phone, ('''' + [Mobile]) as Mobile, [Address], [ContactFor], [Subject], [Message], [ContactDate] FROM [Contact] where ContactFor=@ContactFor order by ContactID desc "
                
                UpdateCommand="UPDATE [Contact] SET [FullName] = @FullName, [Designation] = @Designation, [Email] = @Email, [Phone] = @Phone, [Mobile] = @Mobile, [Address] = @Address, [ContactFor] = @ContactFor, [Subject] = @Subject, [Message] = @Message, [ContactDate] = @ContactDate WHERE [ContactID] = @ContactID">
                <DeleteParameters>
                    <asp:Parameter Name="ContactID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="FullName" Type="String" />
                    <asp:Parameter Name="Designation" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="ContactFor" Type="String" />
                    <asp:Parameter Name="Subject" Type="String" />
                    <asp:Parameter Name="Message" Type="String" />
                    <asp:Parameter Name="ContactDate" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:QueryStringParameter Name="ContactFor" QueryStringField="cfor" DefaultValue="Contact" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FullName" Type="String" />
                    <asp:Parameter Name="Designation" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="ContactFor" Type="String" />
                    <asp:Parameter Name="Subject" Type="String" />
                    <asp:Parameter Name="Message" Type="String" />
                    <asp:Parameter Name="ContactDate" Type="DateTime" />
                    <asp:Parameter Name="ContactID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
       </div>
    <!-- Eof content -->
</asp:Content>

