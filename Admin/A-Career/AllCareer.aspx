﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllCareer.aspx.vb" Inherits="Admin_A_Career_AllCareer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <h1 class="page-title">Career</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Career"></asp:Label></h2>
        <div>

         <p>
                    <label>Language</label>
                    <asp:DropDownList ID="ddlLang" runat="server" CssClass ="input-xlarge" AutoPostBack="true"  DataSourceID="sdsLang"  DataTextField="LangFullName" DataValueField="Lang"> </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLang" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                    </asp:SqlDataSource>
        </p>

           <div class="well">
           
          <table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                            <th >
                                                Title</th>
                                                                                       
                                            <th >
                                                Job Code</th>
                                            <th >
                                                Sort Order</th>
                                            
                                            <th >
                                                Master ID</th>                                    
                                         
                                            <th >
                                                Lang</th>

                                            <th >
                                                Date </th>

                                            <th style="width: 60px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                     <asp:ListView ID="GridView1" runat="server" DataKeyNames="CareerID" 
                    DataSourceID="sdsCareer">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            <td>
                                <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblJobCode" runat="server" Text='<%# Eval("JobCode") %>' />
                            </td>
                            <td>
                                <asp:Label ID="SortIndex" runat="server" Text='<%# Eval("Sortindex") %>' />
                            </td>
                          
                            <td>
                                <asp:Label ID="lblMasterID" runat="server" 
                                    Text='<%# Eval("MasterID") %>' />
                            </td>
                           
                           
                            <td>
                                <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                            </td>

                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy") %>' />
                            </td>
                           

                             <td>
                             <a href='<%# "Career.aspx?crid=" & Eval("CareerID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                           &nbsp
                              <a href='<%# "Career.aspx?crid=" & Eval("CareerID")& "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign" ></i> </a>
                            &nbsp
                                <a href='<%# "#" & Eval("CareerID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("CareerID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="5"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                                
                                           
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 
                                
          </div> 
    <!-- Eof content -->

            <asp:SqlDataSource ID="sdsCareer" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT Career.CareerID, Career.Title, Career.SmallDetails, Career.BigDetails, Career.SmallImage, Career.BigImage, Career.Link, Career.Featured, Career.SortIndex, Career.Status, Career.LastUpdated, Career.MasterID, Career.Lang,Career.JobCode FROM Career INNER JOIN Languages ON Career.Lang = Languages.Lang where Career.Lang=@Lang ORDER BY Career.MasterID DESC, Languages.SortIndex" 
                DeleteCommand="DELETE FROM Career WHERE (CareerID = @CareerID)">
                <DeleteParameters>
                    <asp:Parameter Name="CareerID" />
                </DeleteParameters>
                <SelectParameters>
                      <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                        PropertyName="SelectedValue" />
                </SelectParameters> 
            </asp:SqlDataSource>
</div>
    <!-- Eof content -->
</asp:Content>

