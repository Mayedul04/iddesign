﻿
Partial Class Admin_A_NewsletterSubscription_AllNewsletterSubscription
    Inherits System.Web.UI.Page

    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.Click
        Utility.DownloadCSV(Utility.EncodeTitle("NewsletterSubscription " & DateTime.Now, "-") & ".csv", "SELECT [NewSubID],  [Email], [SubscriptionDate] FROM [NewsletterSubscription]", Response)
    End Sub
End Class
