﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" ValidateRequest="false"
    AutoEventWireup="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false"
    CodeFile="TopBannerEdit.aspx.vb" Inherits="TopBannerEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="block">
        <div class="block-heading">
            Banner</div>
        <div class="block-body">
            <h1 class="page-title">
                Banner</h1>
            
            <h2>
                <asp:Label ID="lblTabTitle" runat="server" Text="Add Banner"></asp:Label></h2>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
            </div>
            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">
                    <p>
                        <label>
                            Section</label>
                        <asp:DropDownList ID="ddSection" CssClass="input-xlarge" runat="server" AutoPostBack="True">
                             <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="Home" Selected="True" Value="Home"></asp:ListItem>
                            <asp:ListItem  Text="Style Groups" Value="Groups"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label>
                            Title</label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="txt"></asp:TextBox>
                    </p>
                    <asp:Panel ID="pnlSmallDetails" Visible="false" runat="server">
                        <p>
                            <label>
                                Small Details</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" Height="100" Width="550" TextMode="MultiLine"
                                CssClass="input-xlarge"></asp:TextBox>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSmallImage" Visible="false" runat="server">
                        <p>
                            <label>
                                Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                            <asp:Image ID="imgSmallImage" runat="server" />
                        </p>
                        <p>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                            <label>
                                <%--<asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" ControlToValidate="fuSmallImage"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                            </label>
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigImage" runat="server">
                        <p>
                            <label>
                                Big Image (Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                            <asp:Image ID="imgBigImage" Visible="false" runat="server" />
                        </p>
                        <p>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                            <label>
                                <asp:RequiredFieldValidator ID="rfvBigImage" runat="server" ControlToValidate="fuBigImage"
                                    ErrorMessage="* Required"></asp:RequiredFieldValidator>
                            </label>
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                        </p>
                    </asp:Panel>
                    <p>
                        <label>
                            Image Alt Text</label>
                        <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </p>
                    <p style="display:none;">
                        <label>
                            Link</label>
                        <asp:TextBox ID="txtLink" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </p>
                    <p>
                        <label>
                            Sort Order:
                        </label>
                        <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                        <label class="red">
                            <asp:RangeValidator ID="RangeValidator1" Display="Dynamic" ControlToValidate="txtSortIndex"
                                SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                                ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                        </label>
                    </p>
                    <p>
                        <label>
                            Status:</label>
                        <asp:CheckBox ID="chkStatus" CssClass="input-xlarge" runat="server" Checked="true" />
                    </p>
                    <p style="display:none;">
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
                            DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                        </asp:SqlDataSource>
                    </p>
                    <p>
                        <label>
                            Last Updated :</label>
                        <asp:Label ID="lbLastUpdated" CssClass="input-xlarge" runat="server"></asp:Label>
                    </p>
                </div>
                <div class="btn-toolbar">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                        <i class="icon-save"></i> Add New</button>
            
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- Eof content -->
            <asp:SqlDataSource ID="SqlDataSourceTopBanner" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Banner] WHERE [BannerID] = @BannerID" InsertCommand="INSERT INTO [Banner] ([SectionName], [Title], [SmallDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [SortIndex], [Status],  [LastUpdated],[Lang]) VALUES (@SectionName, @Title, @SmallDetails, @SmallImage, @BigImage, @ImageAltText, @Link, @SortIndex, @Status, @LastUpdated,@Lang)"
                SelectCommand="SELECT * FROM [Banner]" UpdateCommand="UPDATE [Banner] SET [SectionName] = @SectionName, [Title] = @Title, [SmallDetails] = @SmallDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText, [Link] = @Link, [SortIndex] = @SortIndex, [Status] = @Status,  [LastUpdated] = @LastUpdated,Lang=@Lang WHERE [BannerID] = @BannerID">
                <DeleteParameters>
                    <asp:Parameter Name="BannerID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="ddSection" Name="SectionName" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue"
                        Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="ddSection" Name="SectionName" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:QueryStringParameter Name="BannerID" QueryStringField="bannerId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
