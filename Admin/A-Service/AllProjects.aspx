﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllProjects.aspx.vb" Inherits="Admin_A_Service_AllProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">
        Project Division</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <a href="../A-Service/AllService.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="All Projects"></asp:Label></h2>
    <div>
        <div class="well">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsList_Project" DataKeyNames="ProjectID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ParentTitleLabel" runat="server" Text='<%# Eval("ParentTitle") %>' />
                        </td>
                        
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img width="120px" src='<%# "../" & Eval("BigImage") %>' />
                        </td>
                        
                        <td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" Checked='<%# Eval("Featured") %>'
                                Enabled="false" />
                        </td>
                       
                        <td>
                            <a href="../A-CommonGallery/AllCommonGallery.aspx?TableName=List_Project&TableMasterID=<%# Eval("MasterID") %>&t=<%# Server.UrlEncode(Eval("Title").ToString()) %>&BigImageWidth=272&BigImageHeight=152">Add/Edit Images</a>
                        </td>

                        <td>
                            <a href='<%#  "Project.aspx?l1cid=" & Eval("ProjectID") & "&l1id="& Eval("ServiceID")  %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            
                            <a href='<%# "#" & Eval("ProjectID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ProjectID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    Parent Title
                                </th>
                                
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    Small Image
                                </th>
                                
                                <th id="Th6" runat="server">
                                    Featured
                                </th>
                               
                                <th>
                                    Images
                                </th>
                                
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsList_Project" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT List_Project.ProjectID, List_Project.Title, List_Project.SmallDetails, List_Project.BigDetails, List_Project.SmallImage, List_Project.BigImage, List_Project.Link, List_Project.Featured, List_Project.SortIndex, List_Project.NewArrival, List_Project.LastUpdated, List_Service.Title AS ParentTitle, List_Project.ServiceID, List_Project.MasterID, List_Project.Lang FROM List_Project INNER JOIN List_Service ON List_Project.ServiceID = List_Service.ServiceID WHERE (List_Project.ServiceID = @ServiceID) ORDER BY List_Project.ProjectID DESC" 
                DeleteCommand="DELETE FROM List_Project WHERE (ProjectID = @ProjectID)">
                <DeleteParameters>
                    <asp:Parameter Name="ProjectID" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:QueryStringParameter Name="ServiceID" QueryStringField="L1id" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

