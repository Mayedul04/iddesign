﻿
Partial Class Admin_A_Service_AllProjects
    Inherits System.Web.UI.Page
    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Project.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub
End Class
