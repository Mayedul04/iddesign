﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Project.aspx.vb" Inherits="Admin_A_Service_Project" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="page-title">Project</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnBack" class="btn btn-primary">
            Back</button>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Collection</label>
                <asp:DropDownList ID="ddlServices" runat="server" AppendDataBoundItems="True" DataSourceID="sdsService" DataTextField="Title" DataValueField="ServiceID">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsService" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [ServiceID], [Title] FROM [List_Service]"></asp:SqlDataSource>
            </p>
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" Visible="false" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>
                        Small Details:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlDetails" runat="server">
                <p>
                    <label>
                        Details:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );



                    </script>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlLink" Visible="false" runat="server">
                <p>
                    <label>
                        Link:</label>
                    <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                </p>
            </asp:Panel>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p style="display:none;">
                <asp:CheckBox ID="chkNewArrival" Checked="true" runat="server" TextAlign="Left" Text="New Arrival? " />
                <label class="red">
                </label>
            </p>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style="display: none;">
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>
        <asp:SqlDataSource ID="sdsList_Project" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [List_Project] WHERE [ProjectID] = @ProjectID" InsertCommand="INSERT INTO [List_Project] ( ServiceID, [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [Link], [Featured], [SortIndex], [Status], [LastUpdated],ImageAltText, MasterID, Lang, NewArrival) VALUES ( @ServiceID, @Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @Link, @Featured, @SortIndex, @Status, @LastUpdated,@ImageAltText, @MasterID, @Lang, @NewArrival)"
            SelectCommand="SELECT * FROM [List_Project]" UpdateCommand="UPDATE [List_Project] SET ServiceID=@ServiceID, [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [Link] = @Link, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated,ImageAltText=@ImageAltText, MasterID=@MasterID,Lang=@Lang, NewArrival=@NewArrival  WHERE [ProjectID] = @ProjectID">
            <DeleteParameters>
                <asp:Parameter Name="ProjectID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:QueryStringParameter Name="ServiceID" QueryStringField="l1id" Type="Int32" />
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                    Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                    Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                    Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                    Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" />
                <asp:ControlParameter ControlID="chkNewArrival" Name="NewArrival" PropertyName="Checked" />
            </InsertParameters>
            <UpdateParameters>
                <asp:QueryStringParameter Name="ProjectID" QueryStringField="l1cid" Type="Int32" />
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                    Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                    Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                    Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                    Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                    Type="DateTime" />
                <asp:QueryStringParameter Name="ServiceID" QueryStringField="l1id" Type="Int32" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" />
                <asp:ControlParameter ControlID="chkNewArrival" Name="NewArrival" PropertyName="Checked" />
            </UpdateParameters>
        </asp:SqlDataSource>

        <%
            If hdnMasterID.Value <> "" Then
                Response.Write("<a href=""../A-CommonGallery/AllCommonGallery.aspx?TableName=List_Project&TableMasterID=" & hdnMasterID.Value & "&BigImageWidth=94&BigImageHeight=68&t=" & Server.UrlEncode(txtTitle.Text) & """>Add/Edit Images</a>")
            End If
        %>
        <asp:ListView ID="ListView1" runat="server" DataKeyNames="CommonGalleryID"
            DataSourceID="SqlDataSourceGallery">

            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="CommonGalleryIDLabel" runat="server"
                            Text='<%# Eval("CommonGalleryID") %>' />
                    </td>
                    <td>
                        <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                    </td>
                    <td>
                        <img src='<%# "../" & Eval("SmallImage")  %>' width="120px" />

                    </td>
                    <td>
                        <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                    </td>
                    <td>
                        <asp:CheckBox ID="StatusCheckBox" runat="server"
                            Checked='<%# Eval("Status") %>' Enabled="false" />
                    </td>
                    <td>
                        <asp:Label ID="LastUpdatedLabel" runat="server"
                            Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                    </td>
                    <td>
                        <asp:Label ID="TableNameLabel" runat="server" Text='<%# Eval("TableName") %>' />
                    </td>
                    <td>
                        <asp:Label ID="TableMasterIDLabel" runat="server"
                            Text='<%# Eval("TableMasterID") %>' />
                    </td>
                    <td>
                        <a href='<%#  "../A-CommonGallery/CommonGallery.aspx?cgid=" & Eval("CommonGalleryID") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp;
                            <a href='<%# "#" & Eval("CommonGalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                        <div class="modal small hide fade" id='<%# Eval("CommonGalleryID") %>' tabindex="-1" role="dialog"
                            aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×</button>
                                <h3 id="myModalLabel">Delete Confirmation</h3>
                            </div>
                            <div class="modal-body">
                                <p class="error-text">
                                    <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">
                                    Cancel</button>
                                <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                    Text="Delete" />
                            </div>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <LayoutTemplate>

                <table id="itemPlaceholderContainer" border="0" style="" class="table">
                    <thead>
                        <tr id="Tr1" runat="server" style="">
                            <th id="Th1" runat="server">Common Gallery ID</th>
                            <th id="Th2" runat="server">Title</th>
                            <th id="Th3" runat="server">Image</th>
                            <th id="Th4" runat="server">Sort Order</th>
                            <th id="Th5" runat="server">Status</th>
                            <th id="Th6" runat="server">Last Updated</th>
                            <th id="Th7" runat="server">TableName</th>
                            <th id="Th8" runat="server">TableMasterID</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <div class="paginationNew pull-right">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True"
                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True"
                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </div>

                </table>

            </LayoutTemplate>
        </asp:ListView>


        <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [CommonGallery] WHERE [CommonGalleryID] = @CommonGalleryID"
            SelectCommand="SELECT * FROM [CommonGallery] WHERE ([TableName] like @TableName and TableMasterID=@TableMasterID )"
            InsertCommand="INSERT INTO [CommonGallery] ([Title], [SmallImage], [BigImage], [ImageAltText], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (@Title, @SmallImage, @BigImage, @ImageAltText,1, @LastUpdated, @TableName, @TableMasterID)"
            UpdateCommand="UPDATE [CommonGallery] SET [Title] = @Title, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText,  [Status] = @Status, [LastUpdated] = @LastUpdated, [TableName] = @TableName, [TableMasterID] = @TableMasterID, [Lang] = @Lang WHERE [CommonGalleryID] = @CommonGalleryID">
            <DeleteParameters>
                <asp:Parameter Name="CommonGalleryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>

                <asp:ControlParameter ControlID="hdnTitle" Name="Title" PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnImageName" Name="SmallImage"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnImageName" Name="BigImage"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnTitle" Name="ImageAltText"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated"
                    PropertyName="Value" />
                <asp:CookieParameter CookieName="TableName" Name="TableName" />
                <asp:CookieParameter CookieName="TableMasterID" Name="TableMasterID" />

            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="List_Project" Name="TableName" Type="String" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="TableMasterID"
                    PropertyName="Value" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="SmallImage" Type="String" />
                <asp:Parameter Name="BigImage" Type="String" />
                <asp:Parameter Name="ImageAltText" Type="String" />
                <asp:Parameter Name="SortIndex" Type="Int32" />
                <asp:Parameter Name="Status" Type="Boolean" />
                <asp:Parameter Name="LastUpdated" Type="DateTime" />
                <asp:Parameter Name="TableName" Type="String" />
                <asp:Parameter Name="TableMasterID" Type="Int32" />
                <asp:Parameter Name="Lang" Type="String" />
                <asp:Parameter Name="CommonGalleryID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

