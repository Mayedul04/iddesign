﻿
Partial Class Admin_A_Service_Project
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "555", smallImageHeight As String = "310", bigImageWidth As String = "555", bigImageHeight As String = "310", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If

        ''  Utility.GetDimentionSetting("List1Child", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("l1cid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Project"
                LoadContent(Request.QueryString("l1cid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        'If fuSmallImage.FileName <> "" Then
        '    hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        'End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        'If hdnSmallImage.Value <> "" Then
        '    imgSmallImage.Visible = True
        '    imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        'End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If




        If String.IsNullOrEmpty(Request.QueryString("l1cid")) Then
            hdnMasterID.Value = GetMasterID(Request.QueryString("l1id"), Request.QueryString("lang"))
            If sdsList_Project.Insert() > 0 Then
                InsertIntoSEO()
                Response.Redirect("AllProjects.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
            Else
                divError.Visible = True
            End If
        Else
            If sdsList_Project.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID]) SELECT top 1  List_Project.Title,List_Project.SmallDetails,List_Project.SmallDetails,1,'List_Project', List_Project.ServiceID  FROM List_Project order by List_Project.ProjectID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        ServiceID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, Featured, SortIndex, Status, LastUpdated,ImageAltText,MasterID,Lang, NewArrival   FROM   List_Project where ProjectID=@ProjectID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ProjectID", Data.SqlDbType.Int)
        cmd.Parameters("ProjectID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            ddlServices.SelectedValue = reader("ServiceID").ToString()
            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            'If hdnSmallImage.Value <> "" Then
            '    imgSmallImage.Visible = True
            '    imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            'End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            txtLinkTextBox.Text = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            Boolean.TryParse(reader("NewArrival") & "", chkNewArrival.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""

            hdnMasterID.Value = reader("MasterID").ToString()

        End If
        conn.Close()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.ServerClick
        Response.Redirect("AllProjects.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
    End Sub

    Protected Function GetMasterID(ByVal parentID As Int16, ByVal Lang As String) As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (isNull( Max( MasterID),0)+1) as MaxMasterID  FROM   List_Project where ServiceID=@ServiceID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ServiceID", Data.SqlDbType.Int).Value = parentID
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Lang


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function
End Class
