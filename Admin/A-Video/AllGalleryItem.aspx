﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllGalleryItem.aspx.vb" Inherits="AllGalleryItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Video Item</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <%--<a href="../A-Gallery/AllGallery.aspx" data-toggle="modal" class="btn btn-primary">Back</a>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Video Item"></asp:Label></h2>
    <div>
        <div class="well">
            
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="VideoGalleryID" DataSourceID="SqlDataSourceGalleryItem">
                <EmptyDataTemplate>
                    No data was returned.
                </EmptyDataTemplate>
                <ItemTemplate>
                    <li class="thumbnail">
                        <img class="grayscale" src='<%# Eval("VideoImageURL")%>' width="140px" alt='<%# Eval("Title") %>'>
                        <a href='<%# "GalleryItem.aspx?galleryItemId=" & Eval("VideoGalleryID") %>'
                            title="Edit"><i class="icon-pencil"></i></a>&nbsp <a href='<%# "#" & Eval("VideoGalleryID") %>'
                                data-toggle="modal"><i class="icon-remove"></i></a>
                        <div class="modal small hide fade" id='<%# Eval("VideoGalleryID") %>' tabindex="-1"
                            role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×</button>
                                <h3 id="myModalLabel">
                                    Delete Confirmation</h3>
                            </div>
                            <div class="modal-body">
                                <p class="error-text">
                                    <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">
                                    Cancel</button>
                                <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                    Text="Delete" />
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <ul id="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div style="">
                    </div>
                </LayoutTemplate>
            </asp:ListView>
        </div>
        <asp:SqlDataSource ID="SqlDataSourceGalleryItem" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [VideoGallery] WHERE [VideoGalleryID]= @VideoGalleryID"
            SelectCommand="SELECT * FROM [VideoGallery]   order by SortIndex">
            <DeleteParameters>
                <asp:Parameter Name="VideoGalleryID" Type="Int32" />
            </DeleteParameters>
           
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
