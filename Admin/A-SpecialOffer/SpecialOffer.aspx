﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" ValidateRequest="false"
    AutoEventWireup="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false"
    CodeFile="SpecialOffer.aspx.vb" Inherits="SpecialOffer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Promotion</h1>
    <div class="btn-toolbar">
        <a href="../A-SpecialOffer/AllSpecialOffer.aspx" data-toggle="modal" class="btn  btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Special Offer Add/Edit"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>


        <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">

            <p>
                <label>Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic"  ValidationGroup="form" ControlToValidate="txtTitle"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" Visible="false" runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>Small Details:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"  runat="server" ControlToValidate="txtSmallDetails"
                            ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$" ErrorMessage="* character limit is 1000"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <p>
                <label>Details</label>
                <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                <script>

                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.

                    CKEDITOR.replace('<%=txtDetails.ClientID %>',
                        {
                            filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                            "extraPlugins": "imagebrowser",
                            "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                        }
                    ); 
            
                </script>
            </p>
            
            <p>
                <label>Start Date</label>
                <asp:TextBox ID="txtStartDate" CssClass="input-xlarge" ValidationGroup="date" runat="server"></asp:TextBox>&nbsp;&nbsp;
                <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" TargetControlID="txtStartDate">
                </cc1:CalendarExtender>
            </p>
            <p>
                <label>End Date</label>
                <asp:TextBox ID="txtEndDate" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" TargetControlID="txtEndDate">
                </cc1:CalendarExtender>
            </p>
            <p style="display:none;">
                <label>Link</label>
                <asp:TextBox CssClass="input-xlarge" ID="txtLink" runat="server"></asp:TextBox>
            </p>
            <p>
                <label>Featured</label>
                <asp:CheckBox ID="chkFeatured" runat="server" />
            </p>
            <p>
                <label>Sort Order: </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
            <p>
                <label>Status</label>
                <asp:CheckBox ID="chkStatus" runat="server" Checked="True" />
            </p>
            <p style="display:none;">
                <label>Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                </asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            
            <p>
                <label>Last Updated :</label>
                <asp:Label ID="lblLastUpdated" runat="server"></asp:Label></p>


        </div> 
        <div class="btn-toolbar">
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        </div>
    </div>







    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDatasourceSpecialOffer" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [SpecialOffer] WHERE [SpecialOfferID] = @SpecialOfferID"
        InsertCommand="INSERT INTO [SpecialOffer] ([Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate],MasterID, Lang) VALUES (@Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @ImageAltText, @Link, @Featured, @SortIndex, @Status, @LastUpdated, @StartDate, @EndDate,@MasterID, @Lang)"
        SelectCommand="SELECT * FROM [SpecialOffer]" UpdateCommand="UPDATE [SpecialOffer] SET [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText, [Link] = @Link, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated, [StartDate] = @StartDate, [EndDate] = @EndDate, MasterID=@MasterID, Lang=@Lang  WHERE [SpecialOfferID] = @SpecialOfferID">
        <DeleteParameters>
            <asp:Parameter Name="SpecialOfferID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" Type="String" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lblLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" Type="String" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lblLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:QueryStringParameter Name="SpecialOfferID" QueryStringField="specialOfferId"
                Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:HiddenField ID="hdnNewID" runat="server" />
    <asp:SqlDataSource ID="SqlDataSourceSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [SEO] ([PageType], [PageID],  [SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot],Lang) VALUES (@PageType, @PageID,  @SEOTitle, @SEODescription, @SEOKeyWord, @SEORobot,@Lang)">
        <InsertParameters>
            <asp:Parameter Name="PageType" Type="String" DefaultValue="SpecialOffer" />
            <asp:ControlParameter ControlID="hdnNewID" DefaultValue="" Name="PageID" PropertyName="Value"
                Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SEODescription" PropertyName="Text"
                Type="String" />
            <asp:Parameter Name="SEOKeyWord" Type="String" DefaultValue="" />
            <asp:Parameter Name="SEORobot" Type="Boolean" DefaultValue="true" />
            <asp:ControlParameter ControlID="ddlLang" DefaultValue="" Name="Lang" 
                PropertyName="SelectedValue" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
