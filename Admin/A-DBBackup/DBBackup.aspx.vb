﻿Imports System.IO
Imports System.Data.SqlClient

Partial Class Admin_DBBackup
    Inherits System.Web.UI.Page
    Public serverPath As String
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            
            LoadExistingBackupInfo()
        End If

    End Sub

    Protected Sub linkBackup_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
            Dim sqlConn As SqlConnection = New SqlConnection(strConn)
            sqlConn.Open()
            serverPath = Server.MapPath("~/")
            serverPath = serverPath + "DatabaseBackup"
            If (Not System.IO.Directory.Exists(serverPath)) Then
                System.IO.Directory.CreateDirectory(serverPath)
            End If

            Dim fullPath As String = serverPath + "\" & txtDBName.Text & "-" & Date.Today.ToString("MM-dd-yyyy") + ".bak"
            Dim Name = "Full Backup of " & txtDBName.Text
            Dim sqlString As String = "BACKUP DATABASE " & txtDBName.Text
            sqlString += " TO DISK = '" + fullPath + "' "
            sqlString += " WITH FORMAT, "
            sqlString += "MEDIANAME = '" + serverPath + "', "
            sqlString += "NAME ='" + Name + "';"
            Dim sqlcomm As SqlCommand = New SqlCommand(sqlString, sqlConn)
            sqlcomm.ExecuteNonQuery()
            sqlConn.Close()

            'Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\" + ddDB.SelectedValue + ".bak" + "\")
            divSuccess.Visible = True
            divError.Visible = False
            lblSuccMessage.Text = "backup successfully completed"
            LoadExistingBackupInfo()
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
            lblErrMessage.Text = "backup failure,try again : " + ex.Message
        End Try

    End Sub

    Private Sub LoadExistingBackupInfo()
        serverPath = Server.MapPath("~/")
        serverPath = serverPath + "DatabaseBackup"
        If (System.IO.Directory.Exists(serverPath)) Then
            Dim dirInfo As New DirectoryInfo(serverPath)
            GridView1.DataSource = dirInfo.GetFiles("*.bak")
            GridView1.DataBind()
        End If

    End Sub

End Class
