﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="Admin_A_Profile_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <h1 class="page-title">
        Profile</h1>
    <div class="btn-toolbar">
        
        <a href="../A-Profile/AllProfile.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Profile"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>Person Name:</label>
                <asp:TextBox ID="txtPersonName" runat="server" CssClass="txt" MaxLength="300"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="form" ControlToValidate="txtPersonName"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Designation:</label>
                <asp:TextBox ID="txtDesignation" runat="server" CssClass="txt" MaxLength="300"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtDesignation"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>
                        Small Details:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlDetails" runat="server">
                <p>
                    <label>
                        Details:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        ); 
            
                    </script>
                    
                </p>
            </asp:Panel>
            
            <p>
                <label>Email :</label>
                <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="txt" MaxLength="400" />
                <label class="red">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" 
                    runat="server" ErrorMessage="RegularExpressionValidator" ValidationGroup="form" 
                    ControlToValidate="txtEmail" SetFocusOnError="True" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email</asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <label>Mobile :</label>
                <asp:TextBox ID="txtMobile" runat="server" Text="" CssClass="txt" MaxLength="20" />
                <label class="red">
                </label>
            </p>
            <p>
                <label>Phone :</label>
                <asp:TextBox ID="txtPhone" runat="server" Text="" CssClass="txt" MaxLength="20" />
                <label class="red">
                </label>
            </p>
            <p>
                <label>Facebook :</label>
                <asp:TextBox ID="txtFacebook" runat="server" Text="" CssClass="txt" MaxLength="500" />
                <label class="red">
                </label>
            </p>
            <p>
                <label>Twitter :</label>
                <asp:TextBox ID="txtTwitter" runat="server" Text="" CssClass="txt" MaxLength="500" />
                <label class="red">
                </label>
            </p>
            <p>
                <label>Linked In :</label>
                <asp:TextBox ID="txtLinkedIn" runat="server" Text="" CssClass="txt" MaxLength="500" />
                <label class="red">
                </label>
            </p>

            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <%--<p>
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                </asp:SqlDataSource>
            </p>--%>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>

        <div class="btn-toolbar">
        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        
        <div class="btn-group">
        </div>
    </div>
    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsProfileArticles" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                DeleteCommand="DELETE FROM [List_Profile] WHERE [ProfileID] = @ProfileID" 
                InsertCommand="INSERT INTO [List_Profile] ([PersonName], [Designation], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [LinkedIn], [Facebook], [Twitter], [Mobile], [Phone], [Email], [Featured], [SortIndex], [Status], [LastUpdated],ImageAltText) VALUES (@PersonName, @Designation, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @LinkedIn, @Facebook, @Twitter, @Mobile, @Phone, @Email, @Featured, @SortIndex, @Status, @LastUpdated,@ImageAltText)" 
                SelectCommand="SELECT * FROM [List_Profile]" 
                
                UpdateCommand="UPDATE [List_Profile] SET [PersonName] = @PersonName, [Designation] = @Designation, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [LinkedIn] = @LinkedIn, [Facebook] = @Facebook, [Twitter] = @Twitter, [Mobile] = @Mobile, [Phone] = @Phone, [Email] = @Email, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated, ImageAltText=@ImageAltText  WHERE [ProfileID] = @ProfileID">
                <DeleteParameters>
                    <asp:Parameter Name="ProfileID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    
                    <asp:ControlParameter Name="PersonName" Type="String" ControlID="txtPersonName" 
                        PropertyName="Text" />
                    <asp:ControlParameter Name="Designation" Type="String" 
                        ControlID="txtDesignation" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                    <asp:ControlParameter Name="LinkedIn" Type="String" ControlID="txtLinkedIn" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtFacebook" Name="Facebook" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtTwitter" Name="Twitter" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                </InsertParameters>
                <UpdateParameters>
                    
                    <asp:ControlParameter Name="PersonName" Type="String" ControlID="txtPersonName" 
                        PropertyName="Text" />
                    <asp:ControlParameter Name="Designation" Type="String" 
                        ControlID="txtDesignation" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                    <asp:ControlParameter Name="LinkedIn" Type="String" ControlID="txtLinkedIn" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtFacebook" Name="Facebook" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtTwitter" Name="Twitter" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                    <asp:QueryStringParameter Name="ProfileID" QueryStringField="pid" 
                        Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

</asp:Content>

