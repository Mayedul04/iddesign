﻿
Partial Class Admin_A_Profile_Profile
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("Profile", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("pid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Profile"
                LoadContent(Request.QueryString("pid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "Small", Utility.EncodeTitle(txtPersonName.Text, "-") & "Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "Big", Utility.EncodeTitle(txtPersonName.Text, "-") & "Big"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If




        If String.IsNullOrEmpty(Request.QueryString("pid")) Then
            If sdsProfileArticles.Insert() > 0 Then
                InsertIntoSEO()
                divSuccess.Visible = True 'Response.Redirect("AllProfile.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsProfileArticles.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = True
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID]) SELECT top 1  List_Profile.PersonName,List_Profile.SmallDetails,List_Profile.SmallDetails,1,'Profile', List_Profile.ProfileID  FROM List_Profile order by List_Profile.ProfileID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        ProfileID, PersonName, Designation, SmallDetails, BigDetails, SmallImage, BigImage, LinkedIn, Facebook, Twitter, Mobile, Phone, Email, Featured, SortIndex,  Status, LastUpdated,ImageAltText  FROM List_Profile where ProfileID=@ProfileID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ProfileID", Data.SqlDbType.Int)
        cmd.Parameters("ProfileID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtPersonName.Text = reader("PersonName") & ""
            txtDesignation.Text = reader("Designation") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            txtLinkedIn.Text = reader("LinkedIn") & ""
            txtFacebook.Text = reader("Facebook") & ""
            txtTwitter.Text = reader("Twitter") & ""
            txtMobile.Text = reader("Mobile") & ""
            txtPhone.Text = reader("Phone") & ""
            txtEmail.Text = reader("Email") & ""

            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub

    



End Class
