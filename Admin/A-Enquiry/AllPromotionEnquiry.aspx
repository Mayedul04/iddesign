﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllPromotionEnquiry.aspx.vb" Inherits="Admin_A_Enquiry_AllPromotionEnquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <h1 class="page-title">Enquiry report</h1>

    <div class="btn-toolbar">
        <button runat="server" id="btnDownload" class="btn btn-primary"><i class="icon-save"></i>Download </button>
        <div class="btn-group">
        </div>
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Promotion Enquiry"></asp:Label></h2>
    <div>

        <div class="well">

            <table class="table">

                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Promotion</th>
                        <th>Contact Info</th>
                        <th>Message</th>
                        <th>Date </th>
                        <th style="width: 20px;"></th>
                    </tr>

                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="EnquiryID"
                        DataSourceID="sdsEnquiry">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                 <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Promotion") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# "Email: " & Eval("Email") %>' /><br />
                                    <asp:Label ID="lblPhone" runat="server" Text='<%# "Phone: " & Eval("Phone") %>' /><br />


                                </td>
                                
                                <td>
                                    <asp:TextBox ID="lblMessage"  TextMode="MultiLine" Rows="3" Text='<%# Eval("Message") %>' Width="150px" runat="server"></asp:TextBox>
                                   
                                </td>


                                <td>
                                    <asp:Label ID="LastUpdatedLabel" runat="server"
                                        Text='<%# Convert.ToDateTime(Eval("Date1")).Tostring("MMM dd, yyyy") %>' />
                                </td>


                                <td>

                                    <a href='<%# "#" & Eval("EnquiryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                    <div class="modal small hide fade" id='<%# Eval("EnquiryID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">

                                            <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server" Text="Delete" />

                                        </div>
                                    </div>

                                </td>


                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>

                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                            <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" PageSize="25" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />



                                    </Fields>
                                </asp:DataPager>
                            </div>

                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>

        </div>
        <!-- Eof content -->

        <asp:SqlDataSource ID="sdsEnquiry" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM ProductEnquiry WHERE (EnquiryID = @ID)"
            SelectCommand="SELECT ProductEnquiry.EnquiryID, ProductEnquiry.Name, ProductEnquiry.Email, ProductEnquiry.Phone, ProductEnquiry.Message, ProductEnquiry.Date1, SpecialOffer.Title as Promotion FROM ProductEnquiry INNER JOIN SpecialOffer ON ProductEnquiry.PromoID = SpecialOffer.SpecialOfferID">
            <DeleteParameters>
                <asp:Parameter Name="ID" />
            </DeleteParameters>

        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>

