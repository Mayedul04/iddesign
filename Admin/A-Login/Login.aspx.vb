﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.Web.Script.Serialization

Partial Class Admin_A_Login_Login
    Inherits System.Web.UI.Page
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim userID As String = txtUserID.Text.Trim()
        Dim password As String = txtPassword.Text.Trim()
        If userID.Contains("'") Or String.IsNullOrEmpty(userID) Then
            lblMessage.Text = "User ID or password Invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If
        If password.Contains("'") Or String.IsNullOrEmpty(password) Then
            lblMessage.Text = "User ID or password Invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select * from AdminPanelLogin where UN_1=@userID and PS_1=@password"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50).Value = txtUserID.Text
        cmd.Parameters.Add("password", Data.SqlDbType.VarChar, 100).Value = txtPassword.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim islocked As Boolean = False
        If reader.HasRows Then
            reader.Read()

            'check login attempts witin 3 mintures, if its FailedAttempts>=3 and minute<3 then locked
            If reader("FailedAttempts").ToString() <> "" And reader("LastLoginAttempt").ToString() <> "" Then
                If CInt(reader("FailedAttempts").ToString()) > 0 And CInt(reader("FailedAttempts").ToString()) Mod 3 = 0 And DateDiff(DateInterval.Minute, CDate(reader("LastLoginAttempt").ToString()), Date.Now) < 30 Then
                    lblMessage.Text = "Sorry you are temporary locked. please try again after 30 minutes."
                    lblMessage.ForeColor = Drawing.Color.Red
                    islocked = True
                End If
            End If
            If islocked = False Then
                If Request.Cookies("userName") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("userName", txtUserID.Text))
                Else
                    Response.Cookies("userName").Value = txtUserID.Text
                End If

                If Request.Cookies("userFullName") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("userFullName", reader("Title").ToString()))
                Else
                    Response.Cookies("userFullName").Value = reader("Title").ToString()
                End If

                If Request.Cookies("userRole") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("userRole", reader("Role").ToString()))
                Else
                    Response.Cookies("userRole").Value = reader("Role").ToString()
                End If

                If Request.Cookies("userpass") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("userpass", New Encription().EncryptTripleDES(txtPassword.Text.Trim(), "*n3x@")))
                Else
                    Response.Cookies("userpass").Value = New Encription().EncryptTripleDES(txtPassword.Text.Trim(), "*n3x@")
                End If

                If Not chkRemember.Checked Then
                    Response.Cookies("userName").Expires = DateTime.Now.AddDays(1)
                    Response.Cookies("userFullName").Expires = DateTime.Now.AddDays(1)
                    Response.Cookies("userpass").Expires = DateTime.Now.AddDays(1)
                    Response.Cookies("userRole").Expires = DateTime.Now.AddDays(1)
                End If

                cmd.Parameters.Clear()
                reader.Close()

                'login success update LastLoginAttempt and clear FailedAttempts value
                selectString = "update AdminPanelLogin set FailedAttempts = NULL, LastLoginAttempt = @LastLoginAttempt where UN_1 = @userID"
                cmd = New SqlCommand(selectString, conn)
                cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50).Value = txtUserID.Text
                cmd.Parameters.Add("LastLoginAttempt", Data.SqlDbType.DateTime).Value = Date.Now
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()

                conn.Close()

                'insert audit statistics for login user
                insertAuditLog("Success")
                Response.Redirect("../Default.aspx")
            End If
        Else
            cmd.Parameters.Clear()
            reader.Close()
            selectString = "SELECT ISNULL(FailedAttempts, 0) as FailedAttempts, LastLoginAttempt FROM AdminPanelLogin WHERE UN_1 = @userID"
            cmd = New SqlCommand(selectString, conn)
            cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50).Value = txtUserID.Text
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                If reader("FailedAttempts").ToString() <> "" And reader("LastLoginAttempt").ToString() <> "" Then
                    If CInt(reader("FailedAttempts").ToString()) > 0 And CInt(reader("FailedAttempts").ToString()) Mod 3 = 0 And DateDiff(DateInterval.Minute, CDate(reader("LastLoginAttempt").ToString()), Date.Now) < 30 Then
                        lblMessage.Text = "Sorry you are temporary locked. please try again after 30 minutes."
                        lblMessage.ForeColor = Drawing.Color.Red
                    Else
                        'login falied update LastLoginAttempt and increase FailedAttempts value
                        cmd.Parameters.Clear()
                        reader.Close()

                        selectString = "update AdminPanelLogin set FailedAttempts = ISNULL(FailedAttempts, 0) + 1, LastLoginAttempt = @LastLoginAttempt where UN_1 = @userID"
                        cmd = New SqlCommand(selectString, conn)
                        cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50).Value = txtUserID.Text
                        cmd.Parameters.Add("LastLoginAttempt", Data.SqlDbType.DateTime).Value = Date.Now
                        cmd.ExecuteNonQuery()
                        cmd.Parameters.Clear()

                        insertAuditLog("Failed")
                        lblMessage.Text = "User ID or password Invalid"
                        lblMessage.ForeColor = Drawing.Color.Red
                    End If
                Else
                    'login falied update LastLoginAttempt and increase FailedAttempts value
                    cmd.Parameters.Clear()
                    reader.Close()

                    selectString = "update AdminPanelLogin set FailedAttempts = ISNULL(FailedAttempts, 0) + 1, LastLoginAttempt = @LastLoginAttempt where UN_1 = @userID"
                    cmd = New SqlCommand(selectString, conn)
                    cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50).Value = txtUserID.Text
                    cmd.Parameters.Add("LastLoginAttempt", Data.SqlDbType.DateTime).Value = Date.Now
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()

                    insertAuditLog("Failed")
                    lblMessage.Text = "User ID or password Invalid"
                    lblMessage.ForeColor = Drawing.Color.Red
                End If
            Else
                lblMessage.Text = "User ID or password Invalid"
                lblMessage.ForeColor = Drawing.Color.Red
            End If
            conn.Close()
        End If

    End Sub

    Private Sub insertAuditLog(ByVal loginStatus As String)
        Try
            Dim ipAddress As String = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
            If String.IsNullOrEmpty(ipAddress) Then
                ipAddress = Request.ServerVariables("REMOTE_ADDR")
            End If

            Dim APIKey As String = "53a364977727650a5e9933d438b13217e954655cac05ff82ebcc877aedc7f00a"
            Dim url As String = String.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress)
            Using client As New WebClient()
                Dim json As String = client.DownloadString(url)
                Dim location As Location = New JavaScriptSerializer().Deserialize(Of Location)(json)
                Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
                conn.Open()
                Dim sqlString = "INSERT INTO AuditLog(UserID,IPAddress,Country,City,LoginDateTime,LoginStatus) VALUES (@UserID,@IPAddress,@Country,@City,@LoginDateTime,@LoginStatus); SELECT SCOPE_IDENTITY() as NewID;"
                Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(sqlString, conn)
                cmd.Parameters.Add("UserID", Data.SqlDbType.NVarChar).Value = txtUserID.Text
                cmd.Parameters.Add("IPAddress", Data.SqlDbType.NVarChar).Value = location.IPAddress
                cmd.Parameters.Add("Country", Data.SqlDbType.NVarChar).Value = location.CountryName
                cmd.Parameters.Add("City", Data.SqlDbType.NVarChar).Value = location.CityName
                cmd.Parameters.Add("LoginDateTime", Data.SqlDbType.DateTime).Value = Date.UtcNow.ToString()
                cmd.Parameters.Add("LoginStatus", Data.SqlDbType.NVarChar).Value = loginStatus
                Dim reader As SqlDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    If loginStatus = "Success" Then
                        If Request.Cookies("userAuditLogID") Is Nothing Then
                            Response.Cookies.Add(New HttpCookie("userAuditLogID", reader("NewID").ToString()))
                        Else
                            Response.Cookies("userAuditLogID").Value = reader("NewID").ToString()
                        End If
                    End If
                End If
                conn.Close()
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Public Class Location
        Public Property IPAddress() As String
            Get
                Return m_IPAddress
            End Get
            Set(ByVal value As String)
                m_IPAddress = value
            End Set
        End Property
        Private m_IPAddress As String
        Public Property CountryName() As String
            Get
                Return m_CountryName
            End Get
            Set(ByVal value As String)
                m_CountryName = value
            End Set
        End Property
        Private m_CountryName As String
        Public Property CityName() As String
            Get
                Return m_CityName
            End Get
            Set(ByVal value As String)
                m_CityName = value
            End Set
        End Property
        Private m_CityName As String
    End Class
End Class
