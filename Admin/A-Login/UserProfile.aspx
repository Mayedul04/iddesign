﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="UserProfile.aspx.vb" Inherits="Admin_UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Update User Profile</h1>
    <div class="btn-toolbar">
        <a href="../A-Login/AllUser.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Update User Profile"></asp:Label></h2>
    <p>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </p>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    User Name :</label>
                <asp:TextBox ID="txtUserName" CssClass="input-xlarge" runat="server" MaxLength="200"></asp:TextBox>
                <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtUserName"
                        runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Login ID :</label>
                <asp:TextBox ID="txtUserID" CssClass="input-xlarge" runat="server" MaxLength="50"></asp:TextBox>
                <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtUserID"
                        runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Password :(Leave it empty if you don't like to change)
                </label>
                <asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" MaxLength="20"
                    TextMode="Password"></asp:TextBox>
            </p>
            <p>
                <label>
                    Retype Password :</label>
                <asp:TextBox ID="txtRetypePassword" CssClass="input-xlarge" runat="server" MaxLength="20"
                    TextMode="Password"></asp:TextBox>
                <label>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                        Display="Dynamic" ControlToValidate="txtRetypePassword" ValidationGroup="form"
                        ErrorMessage="CompareValidator" SetFocusOnError="True">* Password not matched</asp:CompareValidator>
                </label>
            </p>
            <p>
                <label>
                    Email :</label>
                <asp:TextBox ID="txtEmail" CssClass="input-xlarge" runat="server" MaxLength="100"></asp:TextBox>
                <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail"
                        runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </label>
                <label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form"
                        Display="Dynamic" SetFocusOnError="True" runat="server" ErrorMessage="* Invalid Email"
                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></label>
            </p>
            <p>
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                    <i class="icon-save"></i>Add New</button></p>
        </div>
    </div>
    <!-- Eof content -->
    <p>
        <asp:HiddenField ID="hdhPrevEmail" runat="server" />
        <asp:HiddenField ID="hdnRole" Value="admin" runat="server" />
        <asp:HiddenField ID="hdnLastUpdated" runat="server" />
        <asp:HiddenField ID="hdnCreatedBy" runat="server" />
        <asp:HiddenField ID="hdnUserID" runat="server" />
        <asp:HiddenField ID="hdnPassword" runat="server" />
        <asp:SqlDataSource ID="sdsAdminPanelLogin" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            UpdateCommand="UPDATE [AdminPanelLogin] SET [PS_1] = @PS_1,  [Title] = @Title, [Email] = @Email, [Role] = @Role, [CreationDate] = @CreationDate, [CreatedBy] = @CreatedBy, [Status] = @Status WHERE UserID = @UserID">
            <UpdateParameters>
                <asp:ControlParameter ControlID="hdnPassword" Name="PS_1" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnRole" Name="Role" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnLastUpdated" Name="CreationDate" PropertyName="Value"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="hdnCreatedBy" Name="CreatedBy" PropertyName="Value"
                    Type="String" />
                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                <asp:QueryStringParameter Name="UserID" Type="String" QueryStringField="uid" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>
