﻿
Partial Class Admin_MasterPage_signout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Response.Cookies("userName").Value = ""
        Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userFullName").Value = ""
        Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userpass").Value = ""
        Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userRole").Value = ""
        Response.Cookies("userRole").Expires = Date.Now.AddDays(-2)
        Context.Items.Remove("userpass")

        If Request.Cookies("userAuditLogID") Is Nothing = False Then
            If Request.Cookies("userAuditLogID").Value.ToString() <> "" Then
                Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
                conn.Open()
                Dim sqlString = "update AuditLog set LogoutDateTime = @LogoutDateTime Where AuditLogID = @AuditLogID"
                Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(sqlString, conn)
                cmd.Parameters.Add("LogoutDateTime", Data.SqlDbType.DateTime).Value = Date.UtcNow.ToString()
                cmd.Parameters.Add("AuditLogID", Data.SqlDbType.BigInt).Value = Request.Cookies("userAuditLogID").Value.ToString()
                cmd.ExecuteNonQuery()
                conn.Close()
            End If
        End If
        Response.Cookies("userAuditLogID").Value = ""
        Response.Cookies("userAuditLogID").Expires = Date.Now.AddDays(-2)
        Response.Redirect("../A-Login/Login.aspx")
    End Sub
End Class
