

CREATE TABLE [dbo].[AuditLog](
	[AuditLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](100) NULL,
	[IPAddress] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[LoginDateTime] [datetime] NULL,
        [LogoutDateTime] [datetime] NULL,
        [LoginStatus] [nvarchar](100) NULL,
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[AuditLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

alter table AdminPanelLogin add FailedAttempts int null;
alter table AdminPanelLogin add LastLoginAttempt datetime null;


