﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AuditLogSecurityAsk.aspx.vb" Inherits="AuditLogSecurityAsk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Audit Log Security</h1>
    <div class="btn-toolbar">
        <a href="../A-Login/AllUser.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <p>
                <strong>*** Please enter your email and password for access audit log statistics</strong>
            </p>
            <p>
                <label>
                    Enter you email</label>
                <asp:TextBox ID="txtEmail" CssClass="input-xlarge" runat="server" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail"
                    runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label>
                    Enter your password</label>
                <asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" MaxLength="100"
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtPassword"
                    runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </p>
            <p>
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                    <i class="icon-save"></i>Submit</button></p>
        </div>
    </div>
</asp:Content>
