﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="NewUser.aspx.vb" Inherits="Admin_A_Login_NewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Add New User</h1>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="btn-toolbar">
        <a href="../A-Login/AllUser.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Username</label>
                <asp:TextBox ID="txtUserName" CssClass="input-xlarge" runat="server" MaxLength="200"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtUserName"
                    Display="Dynamic" runat="server" ErrorMessage="* Required" ValidationGroup="form"
                    SetFocusOnError="True"></asp:RequiredFieldValidator></p>
            <p>
                <label>
                    Login ID</label>
                <asp:TextBox ID="txtUserID" CssClass="input-xlarge" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtUserID"
                    runat="server" Display="Dynamic" ErrorMessage="* Required" ValidationGroup="form"
                    SetFocusOnError="True"></asp:RequiredFieldValidator></p>
            <p>
                <label>
                    Password</label>
                <asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" MaxLength="20"
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtPassword"
                    Display="Dynamic" runat="server" ErrorMessage="* Required" ValidationGroup="form"
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="txtPassword"
                    ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
                    ValidationGroup="form" Display="Dynamic" ErrorMessage="Password must contain: Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" /></p>
            <p>
                <label>
                    Retype Password</label>
                <asp:TextBox ID="txtRetypePassword" CssClass="input-xlarge" runat="server" MaxLength="20"
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtRetypePassword"
                    Display="Dynamic" runat="server" ErrorMessage="* Required" ValidationGroup="form"
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                    Display="Dynamic" ControlToValidate="txtRetypePassword" ValidationGroup="form"
                    ErrorMessage="CompareValidator" SetFocusOnError="True">* Password not matched</asp:CompareValidator></p>
            <p>
                <label>
                    Email</label>
                <asp:TextBox ID="txtEmail" CssClass="input-xlarge" runat="server" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail"
                    runat="server" ErrorMessage="* Required" ValidationGroup="form" SetFocusOnError="True"
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form"
                    Display="Dynamic" SetFocusOnError="True" runat="server" ErrorMessage="* Invalid Email"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></p>
            <p>
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                    <i class="icon-save"></i>Add New</button></p>
        </div>
    </div>
    <asp:HiddenField ID="hdnRole" Value="admin" runat="server" />
    <asp:SqlDataSource ID="sdsAdminPanelLogin" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [AdminPanelLogin] ([PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (@PS_1, @UN_1, @Title, @Email, @Role, @CreationDate, @CreatedBy, @Status)">
        <InsertParameters>
            <asp:ControlParameter ControlID="txtPassword" Name="PS_1" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtUserID" Name="UN_1" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnRole" Name="Role" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="hdnLastUpdated" DefaultValue="" Name="CreationDate"
                PropertyName="Value" Type="DateTime" />
            <asp:CookieParameter CookieName="UserName" Name="CreatedBy" Type="String" />
            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:HiddenField ID="hdnLastUpdated" runat="server" />
</asp:Content>
