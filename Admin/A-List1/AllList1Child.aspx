﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllList1Child.aspx.vb" Inherits="Admin_A_List1_AllList1Child" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Products</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <a href="../A-List1/AllList1.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary"  Visible="False"><i class='icon-save'></i> Export</asp:LinkButton>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="All Products"></asp:Label></h2>
    <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
     <cc1:TextBoxWatermarkExtender ID="txtFName_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by product code">
                                                </cc1:TextBoxWatermarkExtender>
    <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-primary"/>
    <%--<asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>--%>
    <div>
        <div class="well">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsList1_Child" DataKeyNames="ChildID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ParentTitleLabel" runat="server" Text='<%# Eval("ParentTitle") %>' />
                        </td>
                        
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img width="120px" src='<%# "../" & Eval("SmallImage") %>' />
                        </td>
                        
                        <td>
                            <asp:CheckBox ID="chkNew" AutoPostBack="true" runat="server" Checked='<%# Eval("NewArrival") %>'
                                Enabled="true" OnCheckedChanged="chkNew_CheckedChanged" />
                            <asp:HiddenField ID="hdnid" value='<%# Eval("ChildID")%>' runat="server" />
                        </td>
                       <%-- <td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" Checked='<%# Eval("Featured") %>'
                                Enabled="false" />
                        </td>--%>
                       
                        <td>
                            <a href="../A-CommonGallery/AllCommonGallery.aspx?TableName=List1_Child&TableMasterID=<%# Eval("MasterID") %>&BigImageWidth=555&BigImageHeight=277&t=<%# Server.UrlEncode(Eval("Title").ToString()) %>">Add/Edit Images</a>
                        </td>

                        <td>
                            <a href='<%#  "List1Child.aspx?l1cid=" & Eval("ChildID") & "&l1id="& Eval("ListID")  %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            
                            <a href='<%# "#" & Eval("ChildID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ChildID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    Parent Title
                                </th>
                                
                                <th id="Th2" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    Small Image
                                </th>
                              
                                <th id="Th5" runat="server">
                                    New Arrival?
                                </th>
                                <%--<th id="Th6" runat="server">
                                    Featured
                                </th>--%>
                               
                                <th>
                                    Images
                                </th>
                                
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
                 <EmptyDataTemplate>
            No data was returned.
        </EmptyDataTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsList1_Child" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT List1_Child.ChildID, List1_Child.Title as Title, List1_Child.SmallDetails, List1_Child.BigDetails, List1_Child.SmallImage, List1_Child.BigImage, List1_Child.Link, List1_Child.Featured, List1_Child.SortIndex, List1_Child.NewArrival, List1_Child.LastUpdated, List1.Title AS ParentTitle, List1_Child.ListID, List1_Child.MasterID, List1_Child.Lang FROM List1_Child INNER JOIN List1 ON List1_Child.ListID = List1.ListID WHERE (List1_Child.ListID = @ListID and List1_Child.Status = 1)  ORDER BY List1_Child.ChildID DESC" 
                DeleteCommand="DELETE FROM List1_Child WHERE (ChildID = @ChildID)">
                <DeleteParameters>
                    <asp:Parameter Name="ChildID" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:QueryStringParameter Name="ListID" QueryStringField="L1id" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>


