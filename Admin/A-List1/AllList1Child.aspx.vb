﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO

Partial Class Admin_A_List1_AllList1Child
    Inherits System.Web.UI.Page

    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("List1Child.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If sdsList1_Child.SelectParameters.Count > 1 Then
                sdsList1_Child.SelectParameters.RemoveAt(1)
            End If
        End If
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub

    Protected Sub chkNew_CheckedChanged(sender As Object, e As EventArgs)
        Dim a As Integer
        For Each row As ListViewDataItem In ListView1.Items
            Dim chkBox = DirectCast(row.FindControl("chkNew"), CheckBox)
            Dim proID = DirectCast(row.FindControl("hdnid"), HiddenField).Value
            If chkBox.Checked Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update List1_Child Set NewArrival=1 where ChildID=@ChildID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = proID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    ListView1.DataBind()
                End If
                sqlConn.Close()
            Else
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update List1_Child Set NewArrival=0 where ChildID=@ChildID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = proID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    ListView1.DataBind()
                End If
                sqlConn.Close()
            End If
        Next
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtSearch.Text <> "" Then
            sdsList1_Child.SelectCommand = "SELECT List1_Child.ChildID, List1_Child.Title, List1_Child.SmallDetails, List1_Child.BigDetails, List1_Child.SmallImage, List1_Child.BigImage, List1_Child.Link, List1_Child.Featured, List1_Child.SortIndex, List1_Child.NewArrival, List1_Child.LastUpdated, List1.Title AS ParentTitle, List1_Child.ListID, List1_Child.MasterID, List1_Child.Lang FROM List1_Child INNER JOIN List1 ON List1_Child.ListID = List1.ListID WHERE (List1_Child.ListID = @ListID) and ProductID=@ProductID ORDER BY List1_Child.ChildID DESC"
            sdsList1_Child.SelectParameters.Add("ProductID", txtSearch.Text)
        End If
    End Sub
    
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim selectString1 As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT dbo.List1_Child.ChildID, dbo.CommonGallery.BigImage  FROM  dbo.CommonGallery INNER JOIN   dbo.List1_Child ON dbo.CommonGallery.TableMasterID = dbo.List1_Child.MasterID  where List1_Child.Status=1  "
        ' Dim selectString = "SELECT MasterID, Title, BigImage  FROM   dbo.List1_Child  where List1_Child.Status=1 and BigImage<>'' and  List1_Child.MasterID>433"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            While reader.Read
                If System.IO.File.Exists(Server.MapPath("../" & reader("BigImage").ToString())) Then
                    'Label2.Text += "Ok <br/>"
                Else
                    selectString1 = "Update dbo.List1_Child Set [Status]=0 where dbo.List1_Child.ChildID=" & reader("ChildID").ToString()
                    InsertGalleryItem(selectString1)
                End If


                'selectString1 = "INSERT INTO [CommonGallery] ([Title], [SmallImage], [BigImage], [ImageAltText], [Status], [LastUpdated], [TableName],[TableMasterID]) VALUES ('" & reader("Title").ToString() & "','" & reader("BigImage").ToString().Replace("-big.jpg", "-gal.jpg") & "','" & reader("BigImage").ToString().Replace("-big.jpg", "-gal.jpg") & "', '" & reader("Title").ToString() & "'," & 1 & ", " & Date.Today() & ",'List1_Child'," & reader("MasterID") & ")"
                'InsertGalleryItem(selectString1)

            End While
        End If
        conn.Close()
    End Sub
    Public Sub InsertGalleryItem(ByVal insertstring As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(insertstring, conn)
        cmd.CommandText = insertstring
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub
    
End Class
