﻿Imports System.Data.SqlClient
Partial Class Admin_A_Home_Home
    Inherits System.Web.UI.Page
    Public totalContact As String
    Public topContact As String
    Public totalRegistration As String
    Public topRegistration As String
    Public apikey As String = "1124731f374a01d591e1e86ff29c127879f0820b"

    Public profileid As String = "78632901"

    Public timeline As String = "1m"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ltTotlaContact.Text = getTotalContact()
            'getTotalContact()
            ltTotalReg.Text = getTotalRegistration()

            ltTotalNewsLetter.Text = getTotalNewsletter()

            ltTopContact.Text = GetTopContact()

            ltTopReg.Text = GetTopRegistration()

            ltCountContact.Text = getTotalContact()

            ltCountReg.Text = getTotalRegistration()
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT  SModID, ModuleName, SortIndex, Status FROM Settings_Module where ModuleName='RegistrationForm' "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                'Boolean.TryParse(reader("Status") & "", divRegistration.Visible)
                ' Boolean.TryParse(reader("Status") & "", divTotalRegistration.Visible)
            End While
            reader.Close()

            selectString = "SELECT  SModID, ModuleName, SortIndex, Status FROM Settings_Module where ModuleName='Contact' "
            cmd = New Data.SqlClient.SqlCommand(selectString, conn)
            reader = cmd.ExecuteReader()
            While reader.Read()
                ' Boolean.TryParse(reader("Status") & "", divTotalContact.Visible)
                ' Boolean.TryParse(reader("Status") & "", SitePerformence.Visible)
            End While

            conn.Close()

        End If
    End Sub
    'Public Function ContactData() As String
    '    Dim i As Integer = 0
    '    Dim date1 As String = ""
    '    Dim str As String = ""
    '    Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    cn.Open()
    '    Dim selectString As String = "SELECT top 7 CONVERT(varchar, ContactDate, 101)  as ContactDate, COUNT(ContactID) AS TotalContact  FROM   Contact GROUP BY  CONVERT(varchar, ContactDate, 101) order by CONVERT(varchar, ContactDate, 101) DESC"
    '    Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    While reader.Read
    '        i = reader("TotalContact")
    '        date1 = reader("ContactDate")
    '        str = str & "['" & date1.ToString() & "', " & i.ToString() & ", " & i.ToString() & "] ,"
    '    End While

    '    reader.Close()
    '    cn.Close()

    '    Return str
    'End Function

    'Public Function RegistrationData() As String
    '    Dim i As Integer = 0
    '    Dim date1 As String = ""
    '    Dim str As String = ""
    '    Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    cn.Open()
    '    Dim selectString As String = "SELECT top 7 CONVERT(varchar, RegistrationDate, 101)  as RegistrationDate, COUNT(RegID) AS TotalReg  FROM Reg_Member_ GROUP BY  CONVERT(varchar, RegistrationDate, 101) order by CONVERT(varchar, RegistrationDate, 101) DESC"
    '    Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    While reader.Read
    '        i = reader("TotalReg")
    '        date1 = reader("RegistrationDate")
    '        str = str & "['" & date1.ToString() & "', " & i.ToString() & ", " & i.ToString() & "] ,"
    '    End While

    '    reader.Close()
    '    cn.Close()

    '    Return str
    'End Function

    'Public Function SubscriptionData() As String
    '    Dim i As Integer = 0
    '    Dim date1 As String = ""
    '    Dim str As String = ""
    '    Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    cn.Open()
    '    Dim selectString As String = "SELECT top 7 CONVERT(varchar, SubscriptionDate, 101)  as SubscriptionDate, COUNT(NewSubID) AS TotalSubscription  FROM NewsletterSubscription GROUP BY  CONVERT(varchar, SubscriptionDate, 101) order by CONVERT(varchar, SubscriptionDate, 101) DESC"
    '    Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    While reader.Read
    '        i = reader("TotalSubscription")
    '        date1 = reader("SubscriptionDate")
    '        str = str & "['" & date1.ToString() & "', " & i.ToString() & ", " & i.ToString() & "] ,"
    '    End While

    '    reader.Close()
    '    cn.Close()

    '    Return str
    'End Function

    Public Function Contact_SubscriptionData() As String

        Dim str As String = ""
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim selectString As String = "SELECT        GroupContacts.totalContact, GroupSubs.totalSubs, ISNULL(GroupContacts.ContactDate, GroupSubs.SubscriptionDate) AS TheDate " & _
            " FROM            (SELECT       isNull( COUNT(ContactID),0) AS totalContact, ContactDate " & _
            "        FROM dbo.Contact  " & _
            "                          GROUP BY ContactDate) AS GroupContacts FULL OUTER JOIN " & _
            "                             (SELECT       isNull( COUNT(NewSubID),0) AS totalSubs, SubscriptionDate " & _
            "        FROM dbo.NewsletterSubscription " & _
            "                               GROUP BY SubscriptionDate) AS GroupSubs ON GroupContacts.ContactDate = GroupSubs.SubscriptionDate "

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            str = str & "['" & DateTime.Parse(reader("theDate") & "").ToShortDateString() & "', " & If(reader("totalContact") & "" = "", "0", reader("totalContact")) & ", " & If(reader("totalSubs") & "" = "", "0", reader("totalSubs")) & "] ,"
        End While
        str = str.TrimEnd(",")
        reader.Close()
        cn.Close()

        Return str
    End Function

    Public Function getTotalRegistration() As String
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim M As String = ""
        Dim selectString As String = "select count(RegID) as totalRegistration from Reg_Member_"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            M += reader("totalRegistration").ToString()
        End If
        reader.Close()

        'Dim selectString1 As String = "select top 8 FullName,Comments from Reg_Member_ order by RegistrationDate Desc"
        'Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
        'Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        'While reader1.Read
        '    topRegistration += "<li><a href=""../A-RegistrationForm/AllRegistrationForm.aspx"">" + reader1("Comments").ToString() + "</a></li>"
        'End While
        'reader1.Close()
        cn.Close()
        Return M
    End Function

    Public Function getTotalContact() As String
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim M As String = ""
        Dim selectString As String = "select count(ContactID) as totalContact from contact"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            M = reader("totalContact").ToString()
        End If
        reader.Close()

        'Dim selectString1 As String = "select top 8 FullName,Message from contact order by contactDate Desc"
        'Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
        'Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        'While reader1.Read
        '    M += "<a href=""../A-Contact/AllContact.aspx"">" + reader1("Message").ToString() + "</a>"
        'End While
        'reader1.Close()
        cn.Close()

        Return M
    End Function

    Public Function getTotalNewsletter() As String
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim M As String = ""
        Dim selectString As String = "select count(NewSubID) as totalNewsletter from NewsletterSubscription"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            M = reader("totalNewsletter").ToString()
        End If
        reader.Close()

        'Dim selectString1 As String = "select top 8 FullName,Message from contact order by contactDate Desc"
        'Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
        'Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        'While reader1.Read
        '    M += "<a href=""../A-Contact/AllContact.aspx"">" + reader1("Message").ToString() + "</a>"
        'End While
        'reader1.Close()
        cn.Close()

        Return M
    End Function


    Public Function GetTopContact() As String
        Dim M As String = ""
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim selectString1 As String = "select top 5 FullName,Message,contactDate from contact order by contactDate Desc"
        Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        While reader1.Read
  
            M += "<tr>"
            M += "<td>" & reader1("FullName").ToString & "</td>"
            M += "<td>" & reader1("Message").ToString & "</td>"
            M += "<td>" & Convert.ToDateTime(reader1("contactDate").ToString).ToShortDateString & "</td>"
            M += "</tr>"

        End While
        reader1.Close()
        cn.Close()

        Return M

    End Function

    Public Function GetTopRegistration() As String

        Dim M As String = ""
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        cn.Open()
        Dim selectString1 As String = "select top 5 FullName,Comments,RegistrationDate from Reg_Member_ order by RegistrationDate Desc"
        Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        While reader1.Read
            topRegistration += "<li><a href=""../A-RegistrationForm/AllRegistrationForm.aspx"">" + reader1("Comments").ToString() + "</a></li>"

            M += "<tr>"
            M += "<td>" & reader1("FullName").ToString & "</td>"
            M += "<td>" & reader1("Comments").ToString & "</td>"
            M += "<td>" & Convert.ToDateTime(reader1("RegistrationDate").ToString).ToShortDateString & "</td>"
            M += "</tr>"

        End While
        reader1.Close()
        cn.Close()
        Return M


    End Function


End Class
