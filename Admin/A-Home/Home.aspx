﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Admin_A_Home_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
          ['Date', 'Contacts', 'Newsletter Subscription'],
          <%= Contact_SubscriptionData() %>
        ]);

            var options = {
                title: ''
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_ContactAndNewsletter'));
            chart.draw(data, options);
        }
    </script>

            <script src='../javascripts/oocharts.js'></script>

        
           <script type="text/javascript">

               window.onload = function () {

                   oo.setAPIKey('<%= apikey %>');

                   oo.load(function () {

                       var table = new oo.Table('<%= profileid %>', '<%= timeline %>');

                       table.addMetric("ga:visits", "Visits");

                       table.addDimension("ga:city", "City");

                       table.draw('visitortable');





                       var pie = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie.setMetric("ga:visits", "Visits");
                       pie.setDimension("ga:browser");

                       pie.draw('visitorpie');




                       var pie2 = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie2.setMetric("ga:organicSearches", "Search");
                       pie2.setDimension("ga:continent");

                       pie2.draw('visitorpie2');



                       
                       var bar = new oo.Bar('<%= profileid %>', '<%= timeline %>');

                       bar.addMetric("ga:visits", "Visits");

                       bar.addMetric("ga:newVisits", "New Visits");

                       bar.setDimension("ga:continent");

                       bar.draw('visitorbar');


                       var timeline = new oo.Timeline('<%= profileid %>', '<%= timeline %>');

                       timeline.addMetric("ga:visits", "Visits");

                       timeline.addMetric("ga:newVisits", "New Visits");

                       timeline.draw('visitortimeline');



                       var Column1 = new oo.Column('<%= profileid %>', '<%= timeline %>');

                       //                       Column1.addMetric("ga:organicSearches", "Visits");

                       //                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.addMetric("ga:visits", "Visits");

                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.setDimension("ga:deviceCategory");

                       Column1.draw('visitorcolumn');



                   });
               };

        </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="stats">
    <p class="stat"><span class="number">
    <asp:Literal ID="ltTotlaContact" runat="server"></asp:Literal></span>Contact</p>
    <p class="stat"><span class="number">
        <asp:Literal ID="ltTotalReg" runat="server"></asp:Literal>
    </span>Registration</p>
    <p class="stat"><span class="number">
    <asp:Literal ID="ltTotalNewsLetter" runat="server"></asp:Literal>
    </span>Newsletter</p>
</div>
<h1 class="page-title">Dashboard</h1>

<div class="row-fluid">
    <div class="block">
        <p class="block-heading" data-toggle="collapse" data-target="#chart-container">Visitor Vs Timeline</p>
        <div id="chart-container" class="block-body collapse in">
            <%--<div id="line-chart"></div>--%>
            <%--<div id="chart_ContactAndNewsletter" class="mainchart">
            </div>--%>
              <div id='visitortimeline'></div>  
        </div>
    </div>
</div>

<div class="row-fluid">
     <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorColumn">Visitor Vs Device Category</div>    
        <div id="widgetVisitorColumn" class="block-body collapse in">
         <div id='visitorcolumn'></div> 
         </div>
    </div>
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorBar">Visitor Vs Continent</div>
        <div id="widgetVisitorBar" class="block-body collapse in">
             <div id='visitorbar'></div>
        </div>
    </div>

</div>

<div class="row-fluid">

    <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorPie">Visitor Vs Browser</div>    
        <div id="widgetVisitorPie" class="block-body collapse in">
         <div id='visitorpie'></div>
         </div>
    </div>

        <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorPie2">Search Vs Continent</div>    
        <div id="widgetVisitorPie2" class="block-body collapse in">
              <div id='visitorpie2'></div>
         </div>
        </div>

</div>







<div class="row-fluid">
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#widget2container">Contact us<span class="label label-warning">+<asp:Literal ID="ltCountContact" runat="server"></asp:Literal></span></div>
        <div id="widget2container" class="block-body collapse in">
           <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Message</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                    <asp:Literal ID="ltTopContact" runat="server"></asp:Literal>
              </tbody>
            </table>
            <p><a href="../A-Contact/AllContact.aspx" class="btn btn-primary btn-large" >More &raquo;</a></p>
        </div>
    </div>
    <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widget3container">Registration<span class="label label-warning">+<asp:Literal ID="ltCountReg" runat="server"></asp:Literal></span></div>
       
        <div id="widget3container" class="block-body collapse in">
           <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Comments</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                    <asp:Literal ID="ltTopReg" runat="server"></asp:Literal>
              </tbody>
            </table>
            <p><a class="btn btn-primary btn-large" href="../A-RegistrationForm/AllRegistrationForm.aspx"  >More &raquo;</a></p>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#tablewidget">Users</div>
        <div id="tablewidget" class="block-body collapse in">
            <ol>
                        <li><a href="../A-HTML/AllHTML.aspx" title="">
                            <img alt="" src="../images/user.png">
                            Text</a></li>
                        <li><a href="../A-Banner/AllTopBanner.aspx" title="">
                            <img alt="" src="../images/user.png">
                            Top Banners</a></li>
                        <li><a href="../A-Restaurant/AllRestaurant.aspx" title="">
                            <img alt="" src="../images/user.png">
                            Restaurant</a></li>
                    </ol>
        </div>
    </div>
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#widget1container">Collapsible </div>
        <div id="widget1container" class="block-body collapse in">
            <h2>Important Notice</h2>
            <p>This template was developed with <a href="http://middlemanapp.com/" target="_blank">Middleman</a> and includes .erb layouts and views.</p>
            <p>All of the views you see here (sign in, sign up, users, etc) are already split up so you don't have to waste your time doing it yourself!</p>
            <p>The layout.erb file includes the header, footer, and side navigation and all of the views are broken out into their own files.</p>
            <p>If you aren't using Ruby, there is also a set of plain HTML files for each page, just like you would expect.</p>
        </div>
    </div>
</div>


</asp:Content>

