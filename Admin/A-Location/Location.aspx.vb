﻿
Partial Class Admin_A_Partner_Partner
    Inherits System.Web.UI.Page


    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("locid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update Location"
                LoadContent(Request.QueryString("locid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now



        'hdnDetails.Value = FCKeditor1.Value
        hdnDetails.Value = txtDetails.Text
        If String.IsNullOrEmpty(Request.QueryString("locid")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If

            If sdsList_Locations.Insert() > 0 Then
                '' InsertIntoSEO()
                Response.Redirect("AllLocations.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsList_Locations.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (isNull( Max( MasterID),0)+1) as MaxMasterID  FROM   List_Locations "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        LocationID, Title, Email, Phone, Mobile, Fax, Address1,Address2, MapCode, City, Featured, SortIndex, Status, LastUpdated,Lang,MasterID    FROM   List_Locations where LocationID=@LocationID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("LocationID", Data.SqlDbType.Int)
        cmd.Parameters("LocationID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            AddressTextBox.Text = reader("Address1") & ""
            'FCKeditor1.Value = reader("BigDetails") & ""
            txtDetails.Text = reader("MapCode") & ""
            ddlCity.SelectedValue = reader("City").ToString()
           


            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""

            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            ', Email, Phone, Mobile, Fax, Address
            EmailTextBox.Text = reader("Email") & ""
            PhoneTextBox.Text = reader("Phone") & ""
            MobileTextBox.Text = reader("Mobile") & ""
            FaxTextBox.Text = reader("Fax") & ""
            txtAddline2.Text = reader("Address2") & ""

        End If
        conn.Close()
    End Sub



End Class
