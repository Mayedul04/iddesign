﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="LocationEdit.aspx.vb" Inherits="Admin_A_Partners_PartnerEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="block">
    <div class="block-heading">Location</div>
    <div class="block-body">

    <h1 class="page-title">
        Partner</h1>
    <%--<div class="btn-toolbar">
        <a href="../A-Partners/AllPartner.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>--%>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <label class="success-left-top"></label><label class="success-right-top"></label><label
                class="success-left-bot"></label><label class="success-right-bot"></label>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <label class="error-left-top"></label><label class="error-right-top"></label><label class="error-left-bot">
            </label><label class="error-right-bot"></label>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    City</label>
                <asp:DropDownList ID="ddlCity" runat="server">
                    <asp:ListItem>Dubai</asp:ListItem>
                    <asp:ListItem>Abu Dhabi</asp:ListItem>
                </asp:DropDownList>
            </p>
            <p>
                <label>
                    Name:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Address(Line 1) :
                </label>
                <asp:TextBox ID="AddressTextBox" runat="server" class="txtarea" TextMode="MultiLine" MaxLength="500" Rows="3" />
                <label class="red">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="AddressTextBox"
                        ValidationExpression="^[\s\S\w\W\d\D]{0,500}$" ErrorMessage="* character limit is 500"
                        ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <label>
                    Address(Line 2) :
                </label>
                <asp:TextBox ID="txtAddline2" runat="server" class="txtarea" TextMode="MultiLine" MaxLength="500" Rows="3" />
                <label class="red">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAddline2"
                        ValidationExpression="^[\s\S\w\W\d\D]{0,500}$" ErrorMessage="* character limit is 500"
                        ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </label>
            </p>
            <asp:Panel ID="pnlMap" runat="server">
                <p>
                    <label>Map Code:</label>
                    <%--<FCKeditorV2:FCKeditor ID="FCKeditor1" runat="server" BasePath="../fckeditor/" Height="300px"
                        Width="700px">
                    </FCKeditorV2:FCKeditor>--%>
                    <asp:HiddenField ID="hdnDetails" runat="server" />
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );

                    </script>
                </p>
            </asp:Panel>
            <p>
                <label>
                    Email:</label>
                <asp:TextBox ID="EmailTextBox" runat="server" class="txt" MaxLength="200" />
                <label class="red">
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="form">*</asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="form">*</asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <label>
                    Phone:
                </label>
                <asp:TextBox ID="PhoneTextBox" runat="server" class="txt" MaxLength="30" />
                <label>
                </label>
            </p>
            <p>
                <label>
                    Mobile:
                </label>
                <asp:TextBox ID="MobileTextBox" runat="server" class="txt" MaxLength="30" />
                <label>
                </label>
            </p>
            <p>
                <label>
                    Fax:
                </label>
                <asp:TextBox ID="FaxTextBox" runat="server" class="txt" MaxLength="30" />
                <label>
                </label>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style="display: none;">
                <label>Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>

        <asp:SqlDataSource ID="sdsList_Locations" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [List_Locations] WHERE [LocationID] = @LocationID" InsertCommand="INSERT INTO List_Locations([Title],[Email] ,[Phone] ,[Mobile] ,[Fax],[Address1],[Address2] ,[MapCode],[City] ,[Featured] ,[SortIndex] ,[Status] ,[LastUpdated],[MasterID] ,[Lang]) VALUES (@Title,@Email ,@Phone ,@Mobile ,@Fax,@Address1,@Address2 ,@MapCode,@City ,@Featured ,@SortIndex ,@Status ,@LastUpdated,@MasterID ,@Lang)"
            SelectCommand="SELECT * FROM [List_Locations]"
            UpdateCommand="UPDATE List_Locations SET Title=@Title, Email=@Email ,Phone=@Phone ,Mobile=@Mobile ,Fax=@Fax,Address1=@Address1,Address2=@Address2 ,MapCode=@MapCode,City=@City ,Featured=@Featured ,SortIndex=@SortIndex ,Status=@Status ,LastUpdated=@LastUpdated,MasterID=@MasterID ,Lang=@Lang WHERE (LocationID = @LocationID)">
            <DeleteParameters>
                <asp:Parameter Name="LocationID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" />
                <asp:ControlParameter ControlID="EmailTextBox" Name="Email" PropertyName="Text" />
                <asp:ControlParameter ControlID="PhoneTextBox" Name="Phone" PropertyName="Text" />
                <asp:ControlParameter ControlID="MobileTextBox" Name="Mobile" PropertyName="Text" />
                <asp:ControlParameter ControlID="FaxTextBox" Name="Fax" PropertyName="Text" />
                <asp:ControlParameter ControlID="AddressTextBox" Name="Address1" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtAddline2" Name="Address2" PropertyName="Text" />
                <asp:ControlParameter ControlID="hdnDetails" Name="MapCode" PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlCity" Name="City" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang"
                    PropertyName="SelectedValue" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" />
                <asp:ControlParameter ControlID="EmailTextBox" Name="Email" PropertyName="Text" />
                <asp:ControlParameter ControlID="PhoneTextBox" Name="Phone" PropertyName="Text" />
                <asp:ControlParameter ControlID="MobileTextBox" Name="Mobile" PropertyName="Text" />
                <asp:ControlParameter ControlID="FaxTextBox" Name="Fax" PropertyName="Text" />
                <asp:ControlParameter ControlID="AddressTextBox" Name="Address1" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtAddline2" Name="Address2" PropertyName="Text" />
                <asp:ControlParameter ControlID="hdnDetails" Name="MapCode" PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlCity" Name="City" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang"
                    PropertyName="SelectedValue" />
                <asp:QueryStringParameter Name="LocationID" QueryStringField="locid" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </div>
</div>
</asp:Content>

