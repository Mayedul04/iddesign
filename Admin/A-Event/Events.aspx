﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" ValidateRequest="false"
    AutoEventWireup="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false"
    CodeFile="Events.aspx.vb" Inherits="Events" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
     <h1 class="page-title">Event</h1>
    <div class="btn-toolbar">
        <a href="../A-Event/AllEvent.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Event Add/Edit"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>

    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
    
            <p>
                <label>Title</label>
                <asp:TextBox CssClass="input-xlarge" ID="txtTitle" runat="server"></asp:TextBox>
                <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Small Details</label>
                <asp:TextBox CssClass="input-xlarge" ID="txtSmallDetails" runat="server" Width="550px" Height="100px"
                    TextMode="MultiLine"></asp:TextBox>
            </p>
            <p>
                <label>Details</label>
                <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                <script>

                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.

                    CKEDITOR.replace('<%=txtDetails.ClientID %>',
                        {
                            filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                            "extraPlugins": "imagebrowser",
                            "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                        }
                    ); 
            
                </script>
                
            </p>
            <p>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                            <asp:TextBox ID="txtStartDate" ValidationGroup="date" runat="server"></asp:TextBox>&nbsp;&nbsp;
                            <label>
                                <asp:RequiredFieldValidator ValidationGroup="date" ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="txtStartDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" TargetControlID="txtStartDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:Label ID="lblEndDate" runat="server" ValidationGroup="date" Text="End Date"></asp:Label><asp:TextBox
                                ID="txtEndDate" runat="server"></asp:TextBox>
                            <label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="date" runat="server"
                                    ControlToValidate="txtEndDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <cc1:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" TargetControlID="txtEndDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label3" runat="server" ValidationGroup="date" Text="Time"></asp:Label><asp:TextBox
                                ID="txtDuration" runat="server"></asp:TextBox>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:Button ID="btnUpdate" Text="Submit" runat="server" CssClass="bttn1" Visible="false"
                                ValidationGroup="date" />
                            <asp:Button ID="btnAddnew" Text="Add New" runat="server" CssClass="bttn1" ValidationGroup="date" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:GridView ID="gvEventDetails" runat="server" DataKeyNames="EventDetailsID" 
                                DataSourceID="SqlDatasourceEventDetails" AutoGenerateColumns="False" Width="100%"
                                HorizontalAlign="Center" AllowSorting="True" CssClass="table" 
                                GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="StartDate" DataFormatString="{0:d}" HeaderText="Start Date"
                                        SortExpression="StartDate" />
                                    <asp:BoundField DataField="EndDate" DataFormatString="{0:d}" HeaderText="End Date"
                                        SortExpression="EndDate" />
                                    <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" />
                                    <asp:TemplateField HeaderText="Commands" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgUpdate" CommandArgument='<%# Eval("EventDetailsID") %>' ImageUrl="../assets/images/icons/edit.jpg"
                                                runat="server" CausesValidation="False" Text="Update" OnClick="imgUpdate_Click" />
                                            <asp:ImageButton ID="imgDelButton" ImageUrl="../assets/images/icons/delete.png" runat="server"
                                                CausesValidation="False" CommandName="Delete" OnClientClick='return confirm("Are you sure you want to delete this Date?");'
                                                Text="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle HorizontalAlign="Center" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                    BorderWidth="1px" />
                                <HeaderStyle BorderColor="#DDDDDD" ForeColor="#144265" BorderStyle="Solid" 
                                    BorderWidth="1px" />
                                <AlternatingRowStyle BorderStyle="None" CssClass="alt" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </p>
            <p>
                <label>Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                <asp:Image ID="imgSmallImage" runat="server" />
            </p>
            <p>
                <asp:FileUpload ID="fuSmallImage" runat="server" />
                <asp:HiddenField ID="hdnSmallImage" runat="server" />
            </p>
            <p>
                <label>Big Image (Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                <asp:Image ID="imgBigImage" runat="server" />
            </p>
            <p>
                <asp:FileUpload ID="fuBigImage" runat="server" />
                <asp:HiddenField ID="hdnBigImage" runat="server" />
            </p>
            <p>
                <label>Image Alt Text</label>
                <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>
            <p>
                <label>Featured</label>
                <asp:CheckBox ID="chkFeatured" runat="server" />
            </p>
            <p>
                <label>Sort Order: </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
            <p>
                <label>Status</label>
                <asp:CheckBox ID="chkStatus" runat="server" Checked="True" />
            </p>
            <p>
                <label>Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                </asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <p>
                <label>Last Updated :</label>
                <asp:Label ID="lblLastUpdated" runat="server"></asp:Label></p>
            <div class="btn-toolbar">
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        </div>

        </div>
    </div>



    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDatasourceEvent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Event] WHERE [EventID] = @EventID" InsertCommand="INSERT INTO [List_Event] ([Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Featured], [SortIndex], [Status], [LastUpdated],MasterID, Lang) VALUES (@Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @ImageAltText, @Featured, @SortIndex, @Status, @LastUpdated,@MasterID, @Lang)"
        SelectCommand="SELECT * FROM [List_Event]" UpdateCommand="UPDATE [List_Event] SET [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText, [Featured] =@Featured, [SortIndex] = @SortIndex,  [Status] = @Status, [LastUpdated] = @LastUpdated,MasterID=@MasterID, Lang=@Lang  WHERE [EventID] = @EventID">
        <DeleteParameters>
            <asp:Parameter Name="EventID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" Type="String" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lblLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" Type="String" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lblLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:QueryStringParameter Name="EventID" QueryStringField="eventId" Type="Int32" />
            
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDatasourceEventDetails" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Event_Details] WHERE [EventDetailsID] = @EventDetailsID"
        InsertCommand="INSERT INTO [List_Event_Details] ([EventID], [StartDate], [EndDate],[Time]) VALUES (@EventID, @StartDate, @EndDate, @Time)"
        SelectCommand="SELECT * FROM [List_Event_Details] where EventID=@EventID" UpdateCommand="UPDATE [List_Event_Details] SET [StartDate] = @StartDate, [EndDate] = @EndDate, [Time] = @Time WHERE [EventDetailsID] = @EventDetailsID">
        <DeleteParameters>
            <asp:Parameter Name="EventDetailsID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="hdnEventID" Name="EventID" PropertyName="Value"
                Type="Int32" />
            <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="txtDuration" Name="Time" PropertyName="Text" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="hdnEventID" Name="EventID" PropertyName="Value" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="txtDuration" Name="Time" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnEventDetailsID" Name="EventDetailsID" PropertyName="Value"
                Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:HiddenField ID="hdnEventID" runat="server" />
    <asp:HiddenField ID="hdnEventDetailsID" runat="server" />
    <asp:HiddenField ID="hdnNewID" runat="server" />
    <asp:SqlDataSource ID="SqlDataSourceSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [SEO] ([PageType], [PageID],  [SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot]) VALUES (@PageType, @PageID,  @SEOTitle, @SEODescription, @SEOKeyWord, @SEORobot)">
        <InsertParameters>
            <asp:Parameter Name="PageType" Type="String" DefaultValue="Events" />
            <asp:ControlParameter ControlID="hdnNewID" DefaultValue="" Name="PageID" PropertyName="Value"
                Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SEODescription" PropertyName="Text"
                Type="String" />
            <asp:Parameter Name="SEOKeyWord" Type="String" DefaultValue="" />
            <asp:Parameter Name="SEORobot" Type="Boolean" DefaultValue="true" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
