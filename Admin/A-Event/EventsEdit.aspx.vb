﻿Imports System.Data.OleDb
Imports System.Drawing.Imaging
Imports System.Data.SqlClient

Partial Class EventsEdit
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Utility.GetDimentionSetting("Event", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")

        If IsPostBack = False Then
            If Request.QueryString("eventId") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM List_Event where EventID=" & Request.QueryString("eventId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()
                txtTitle.Text = reader("Title").ToString()
                txtSmallDetails.Text = reader("SmallDetails").ToString()
                txtDetails.Text = reader("BigDetails").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                imgBigImage.ImageUrl = "~/Admin/" + reader("BigImage").ToString()
                hdnBigImage.Value = reader("BigImage").ToString()
                chkFeatured.Checked = reader("Featured").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                lblLastUpdated.Text = reader("LastUpdated").ToString()
                txtImgAlt.Text = reader("ImageAltText") & ""
                hdnMasterID.Value = reader("MasterID") & ""
                ddlLang.SelectedValue = reader("Lang") & ""

                sqlConn.Close()
                imgSmallImage.Visible = True
                imgBigImage.Visible = True
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"

            End If

            If Request.QueryString("eventId") <> "" Then
                hdnEventID.Value = Request.QueryString("eventId")
                gvEventDetails.DataBind()
            Else
                Dim Gen As System.Random
                Gen = New System.Random(My.Computer.Clock.TickCount)
                hdnEventID.Value = Gen.Next(1, 101).ToString
                gvEventDetails.DataBind()
            End If

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            lblLastUpdated.Text = DateTime.Now()
            If fuSmallImage.FileName <> "" Then
                hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
                imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
                imgSmallImage.Visible = True
            End If
            If fuBigImage.FileName <> "" Then
                hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
                imgBigImage.ImageUrl = "~/Admin/" + hdnBigImage.Value
                imgBigImage.Visible = True
            End If

            Dim a As Integer
            If String.IsNullOrEmpty(Request.QueryString("eventId")) Or Request.QueryString("new") = 1 Then
                If Request.QueryString("new") <> "1" Then
                    hdnMasterID.Value = GetMasterID()
                End If

                If gvEventDetails.Rows.Count > 0 Then
                    a = SqlDatasourceEvent.Insert()
                    hdnNewID.Value = getNewID()
                    updateEventDetails()
                    a = SqlDataSourceSEO.Insert()
                    Response.Redirect("AllEvent.aspx")
                Else
                    divSuccess.Visible = False
                    divError.Visible = True
                    lblErrMessage.Text = "Please add at least one Start Date and End Date."
                    Return
                End If


            Else

                If gvEventDetails.Rows.Count > 0 Then
                    a = SqlDatasourceEvent.Update()
                Else
                    divSuccess.Visible = False
                    divError.Visible = True
                    lblErrMessage.Text = "Please add at least one Start Date and End Date."
                    Return
                End If

            End If

            If a > 0 Then
                divSuccess.Visible = True
                divError.Visible = False
            Else
                divSuccess.Visible = False
                divError.Visible = True
            End If
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
        End Try
    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_Event "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function


    Private Function getNewID() As String
        Dim newID As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = "SELECT Max(EventID) as MaxID FROM List_Event"
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        reader.Read()
        newID = CInt(reader("MaxID").ToString())
        sqlConn.Close()
        Return newID
    End Function

    Private Sub updateEventDetails()
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = "update List_Event_Details set EventID=" + hdnNewID.Value + " where EventID=" + hdnEventID.Value + ""
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.ExecuteNonQuery()
        sqlConn.Close()
    End Sub

    Protected Sub btnAddnew_Click(sender As Object, e As System.EventArgs) Handles btnAddnew.Click
        Dim a As Integer = SqlDatasourceEventDetails.Insert()
        gvEventDetails.DataBind()
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        txtDuration.Text = ""
        divSuccess.Visible = False
        divError.Visible = False
        lblErrMessage.Text = ""
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        Dim a As Integer = SqlDatasourceEventDetails.Update()
        gvEventDetails.DataBind()
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        txtDuration.Text = ""
        btnUpdate.Visible = False
        btnAddnew.Visible = True
    End Sub

    Protected Sub imgUpdate_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Dim imgUpdate As ImageButton = DirectCast(sender, ImageButton)
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = "SELECT * FROM List_Event_Details WHERE EventDetailsID=" & imgUpdate.CommandArgument.ToString()
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        reader.Read()
        btnUpdate.Visible = True
        btnAddnew.Visible = False
        hdnEventDetailsID.Value = imgUpdate.CommandArgument.ToString()
        txtStartDate.Text = DateTime.Parse(reader("StartDate").ToString()).ToShortDateString()
        txtEndDate.Text = DateTime.Parse(reader("EndDate").ToString()).ToShortDateString()
        txtDuration.Text = reader("Time").ToString()
        sqlConn.Close()
    End Sub
End Class
