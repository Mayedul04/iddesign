﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Applicants.aspx.vb" Inherits="Admin_A_Career_Report_Applicants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <h1 class="page-title">Career Applicant</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnDownload"  class="btn btn-primary"><i class="icon-save"></i> Download </button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Applicant List"></asp:Label></h2>
        <div>

           <div class="well">
           
          <table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                            <th >
                                                Photo</th>
                                                                                       
                                            <th >
                                               Job Code</th>
                                            <th >
                                                Contact Info</th>
                                            <th >
                                                CV</th>
                                            <th >
                                                Other Attach.</th>
                                            <th >
                                                Date </th>

                                            <th style="width: 20px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                     <asp:ListView ID="GridView1" runat="server" DataKeyNames="CareerApplicantID" 
                    DataSourceID="sdsCareerApplicant">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            <td>
                          
                                 <img  src='<%# "../Admin/"& Eval("photo") %>' alt="Edit" width="100px" />
                            </td>
                            <td>
                                <asp:Label ID="lblJobcode" runat="server" Text='<%# Eval("JobCode") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' /><br />
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>' /><br />
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
      
                            </td>
                            <td>
                                 <a href='<%# "../" & Eval("cv")%>' target="_blank">CV</a>  
                            </td>
                            <td>
                                 <a href='<%# "../" & Eval("AttachOthers")%>' target="_blank">Other Attachment</a>  
                            </td>
                          


                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy") %>' />
                            </td>
                           

                             <td>

                                <a href='<%# "#" & Eval("CareerApplicantID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("CareerApplicantID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="25"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                                
                                           
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 
                                
          </div> 
    <!-- Eof content -->
         
            <asp:SqlDataSource ID="sdsCareerApplicant" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                DeleteCommand="DELETE FROM [CareerApplicant] WHERE [CareerApplicantID] = @CareerApplicantID" 
                SelectCommand="SELECT [CareerApplicantID],[Name], [JobCode],(''''+[Phone]) as Phone, [Email], [CV],[Photo],[AttachOthers], [LastUpdated],[Message] FROM [CareerApplicant] order by LastUpdated desc">
                <DeleteParameters>
                    <asp:Parameter Name="CareerApplicantID" Type="Int64" />
                </DeleteParameters>
                
               </asp:SqlDataSource>
   
   
       </div>
    <!-- Eof content -->

</asp:Content>

