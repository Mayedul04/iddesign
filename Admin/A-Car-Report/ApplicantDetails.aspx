﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="ApplicantDetails.aspx.vb" Inherits="Admin_A_Career_Report_ApplicantDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1>Career Opportunities Applications</h1>
    <div style="padding-bottom: 32px;">
        <div style="float: left;">
            <asp:Button ID="btnCareers" runat="server" Text="Careers"     CssClass="bttn1" />
            
            <%--<asp:ImageButton ID="btnAddNew" runat="server" ImageUrl="~/Admin/assets/images/bttn/addnew.png" ToolTip="Add New" />--%>
        </div>
        <div style="float: right;">
            <asp:ImageButton ID="btnBack" runat="server" ImageUrl="~/Admin/assets/images/bttn/back.png" ToolTip="Back To Application List" />
            
        </div>
    </div>
    <!-- content -->
    <div class="content-wrap">
        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="Application" ></asp:Label>
        </h2>
        <div>
    


            <asp:FormView ID="FormView1" runat="server" DataKeyNames="CareerApplicantID" 
                DataSourceID="sdsApplication" EnableModelValidation="True" 
                BorderStyle="None" CellPadding="8">
                
                <ItemTemplate>
                    <h4><asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("DesiredPosition") %>' /></h2>
                    <br />
                    <asp:Label ID="SmallDetailsLabel" runat="server" 
                        Text='<%# Bind("SmallDetails") %>' />
                    <br />
                    <strong>Applicant Name:</strong>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                    <br />
                    <strong>Email:</strong>
                    <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
                    <br />
                    <strong>Subject:</strong>
                    <asp:Label ID="SubjectLabel" runat="server" Text='<%# Bind("Subject") %>' />
                    <br />
                    <strong>Message:</strong><br />
                    <asp:Label ID="MessageLabel" runat="server" 
                        Text='<%# Bind("Message") %>' />
                    <br />
                    <strong>Photo:</strong><br />
                    <asp:Label ID="Label1" runat="server" 
                        Text='<%# Bind("CoverLetter") %>' />
                     <asp:Image ID="Image1" ImageUrl='<%= "Admin/" & Bind("Photo") %>' runat="server" />
                    <br />
                    <strong>CV:</strong>
                    <a href='<%# "../" & Eval("cv")%>' target="_blank">CV Link</a>

                    <br />
                    
                     <br />
                    <strong>Other Attachment:</strong>
                    <a href='<%# "../" & Eval("AttachOthers")%>' target="_blank">CV Link</a>

                    <br />
                  
                    <strong>Applied At:</strong>
                    <asp:Label ID="LastUpdatedLabel" runat="server" 
                        Text='<%# Eval("LastUpdated", "{0:d}") %>' />
                    

                </ItemTemplate>
                <RowStyle BorderStyle="None" />
            </asp:FormView>
            <asp:SqlDataSource ID="sdsApplication" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="Select * from CareerApplicant order by LastUpdated desc">
                <SelectParameters>
                    <asp:QueryStringParameter Name="CareerApplicantID" QueryStringField="caid" />
                </SelectParameters>
            </asp:SqlDataSource>


        </div>
         <span class="left-bottom"></span><span class="left-top"></span><span class="right-bottom">
        </span><span class="right-top"></span>
    </div>
    

</asp:Content>

