﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="SEOReport.aspx.vb" Inherits="Admin_A_SEO_SEOReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <script src='../javascripts/oocharts.js'></script>

        
           <script type="text/javascript">

               window.onload = function () {

                   oo.setAPIKey('<%= apikey %>');

                   oo.load(function () {

                       var table = new oo.Table('<%= profileid %>', '<%= timeline %>');

                       table.addMetric("ga:visits", "Visits");

                       table.addDimension("ga:city", "City");

                       table.draw('visitortable');





                       var pie = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie.setMetric("ga:visits", "Visits");
                       pie.setDimension("ga:browser");

                       pie.draw('visitorpie');




                       var pie2 = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie2.setMetric("ga:organicSearches", "Search");
                       pie2.setDimension("ga:continent");

                       pie2.draw('visitorpie2');




                       var bar = new oo.Bar('<%= profileid %>', '<%= timeline %>');

                       bar.addMetric("ga:visits", "Visits");

                       bar.addMetric("ga:newVisits", "New Visits");

                       bar.setDimension("ga:continent");

                       bar.draw('visitorbar');


                       var timeline = new oo.Timeline('<%= profileid %>', '<%= timeline %>');

                       timeline.addMetric("ga:visits", "Visits");

                       timeline.addMetric("ga:newVisits", "New Visits");

                       timeline.draw('visitortimeline');



                       var Column1 = new oo.Column('<%= profileid %>', '<%= timeline %>');

                       //                       Column1.addMetric("ga:organicSearches", "Visits");

                       //                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.addMetric("ga:visits", "Visits");

                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.setDimension("ga:deviceCategory");

                       Column1.draw('visitorcolumn');



                   });
               };

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
<div class="stats">
    <p class="stat"><span class="number">
    <asp:Literal ID="ltTotlaContact" runat="server"></asp:Literal></span>Contact</p>
    <p class="stat"><span class="number">
        <asp:Literal ID="ltTotalReg" runat="server"></asp:Literal>
    </span>Registration</p>
    <p class="stat"><span class="number">
    <asp:Literal ID="ltTotalNewsLetter" runat="server"></asp:Literal>
    </span>Newsletter</p>
</div>
<h1 class="page-title">SEO Report</h1>

<div class="row-fluid">
    <div class="block">
        <p class="block-heading" data-toggle="collapse" data-target="#chart-container">Visitor Vs Timeline</p>
        <div id="chart-container" class="block-body collapse in">
            <%--<div id="line-chart"></div>--%>
            <%--<div id="chart_ContactAndNewsletter" class="mainchart">
            </div>--%>
              <div id='visitortimeline'></div>  
        </div>
    </div>
</div>

<div class="row-fluid">
     <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorColumn">Visitor Vs Device Category</div>    
        <div id="widgetVisitorColumn" class="block-body collapse in">
         <div id='visitorcolumn'></div> 
         </div>
    </div>
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorBar">Visitor Vs Continent</div>
        <div id="widgetVisitorBar" class="block-body collapse in">
             <div id='visitorbar'></div>
        </div>
    </div>

</div>

<div class="row-fluid">

    <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorPie">Visitor Vs Browser</div>    
        <div id="widgetVisitorPie" class="block-body collapse in">
         <div id='visitorpie'></div>
         </div>
    </div>

        <div class="block span6">
         <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorPie2">Search Vs Continent</div>    
        <div id="widgetVisitorPie2" class="block-body collapse in">
              <div id='visitorpie2'></div>
         </div>
        </div>

</div>


</asp:Content>
