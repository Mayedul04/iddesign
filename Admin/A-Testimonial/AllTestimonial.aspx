﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllTestimonial.aspx.vb" Inherits="Admin_A_Testimonial_AllTestimonial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">
        Testimonial</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="All Testimonials"></asp:Label></h2>
    <div>
        <div class="well">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsList_Testimonial" 
                DataKeyNames="TestimonialID">
                
                <EmptyDataTemplate>
                    <table runat="server" style="">
                        <tr>
                            <td>No data was returned.</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="TestimonialIDLabel" runat="server" 
                                Text='<%# Eval("TestimonialID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="WrittenByLabel" runat="server" Text='<%# Eval("WrittenBy") %>' />
                        </td>
                        <td>
                            <asp:Label ID="DesignationLabel" runat="server" 
                                Text='<%# Eval("Designation") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CountryLabel" runat="server" Text='<%# Eval("Country") %>' />
                        </td>
                        
                        
                        <td>
                             <img  src='<%# "../"& Eval("SmallImage") %>' alt="Edit" width="100px" />
                        </td>
                        
                        <td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" 
                                Checked='<%# Eval("Featured") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" 
                                Checked='<%# Eval("Status") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" 
                                Text='<%# Eval("LastUpdated","{0:d}" ) %>' />
                        </td>
                       <%-- <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>--%>
                        <td>
                            <a href='<%#  "Testimonial.aspx?tid=" & Eval("TestimonialID") %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <%--<a href='<%# "Testimonial.aspx?tid=" & Eval("TestimonialID") & "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp --%>
                            <a href='<%# "#" & Eval("TestimonialID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("TestimonialID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                                <table  border="0" style="" class="table">
                                    <thead>
                                    <tr runat="server" style="">
                                        <th runat="server">
                                            Testimonial ID</th>                                        
                                        <th runat="server">
                                            Written By</th>
                                        <th runat="server">
                                            Designation</th>
                                        <th runat="server">
                                            Address</th>                                        
                                        <th id="Th6" runat="server">
                                            Small Image</th>
                                        <th id="Th1" runat="server">
                                            Featured</th>
                                        <th id="Th2" runat="server">
                                            Sort Order</th>
                                        <th id="Th3" runat="server">
                                            Status</th>
                                        <th id="Th4" runat="server">
                                            Last Updated</th>
                                        <%--<th>Mastar ID</th>
                                        <th>Lang</th>--%>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    
                                    <tr ID="itemPlaceholder" runat="server">
                                    </tr>
                        
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                        
                    </table>
                </LayoutTemplate>
                
            </asp:ListView>
            <asp:SqlDataSource ID="sdsList_Testimonial" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"                 
                SelectCommand="SELECT TestimonialID, Title, SmallDetails, BigDetails, SmallImage, BigImage, List_Testimonial.Featured, SortIndex, Status, LastUpdated, WrittenBy, Designation, Country,MasterID,lang FROM List_Testimonial" 
                DeleteCommand="DELETE FROM List_Testimonial WHERE (TestimonialID = @TestimonialID)">
                <DeleteParameters>
                    <asp:Parameter Name="TestimonialID" />
                </DeleteParameters>
            </asp:SqlDataSource>
        </div>
    </div>

    


    
<%--    <h1>
        Testimonial</h1>
    <div style="padding-bottom: 32px;">
        <div style="float: left;">
            <asp:ImageButton ID="btnAddNew" runat="server" ImageUrl="~/Admin/assets/images/bttn/addnew.png" ToolTip="Add New" />
        </div>
        <div style="float: right;">
        </div>
    </div>
    <!-- content -->
    <div class="content-wrap">
        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Testimonial"></asp:Label></h2>
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="TestimonialID" DataSourceID="sdsList_Testimonial" EnableModelValidation="True">
                <Columns>
                    
                    <asp:TemplateField HeaderText="Image" SortExpression="SmallImage">
                        <ItemTemplate>
                            <img  src='<%# "../"& Eval("SmallImage") %>' alt="Edit" width="100px" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SmallImage") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="WrittenBy" HeaderText="Written By" 
                        SortExpression="WrittenBy" />
                    <asp:BoundField DataField="Designation" HeaderText="Designation" 
                        SortExpression="Designation" />
                    <asp:BoundField DataField="SmallDetails" HeaderText="Testimonial Text" 
                        SortExpression="SmallDetails" />
                    
                    <asp:BoundField DataField="SortIndex" HeaderText="Sort Order" 
                        SortExpression="SortIndex" />
                    <asp:CheckBoxField DataField="Featured" HeaderText="Featured" 
                        SortExpression="Featured" />
                    <asp:CheckBoxField DataField="Status" HeaderText="Status" 
                        SortExpression="Status" />
                    <asp:BoundField DataField="LastUpdated" DataFormatString="{0:d}" 
                        HeaderText="Last Updated" SortExpression="LastUpdated" />                    
                    <asp:TemplateField>
                        <ItemTemplate>
                            <a href='<%# "Testimonial.aspx?tid=" & Eval("TestimonialID") %>' title="Edit"><img  src="../assets/images/icons/edit.jpg" alt="Edit" /> </a>                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="cmdDelete"  AlternateText="Delete" OnClientClick="return confirm('Are you sure to delete?');" ToolTip="Delete" ImageUrl="~/Admin/assets/images/icons/delete.png" runat="server" CommandName="Delete" />                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="sdsList_Testimonial" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT TestimonialID, Title, SmallDetails, BigDetails, SmallImage, BigImage, List_Testimonial.Featured, SortIndex, Status, LastUpdated, WrittenBy, Designation, Country FROM List_Testimonial" 
                DeleteCommand="DELETE FROM List_Testimonial WHERE (TestimonialID = @TestimonialID)">
                <DeleteParameters>
                    <asp:Parameter Name="TestimonialID" />
                </DeleteParameters>
            </asp:SqlDataSource>
        </div>
        <span class="left-bottom"></span><span class="left-top"></span><span class="right-bottom">
        </span><span class="right-top"></span>
    </div>
    <!-- Eof content -->--%>
</asp:Content>


