﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="TestimonialEdit.aspx.vb" Inherits="Admin_A_Testimonial_Testimonial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="block">
    <div class="block-heading">List1</div>
    <div class="block-body">

    <h1 class="page-title">
        Testimonial</h1>
    <%--<div class="btn-toolbar">
        <a href="../A-Testimonial/AllTestimonial.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>--%>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            
            <p style="display:none;">
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                </label>
            </p>
            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>
                        Trestimonial:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <p>
                <label>Written By:</label>
                <asp:TextBox ID="txtWrittenBy" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtWrittenBy"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Designation:</label>
                <asp:TextBox ID="txtDesignation" runat="server" CssClass="input-xlarge" MaxLength="300"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="form" ControlToValidate="txtDesignation"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Address:</label>
                <asp:TextBox ID="txtCountry" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="form" ControlToValidate="txtCountry"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" runat="server" Visible="false">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            
            <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                <p>
                    <label>
                        Details:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );

                            

                    </script>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlLink" runat="server" Visible="false">
                <p>
                    <label>
                        Link:</label>
                    <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                </p>
            </asp:Panel>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style="display:none;">
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                </asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary" ><i class="icon-save"></i> Add New</button>
            <div class="btn-group">
            </div>
        </div>

        <asp:SqlDataSource ID="sdsTestimonial" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                DeleteCommand="DELETE FROM [List_Testimonial] WHERE [TestimonialID] = @TestimonialID" 
                InsertCommand="INSERT INTO [List_Testimonial] ([Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [WrittenBy], [Designation], [Country], [SortIndex], [Featured], [Status], [LastUpdated], MasterID, Lang  ) VALUES (@Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @WrittenBy, @Designation, @Country, @SortIndex, @Featured, @Status, @LastUpdated, @MasterID, @Lang  )" 
                SelectCommand="SELECT * FROM [List_Testimonial]" 
                
                UpdateCommand="UPDATE [List_Testimonial] SET [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [WrittenBy] = @WrittenBy, [Designation] = @Designation, [Country] = @Country, [SortIndex] = @SortIndex, [Featured] = @Featured, [Status] = @Status, [LastUpdated] = @LastUpdated, MasterID=@MasterID, Lang=@Lang   WHERE [TestimonialID] = @TestimonialID">
                <DeleteParameters>
                    <asp:Parameter Name="TestimonialID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="txtWrittenBy" Name="WrittenBy" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDesignation" Name="Designation" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCountry" Name="Country" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                </InsertParameters>
                <UpdateParameters>
                    
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="txtWrittenBy" Name="WrittenBy" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDesignation" Name="Designation" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCountry" Name="Country" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                    <asp:QueryStringParameter Name="TestimonialID" QueryStringField="tid" 
                        Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
    </div>

</div>
</div>
</asp:Content>


