﻿
Partial Class Admin_MasterPage_Main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Not Utility.isLoggedIn(Request) Then
                Response.Cookies("userName").Value = ""
                Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userFullName").Value = ""
                Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userpass").Value = ""
                Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)

                If Request.Url.AbsolutePath.ToLower().Contains("default.aspx") Then
                    Response.Redirect("A-Login/login.aspx")
                End If
                Response.Redirect("../A-Login/login.aspx")
            End If
            ltUser.Text = Request.Cookies("userFullName").Value
        End If
    End Sub



    Protected Function GetCurrentClass(pageName As String) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.Replace("~/", "").ToLower()
        If virtualPath.Contains(pageName.ToLower()) Then
            Return "active"

        Else
            Return ""
        End If

    End Function
    Protected Function GetCurrentClassByParameter() As String

        If Not Request.QueryString("cfor") Is Nothing Then
            Return "active"

        Else
            Return ""
        End If

    End Function

End Class

