﻿ss<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ContactUs.aspx.vb" Inherits="ContactUs" %>

<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Contact us</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Contact us</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">

        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-5 col-sm-5">

                    <!-- Address Listings -->
                    <ul class="addressListings">

                        <%= Locations() %>
                    </ul>
                    <!-- Address Listings -->
                      
                </div>
             
                <div class="col-md-6 col-md-offset-1 col-sm-7">

                    <div class="contactFormContainer">
                        <h2>CONTACT FORM</h2>

                        <div class="form-horizontal">

                            <!-- Select Menu Input -->
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Subject</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtSub" Text="Contact Us" class="form-control" runat="server"></asp:TextBox>
                                    

                                </div>
                            </div>
                            <!-- Select Menu Input -->

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtName" ValidationGroup="form" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>


                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtEmail1" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmail1" ValidationGroup="form" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail1"
                                        ValidationGroup="form" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtMobile" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtMobile" ValidationGroup="form" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtAddress" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtAddress" ValidationGroup="form" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- Select Menu Input -->
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlCountry" runat="server" class="form-control">
                                        <asp:ListItem Text="Select Country" Value=""></asp:ListItem>
                                        <asp:ListItem Value="Afghanistan">Afghanistan</asp:ListItem>
                                        <asp:ListItem Value="Albania">Albania</asp:ListItem>
                                        <asp:ListItem Value="Algeria">Algeria</asp:ListItem>
                                        <asp:ListItem Value="American Samoa">American Samoa</asp:ListItem>
                                        <asp:ListItem Value="Andorra">Andorra</asp:ListItem>
                                        <asp:ListItem Value="Angola">Angola</asp:ListItem>
                                        <asp:ListItem Value="Anguilla">Anguilla</asp:ListItem>
                                        <asp:ListItem Value="Antartica">Antarctica</asp:ListItem>
                                        <asp:ListItem Value="Antigua Barbuda">Antigua Barbuda</asp:ListItem>
                                        <asp:ListItem Value="Argentina">Argentina</asp:ListItem>
                                        <asp:ListItem Value="Armenia">Armenia</asp:ListItem>
                                        <asp:ListItem Value="Aruba">Aruba</asp:ListItem>
                                        <asp:ListItem Value="Ascension Island">Ascension Island</asp:ListItem>
                                        <asp:ListItem Value="Australia">Australia</asp:ListItem>
                                        <asp:ListItem Value="Austria">Austria</asp:ListItem>
                                        <asp:ListItem Value="Azerbaijan">Azerbaijan</asp:ListItem>
                                        <asp:ListItem Value="Bahamas">Bahamas</asp:ListItem>
                                        <asp:ListItem Value="Bahrain">Bahrain</asp:ListItem>
                                        <asp:ListItem Value="Bangladesh">Bangladesh</asp:ListItem>
                                        <asp:ListItem Value="Barbados">Barbados</asp:ListItem>
                                        <asp:ListItem Value="Belarus">Belarus</asp:ListItem>
                                        <asp:ListItem Value="Belguim">Belgium</asp:ListItem>
                                        <asp:ListItem Value="Belize">Belize</asp:ListItem>
                                        <asp:ListItem Value="Benin">Benin</asp:ListItem>
                                        <asp:ListItem Value="Bermuda">Bermuda</asp:ListItem>
                                        <asp:ListItem Value="Bhutan">Bhutan</asp:ListItem>
                                        <asp:ListItem Value="Bolivia">Bolivia</asp:ListItem>
                                        <asp:ListItem Value="Bosnia Herzegovina">Bosnia Herzegovina</asp:ListItem>
                                        <asp:ListItem Value="Botswana">Botswana</asp:ListItem>
                                        <asp:ListItem Value="Bouvet Island">Bouvet Island</asp:ListItem>
                                        <asp:ListItem Value="Brazil">Brazil</asp:ListItem>
                                        <asp:ListItem Value="British Indian Ocean Territory">British Indian Ocean 
                    Territory</asp:ListItem>
                                        <asp:ListItem Value="Brunei Darussalam">Brunei Darussalam</asp:ListItem>
                                        <asp:ListItem Value="Bulgaria">Bulgaria</asp:ListItem>
                                        <asp:ListItem Value="Burkina Faso">Burkina Faso</asp:ListItem>
                                        <asp:ListItem Value="Burundi">Burundi</asp:ListItem>
                                        <asp:ListItem Value="Cambodia">Cambodia</asp:ListItem>
                                        <asp:ListItem Value="Cameroon">Cameroon</asp:ListItem>
                                        <asp:ListItem Value="Canada">Canada</asp:ListItem>
                                        <asp:ListItem Value="Cap Verde">Cap Verde</asp:ListItem>
                                        <asp:ListItem Value="Cayman Islands">Cayman Islands</asp:ListItem>
                                        <asp:ListItem Value="Central African Republic">Central African Republic</asp:ListItem>
                                        <asp:ListItem Value="Chad">Chad</asp:ListItem>
                                        <asp:ListItem Value="Chile">Chile</asp:ListItem>
                                        <asp:ListItem Value="China">China</asp:ListItem>
                                        <asp:ListItem Value="Christmas Island">Christmas Island</asp:ListItem>
                                        <asp:ListItem Value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</asp:ListItem>
                                        <asp:ListItem Value="Colombia">Colombia</asp:ListItem>
                                        <asp:ListItem Value="Comoros">Comoros</asp:ListItem>
                                        <asp:ListItem Value="Congo, Democratic People's Republic">Congo, Democratic 
                    People's Republic</asp:ListItem>
                                        <asp:ListItem Value="Congo, Republic of">Congo, Republic of</asp:ListItem>
                                        <asp:ListItem Value="Cook Islands">Cook Islands</asp:ListItem>
                                        <asp:ListItem Value="Costa Rica">Costa Rica</asp:ListItem>
                                        <asp:ListItem Value="Cote d'Ivoire">Cote d'Ivoire</asp:ListItem>
                                        <asp:ListItem Value="Crotia/Hrvatska">Croatia/Hrvatska</asp:ListItem>
                                        <asp:ListItem Value="Cuba">Cuba</asp:ListItem>
                                        <asp:ListItem Value="Cyprus">Cyprus</asp:ListItem>
                                        <asp:ListItem Value="Czek Republic">Czech Republic</asp:ListItem>
                                        <asp:ListItem Value="Denmark">Denmark</asp:ListItem>
                                        <asp:ListItem Value="Djibouti">Djibouti</asp:ListItem>
                                        <asp:ListItem Value="Dominica">Dominica</asp:ListItem>
                                        <asp:ListItem Value="Dominican Republic">Dominican Republic</asp:ListItem>
                                        <asp:ListItem Value="East Timor">East Timor</asp:ListItem>
                                        <asp:ListItem Value="Ecuador">Ecuador</asp:ListItem>
                                        <asp:ListItem Value="Egypt">Egypt</asp:ListItem>
                                        <asp:ListItem Value="El Salvador">El Salvador</asp:ListItem>
                                        <asp:ListItem Value="Equatorial Guinea">Equatorial Guinea</asp:ListItem>
                                        <asp:ListItem Value="Eritrea">Eritrea</asp:ListItem>
                                        <asp:ListItem Value="Estonia">Estonia</asp:ListItem>
                                        <asp:ListItem Value="Ethopia">Ethiopia</asp:ListItem>
                                        <asp:ListItem Value="Falkland Island (Malvina)">Falkland Island (Malvina)</asp:ListItem>
                                        <asp:ListItem Value="Faroe Islands">Faroe Islands</asp:ListItem>
                                        <asp:ListItem Value="Fiji">Fiji</asp:ListItem>
                                        <asp:ListItem Value="Finland">Finland</asp:ListItem>
                                        <asp:ListItem Value="France">France</asp:ListItem>
                                        <asp:ListItem Value="French Guiana">French Guiana</asp:ListItem>
                                        <asp:ListItem Value="French Polynesia">French Polynesia</asp:ListItem>
                                        <asp:ListItem Value="French Southern Territories">French Southern Territories</asp:ListItem>
                                        <asp:ListItem Value="Gabon">Gabon</asp:ListItem>
                                        <asp:ListItem Value="Gambia">Gambia</asp:ListItem>
                                        <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                                        <asp:ListItem Value="Germany">Germany</asp:ListItem>
                                        <asp:ListItem Value="Ghana">Ghana</asp:ListItem>
                                        <asp:ListItem Value="Gibraltar">Gibraltar</asp:ListItem>
                                        <asp:ListItem Value="Greece">Greece</asp:ListItem>
                                        <asp:ListItem Value="Greenland">Greenland</asp:ListItem>
                                        <asp:ListItem Value="Grenada">Grenada</asp:ListItem>
                                        <asp:ListItem Value="Guadeloupe">Guadeloupe</asp:ListItem>
                                        <asp:ListItem Value="Guam">Guam</asp:ListItem>
                                        <asp:ListItem Value="Guatemala">Guatemala</asp:ListItem>
                                        <asp:ListItem Value="Guernsey">Guernsey</asp:ListItem>
                                        <asp:ListItem Value="Guinea">Guinea</asp:ListItem>
                                        <asp:ListItem Value="Guinea-Bissau">Guinea-Bissau</asp:ListItem>
                                        <asp:ListItem Value="Guyana">Guyana</asp:ListItem>
                                        <asp:ListItem Value="Haiti">Haiti</asp:ListItem>
                                        <asp:ListItem Value="Heard McDonald Islands">Heard McDonald Islands</asp:ListItem>
                                        <asp:ListItem Value="Holy See (City Vatican State)">Holy See (City Vatican 
                    State)</asp:ListItem>
                                        <asp:ListItem Value="Honduras">Honduras</asp:ListItem>
                                        <asp:ListItem Value="Hong Kong">Hong Kong</asp:ListItem>
                                        <asp:ListItem Value="Hungary">Hungary</asp:ListItem>
                                        <asp:ListItem Value="Iceland">Iceland</asp:ListItem>
                                        <asp:ListItem Value="India">India</asp:ListItem>
                                        <asp:ListItem Value="Indonesia">Indonesia</asp:ListItem>
                                        <asp:ListItem Value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</asp:ListItem>
                                        <asp:ListItem Value="Iraq">Iraq</asp:ListItem>
                                        <asp:ListItem Value="Ireland">Ireland</asp:ListItem>
                                        <asp:ListItem Value="Isle of Man">Isle of Man</asp:ListItem>
                                        <%--<asp:ListItem value="Israel">Israel</asp:ListItem>--%>
                                        <asp:ListItem Value="Italy">Italy</asp:ListItem>
                                        <asp:ListItem Value="Jamaica">Jamaica</asp:ListItem>
                                        <asp:ListItem Value="Japan">Japan</asp:ListItem>
                                        <asp:ListItem Value="Jersey">Jersey</asp:ListItem>
                                        <asp:ListItem Value="Jordan">Jordan</asp:ListItem>
                                        <asp:ListItem Value="Kazakhstan">Kazakhstan</asp:ListItem>
                                        <asp:ListItem Value="Kenya">Kenya</asp:ListItem>
                                        <asp:ListItem Value="Kiribati">Kiribati</asp:ListItem>
                                        <asp:ListItem Value="Korea, Democratic People's Republic">Korea, Democratic 
                    People's Republic</asp:ListItem>
                                        <asp:ListItem Value="Korea, Republic of">Korea, Republic of</asp:ListItem>
                                        <asp:ListItem Value="Kuwait">Kuwait</asp:ListItem>
                                        <asp:ListItem Value="Kyrgyzstan">Kyrgyzstan</asp:ListItem>
                                        <asp:ListItem Value="Lao, People's Democratic Republic">Lao, People's Democratic 
                    Republic</asp:ListItem>
                                        <asp:ListItem Value="Latvia">Latvia</asp:ListItem>
                                        <asp:ListItem Value="Lebanon">Lebanon</asp:ListItem>
                                        <asp:ListItem Value="Lesotho">Lesotho</asp:ListItem>
                                        <asp:ListItem Value="Liberia">Liberia</asp:ListItem>
                                        <asp:ListItem Value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</asp:ListItem>
                                        <asp:ListItem Value="Liechtenstein">Liechtenstein</asp:ListItem>
                                        <asp:ListItem Value="Lithuania">Lithuania</asp:ListItem>
                                        <asp:ListItem Value="Luzembourg">Luxembourg</asp:ListItem>
                                        <asp:ListItem Value="Macau">Macau</asp:ListItem>
                                        <asp:ListItem Value="Macedonia, Former Yugoslav Republic">Macedonia, Former 
                    Yugoslav Republic</asp:ListItem>
                                        <asp:ListItem Value="Madagascar">Madagascar</asp:ListItem>
                                        <asp:ListItem Value="Malawi">Malawi</asp:ListItem>
                                        <asp:ListItem Value="Malaysia">Malaysia</asp:ListItem>
                                        <asp:ListItem Value="Maldives">Maldives</asp:ListItem>
                                        <asp:ListItem Value="Mali">Mali</asp:ListItem>
                                        <asp:ListItem Value="Malta">Malta</asp:ListItem>
                                        <asp:ListItem Value="Marshall Islands">Marshall Islands</asp:ListItem>
                                        <asp:ListItem Value="Marinique">Martinique</asp:ListItem>
                                        <asp:ListItem Value="Mauritania">Mauritania</asp:ListItem>
                                        <asp:ListItem Value="Mauritius">Mauritius</asp:ListItem>
                                        <asp:ListItem Value="Mayotte">Mayotte</asp:ListItem>
                                        <asp:ListItem Value="Mexico">Mexico</asp:ListItem>
                                        <asp:ListItem Value="Micronesia, Federal State of">Micronesia, Federal State of</asp:ListItem>
                                        <asp:ListItem Value="Moldova, Republic of">Moldova, Republic of</asp:ListItem>
                                        <asp:ListItem Value="Monaco">Monaco</asp:ListItem>
                                        <asp:ListItem Value="Mongolia">Mongolia</asp:ListItem>
                                        <asp:ListItem Value="Montserrat">Montserrat</asp:ListItem>
                                        <asp:ListItem Value="Morocco">Morocco</asp:ListItem>
                                        <asp:ListItem Value="Mozambique">Mozambique</asp:ListItem>
                                        <asp:ListItem Value="Myanmar">Myanmar</asp:ListItem>
                                        <asp:ListItem Value="Namibia">Namibia</asp:ListItem>
                                        <asp:ListItem Value="Nauru">Nauru</asp:ListItem>
                                        <asp:ListItem Value="Nepal">Nepal</asp:ListItem>
                                        <asp:ListItem Value="Netherlands">Netherlands</asp:ListItem>
                                        <asp:ListItem Value="Netherlands Antilles">Netherlands Antilles</asp:ListItem>
                                        <asp:ListItem Value="New Zealand">New Zealand</asp:ListItem>
                                        <asp:ListItem Value="Nicaragua">Nicaragua</asp:ListItem>
                                        <asp:ListItem Value="Niger">Niger</asp:ListItem>
                                        <asp:ListItem Value="Nigeria">Nigeria</asp:ListItem>
                                        <asp:ListItem Value="Niue">Niue</asp:ListItem>
                                        <asp:ListItem Value="Norfolk Island">Norfolk Island</asp:ListItem>
                                        <asp:ListItem Value="Northern Mariana Islands">Northern Mariana Islands</asp:ListItem>
                                        <asp:ListItem Value="Norway">Norway</asp:ListItem>
                                        <asp:ListItem Value="Oman">Oman</asp:ListItem>
                                        <asp:ListItem Value="Pakistan">Pakistan</asp:ListItem>
                                        <asp:ListItem Value="Palau">Palau</asp:ListItem>
                                        <asp:ListItem Value="Panama">Panama</asp:ListItem>
                                        <asp:ListItem Value="Papua New Guinea">Papua New Guinea</asp:ListItem>
                                        <asp:ListItem Value="Paraguay">Paraguay</asp:ListItem>
                                        <asp:ListItem Value="Peru">Peru</asp:ListItem>
                                        <asp:ListItem Value="Philippines">Philippines</asp:ListItem>
                                        <asp:ListItem Value="Pitcairn Island">Pitcairn Island</asp:ListItem>
                                        <asp:ListItem Value="Poland">Poland</asp:ListItem>
                                        <asp:ListItem Value="Portugal">Portugal</asp:ListItem>
                                        <asp:ListItem Value="Puerto Rico">Puerto Rico</asp:ListItem>
                                        <asp:ListItem Value="Qatar">Qatar</asp:ListItem>
                                        <asp:ListItem Value="Reunion Island">Reunion Island</asp:ListItem>
                                        <asp:ListItem Value="Romania">Romania</asp:ListItem>
                                        <asp:ListItem Value="Russian Federation">Russian Federation</asp:ListItem>
                                        <asp:ListItem Value="Rwanda">Rwanda</asp:ListItem>
                                        <asp:ListItem Value="Saint Kitts Nevis">Saint Kitts Nevis</asp:ListItem>
                                        <asp:ListItem Value="Saint Lucia">Saint Lucia</asp:ListItem>
                                        <asp:ListItem Value="Saint Vincent &amp; the Grenadines">Saint Vincent &amp; the 
                    Grenadines</asp:ListItem>
                                        <asp:ListItem Value="San Marino">San Marino</asp:ListItem>
                                        <asp:ListItem Value="Sao Tome Principe">Sao Tome Principe</asp:ListItem>
                                        <asp:ListItem Value="Saudi Arabia">Saudi Arabia</asp:ListItem>
                                        <asp:ListItem Value="Senegal">Senegal</asp:ListItem>
                                        <asp:ListItem Value="Seychelles">Seychelles</asp:ListItem>
                                        <asp:ListItem Value="Sierra Leone">Sierra Leone</asp:ListItem>
                                        <asp:ListItem Value="Singapore">Singapore</asp:ListItem>
                                        <asp:ListItem Value="Slovak Republic">Slovak Republic</asp:ListItem>
                                        <asp:ListItem Value="Slivenia">Slovenia</asp:ListItem>
                                        <asp:ListItem Value="Solomon Islands">Solomon Islands</asp:ListItem>
                                        <asp:ListItem Value="Somalia">Somalia</asp:ListItem>
                                        <asp:ListItem Value="South Africa">South Africa</asp:ListItem>
                                        <asp:ListItem Value="South Georgia &amp; the South Sandwich Islands">South 
                    Georgia &amp; the South Sandwich Islands</asp:ListItem>
                                        <asp:ListItem Value="Spain">Spain</asp:ListItem>
                                        <asp:ListItem Value="Sri Lanka">Sri Lanka</asp:ListItem>
                                        <asp:ListItem Value="St. Helena">St. Helena</asp:ListItem>
                                        <asp:ListItem Value="St. Pierre Miquelon">St. Pierre Miquelon</asp:ListItem>
                                        <asp:ListItem Value="Sudan">Sudan</asp:ListItem>
                                        <asp:ListItem Value="Suriname">Suriname</asp:ListItem>
                                        <asp:ListItem Value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen 
                    Islands</asp:ListItem>
                                        <asp:ListItem Value="Swaziland">Swaziland</asp:ListItem>
                                        <asp:ListItem Value="Sweden">Sweden</asp:ListItem>
                                        <asp:ListItem Value="Switzerland">Switzerland</asp:ListItem>
                                        <asp:ListItem Value="Syrian Arab Republic">Syrian Arab Republic</asp:ListItem>
                                        <asp:ListItem Value="Taiwan">Taiwan</asp:ListItem>
                                        <asp:ListItem Value="Tajikistan">Tajikistan</asp:ListItem>
                                        <asp:ListItem Value="Tanzania">Tanzania</asp:ListItem>
                                        <asp:ListItem Value="Thailand">Thailand</asp:ListItem>
                                        <asp:ListItem Value="Togo">Togo</asp:ListItem>
                                        <asp:ListItem Value="Tokelau">Tokelau</asp:ListItem>
                                        <asp:ListItem Value="Tonga">Tonga</asp:ListItem>
                                        <asp:ListItem Value="Trinidad Tobago">Trinidad Tobago</asp:ListItem>
                                        <asp:ListItem Value="Tunisia">Tunisia</asp:ListItem>
                                        <asp:ListItem Value="Turkey">Turkey</asp:ListItem>
                                        <asp:ListItem Value="Turkmenistan">Turkmenistan</asp:ListItem>
                                        <asp:ListItem Value="Turks Ciacos Islands">Turks Ciacos Islands</asp:ListItem>
                                        <asp:ListItem Value="Tuvalu">Tuvalu</asp:ListItem>
                                        <asp:ListItem Value="Uganda">Uganda</asp:ListItem>
                                        <asp:ListItem Value="Ukraine">Ukraine</asp:ListItem>
                                        <asp:ListItem Value="UAE">UAE</asp:ListItem>
                                        <asp:ListItem Value="UK">UK</asp:ListItem>
                                        <asp:ListItem Value="USA">USA</asp:ListItem>
                                        <asp:ListItem Value="Uruguay">Uruguay</asp:ListItem>
                                        <asp:ListItem Value="US Minor Outlying Islands">US Minor Outlying Islands</asp:ListItem>
                                        <asp:ListItem Value="Uzbekistan">Uzbekistan</asp:ListItem>
                                        <asp:ListItem Value="Vanuatu">Vanuatu</asp:ListItem>
                                        <asp:ListItem Value="Venezuela">Venezuela</asp:ListItem>
                                        <asp:ListItem Value="Vietnam">Vietnam</asp:ListItem>
                                        <asp:ListItem Value="Virgin Islands (British)">Virgin Islands (British)</asp:ListItem>
                                        <asp:ListItem Value="Virgin Islands (USA)">Virgin Islands (USA)</asp:ListItem>
                                        <asp:ListItem Value="Virgin Islands (USA)">Wallis Futuna Islands</asp:ListItem>
                                        <asp:ListItem Value="Western Sahara">Western Sahara</asp:ListItem>
                                        <asp:ListItem Value="Western Samoa">Western Samoa</asp:ListItem>
                                        <asp:ListItem Value="Yemen">Yemen</asp:ListItem>
                                        <asp:ListItem Value="Yugoslavia">Yugoslavia</asp:ListItem>
                                        <asp:ListItem Value="Zaire">Zaire</asp:ListItem>
                                        <asp:ListItem Value="Zambia">Zambia</asp:ListItem>
                                        <asp:ListItem Value="Zimbabwe">Zimbabwe</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="ddlCountry"
                                        ValidationGroup="form" runat="server" ErrorMessage="required"></asp:RequiredFieldValidator>

                                </div>
                            </div>
                            <!-- Select Menu Input -->

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Message</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtmsg" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <uc1:MyCaptcha runat="server" ID="MyCaptcha" /> 
                                 <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-5 col-md-offset-2">
                                <asp:Button ID="btnEnquery" runat="server" class="sendEnquiryBttn" ValidationGroup="form" Text="Send Enquiry" />
                                <asp:HiddenField ID="hdnDate" runat="server" />
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>
        <asp:SqlDataSource ID="sdsContact" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
             InsertCommand="INSERT INTO [Contact] ([FullName], [Email], [Mobile], [Address], [Country], [Subject], [Message], [ContactDate],[ContactFor]) VALUES (@FullName, @Email, @Mobile, @Address, @Country, @Subject, @Message, @ContactDate,@ContactFor)">
            
            <InsertParameters>
                <asp:ControlParameter ControlID="txtName" Name="FullName" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtEmail1" Name="Email" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtAddress" Name="Address" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="ddlCountry" Name="Country" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="txtSub" Name="Subject" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtmsg" Name="Message" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnDate" Name="ContactDate" PropertyName="Value" Type="DateTime" />
                <asp:Parameter DefaultValue="Contact" Name="ContactFor" />
            </InsertParameters>
           
        </asp:SqlDataSource>
        <!-- Map Container -->
        <section class="mapContainer">
            <div id="map" style="width: 1140px; height: 500px;"></div>
            <script type="text/javascript">
                var locations = [
                    ['<h1>IDdesign</h1><h3>Mall Of The Emirates</h3><p>Level 1 - Next to Parking Lots G-L <br/>Above Carrefour.</p>', 25.118107, 55.200608, 4],
                    ['<h1>IDdesign</h1><h3>Mirdiff City Center</h3><p>Level 1-C033-34-35 - Dubai </p>', 25.216319, 55.407795, 5],
                    ['<h1>IDdesign</h1><h3>Abu Dhabi Store</h3><p>Al Wahda Mall, Level 3<br/>Abu Dhabi</p>', 24.470431, 54.372839, 3],
                    ['<h1>IDdesign</h1><h3>Deira Store</h3><p>On Dubai Sharjah Service Road <br/>Galadari Signal</p>', 25.271854, 55.344851, 2]
                ];

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 9,
                    center: new google.maps.LatLng(24.8900, 55.200608),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
    </script>
           <%-- <iframe src="<%= map %>" width="100%" height="450" frameborder="0" style="border: 0"></iframe>--%>
           
        </section>
        <!-- Map Container -->
         <%= Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=11&SmallImage=0&BigImage=0&BigDetails=0&ImageAltText=0")%>
         <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="14" />
    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

