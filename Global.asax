﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        RegisterRoute(Routing.RouteTable.Routes)
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Sub RegisterRoute(ByVal routes As Routing.RouteCollection)
       
        routes.MapPageRoute("Default-Home", "", "~/Default.aspx")
        routes.MapPageRoute("ContactUs", "ContactUs", "~/ContactUs.aspx")
        routes.MapPageRoute("AboutUs", "AboutUs", "~/AboutUs.aspx")
        routes.MapPageRoute("StyleGroups", "StyleGroups", "~/StyleGroups.aspx")
        routes.MapPageRoute("StyleGroups1", "StyleGroup/{id}/{title}", "~/StyleGroup.aspx")
         routes.MapPageRoute("StyleGroups2", "StyleGroup/{id}/{title}/{page}", "~/StyleGroup.aspx")
        routes.MapPageRoute("Collections", "Collections", "~/Collections.aspx")
        routes.MapPageRoute("Collections2", "Collections/{page}", "~/Collections.aspx")
        routes.MapPageRoute("Collections1", "Collection/{id}/{title}", "~/Collection.aspx")
        routes.MapPageRoute("Collections3", "Collection/{id}/{title}/{page}", "~/Collection.aspx")
        routes.MapPageRoute("Product1", "Product/{id}/{title}", "~/Product.aspx")
        routes.MapPageRoute("Promo1", "Promotions", "~/Promotions.aspx")
        routes.MapPageRoute("Promo2", "Promotions/{page}", "~/Promotions.aspx")
        routes.MapPageRoute("Promo3", "Promotion/{id}/{title}", "~/Promotion.aspx")
        routes.MapPageRoute("Default-Product", "Product", "~/Nexa-Blog.aspx")
        routes.MapPageRoute("Default-ProductDetails", "Product-Details/{id}/{t}", "~/Nexa-Blog-Details.aspx")
        routes.MapPageRoute("Thanks", "Thanks", "~/Thanks.aspx")
        routes.MapPageRoute("Thanks1", "Thanks/{st}", "~/Thanks.aspx")
        routes.MapPageRoute("Services", "Services", "~/Services.aspx")
        routes.MapPageRoute("Service1", "Service/{id}/{title}", "~/Service.aspx")
        routes.MapPageRoute("Projects", "Projects/{id}/{title}", "~/Projects.aspx")
        routes.MapPageRoute("Project1", "Project/{id}/{title}", "~/Project.aspx")
        routes.MapPageRoute("MediaCentre", "MediaCentre", "~/MediaCenter.aspx")
        routes.MapPageRoute("PhotoGallery", "PhotoGallery", "~/PhotoGallery.aspx")
        routes.MapPageRoute("PhotoGallery1", "PhotoGallery/{page}", "~/PhotoGallery.aspx")
        routes.MapPageRoute("Gallery", "Gallery/{id}/{title}", "~/Gallery.aspx")
        routes.MapPageRoute("Gallery1", "Gallery/{id}/{title}/{page}", "~/Gallery.aspx")
        routes.MapPageRoute("VideoGallery", "VideoGallery", "~/VideoGallery.aspx")
        routes.MapPageRoute("VideoGallery2", "VideoPop/{id}/{title}", "~/VideoPop.aspx")
        routes.MapPageRoute("VideoGallery1", "VideoGallery/{page}", "~/VideoGallery.aspx")
        routes.MapPageRoute("News", "News", "~/News.aspx")
        routes.MapPageRoute("News1", "News/{page}", "~/News.aspx")
        routes.MapPageRoute("News2", "NewsDetails/{id}/{title}", "~/NewsDetails.aspx")
        routes.MapPageRoute("Testimonials", "Testimonials", "~/Testimonials.aspx")
        routes.MapPageRoute("Testimonials1", "Testimonials/{page}", "~/Testimonials.aspx")
        routes.MapPageRoute("Enquiry", "Enquiry/{category}/{id}", "~/Enquiry.aspx")
        routes.MapPageRoute("New", "NewArrivals", "~/NewArrivals.aspx")
        routes.MapPageRoute("New1", "NewArrivals/{page}", "~/NewArrivals.aspx")
        routes.MapPageRoute("Serach", "Search/{q}", "~/Search.aspx")
        routes.MapPageRoute("Error", "Error", "~/Error500.aspx")
    End Sub
       
    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
      
        Dim domainName As String
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Threading.Thread.CurrentThread.CurrentCulture = newCulture
         
        
    End Sub
    
    
    
</script>