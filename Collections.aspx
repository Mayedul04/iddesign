﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Collections.aspx.vb" Inherits="Collections" %>

<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%=fbUrl%>

  <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName%>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Collections</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Collections</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">

        <section class="innerMainContentWrapper">

            <div class="row">

                <!-- IMage Block Listings -->
               
                    <%= Collections() %>                   
              
                <!-- IMage Block Listings -->

            </div>
 
            <!-- Social and Pagination -->
            <div class="row marginTop30">
               
                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                    <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="33"/>
                  <%-- <%= socialShare() %>--%>
                    <!-- Social Icons -->
                </div>

                

                    <!-- Pagination -->
                    <%= PageList%>
                    <!-- Pagination -->

               

            </div>
            <!-- Social and Pagination -->

        </section>



    </section>
</asp:Content>

