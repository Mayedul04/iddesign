﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Error500.aspx.vb" Inherits="Error500" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                 <li><a href='http://iddesignuae.com/'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">Thank You</a></li>
            </ul>

        </div>
    </section>
     <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>404 Error</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Thank You Container -->
    <section class="errorContainer">

        <section class="container">
            
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="imgHold"><img src="/ui/media/dist/img/404-error-img.jpg" alt=""></div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h2>We're Sorry</h2>

                    <p>the page you asked for couldn’t be found please try later</p>

                    <a class="goBackToHome" href="http://iddesignuae.com/">GO BACK TO HOME PAGE</a>

                </div>
            </div>

        </section>

    </section>
    <!-- Thank You Container -->
</asp:Content>

