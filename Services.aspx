﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Services.aspx.vb" Inherits="Services" %>

<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">Services</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Services</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <div class="imgHold">
         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage"  HTMLID="12"/>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    
           <p> <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="12" /></p>
               <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="15" />
                    <h2>Our Services</h2>

                    <!-- Service Listings -->
                    <ul class="serviceListings">
                       <%= GetServices() %>
                    </ul>
                    <!-- Service Listings -->

                </div>

            </div>

        </section>

        <!-- Social Icons -->
    <%--   <asp:Literal ID="lblSocial" runat="server"></asp:Literal>--%>
        <!-- Social Icons -->

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

