﻿Imports System.Data.SqlClient

Partial Class Projects
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List_Service where ServiceID=@ServiceID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ServiceID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString()
            ImgProject.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()
            '  lblSocial.Text = socialShare(reader("Title").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), reader("SmallDetails").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), If(reader("SmallImage").ToString = "" Or IsDBNull(reader("SmallImage").ToString), "Content/Noimages.jpg", reader("SmallImage").ToString))
            lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Service/ServiceEdit.aspx?" & reader("ServiceID"))
            DynamicSEO.PageType = "Service"
            DynamicSEO.PageID = reader("ServiceID")
        End If
        cn.Close()
    End Sub
    Public Function GetProjects() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Project where List_Project.Status=1 and ServiceID=@ServiceID  order by List_Project.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ServiceID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Project/" & reader("ProjectID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function socialShare(ByVal Sharetitle As String, ByVal ShareDesc As String, ByVal ShareImage As String) As String
        Dim M As String = ""

        Dim title As String = Utility.EncodeTitle(Sharetitle, "-")
        Dim imgUrl As String = domainName & "Admin/" & ShareImage
        Dim desc As String = ShareDesc
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk
        Dim linkinurl As String = "http://www.linkedin.com/shareArticle?mini=true&url=" & lnk & "& title=" & title & "&summary=" & desc
        Dim fblikecode As String = "http://www.facebook.com/plugins/like.php?href=" & lnk & "&width&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=1567479410148486"



        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIcons"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet-iner.png""> </a>"

        M += "  </li>"


        'M += "<li>"
        'M += " <a href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & linkinurl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')"" > "
        'M += "<img src=""" & domainName & "ui/media/dist/icons/linkdib-share.png"" alt="""">"
        'M += " </a>"

        'M += " </li>"

        M += "<li><div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button_count"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div></li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/gplus-count.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function
End Class
