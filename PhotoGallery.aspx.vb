﻿Imports System.Data.SqlClient

Partial Class PhotoGallery
    Inherits System.Web.UI.Page
    Public domainName As String
    Public PageList As String = ""
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from Gallery where Status=1 "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        totalRows = Math.Ceiling(cmd.ExecuteScalar / 12)
        cn.Close()
        Return totalRows
    End Function
    Public Function Gallery() As String

        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "PhotoGallery/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 3, searchCriteria)
        End If
        Dim NW As String = "<ul class=""imageBlockListings gallery"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 16; Select * from ( select  ROW_NUMBER()   over(ORDER BY Gallery.GalleryID DESC) AS RowNum , GalleryID , CategoryID, Title,SmallImage, ImageAltText from Gallery where Status=1 and CategoryID=4 "
        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)

        Dim reader As SqlDataReader = cmd.ExecuteReader()

        While reader.Read
            NW = NW & "<li class=""col-md-3 col-sm-3""><div class=""content""><div class=""imgHold""><a href=""" & domainName & "Gallery/" & reader("GalleryID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & _
                 "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ width=""263"" & _"
            If IsDBNull(reader("ImageAltText")) = False Then
                NW += "alt=""" & reader("ImageAltText").ToString() & """ /></a></div><h2>"
            Else
                NW += "alt=""Gallery"" /></a></div><h2>"
            End If
            NW += "<a  href=""" & domainName & "Gallery/" & reader("GalleryID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & " <span><img src=""" & domainName & "ui/media/dist/icons/title-arrow.png"" alt=""Arrow""></span></a></h2>" & _
                  "</div>" & Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</li>"
        End While
        cn.Close()
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Function socialShare() As String
        Dim M As String = ""

        Dim title As String = "Photo Gallery"
        Dim imgUrl As String = ""
        Dim desc As String = ""
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk
        Dim linkinurl As String = "http://www.linkedin.com/shareArticle?mini=true&url=" & lnk & "& title=" & title
        Dim fblikecode As String = "http://www.facebook.com/plugins/like.php?href=" & lnk & "&width&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=1567479410148486"



        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIcons"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet-iner.png""> </a>"

        M += "  </li>"


        'M += "<li>"
        'M += " <a href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & linkinurl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')"" > "
        'M += "<img src=""" & domainName & "ui/media/dist/icons/linkdib-share.png"" alt="""">"
        'M += " </a>"

        'M += " </li>"

        M += "<li><div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button_count"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div></li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/gplus-count.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination right"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
