﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Thanks.aspx.vb" Inherits="Thanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Thank You</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Thank you</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Thank You Container -->
    <section class="thankYouContainer">

        <section class="container">
            <h2>Thank You</h2>

            <p>
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
        </section>

    </section>
</asp:Content>

