﻿Imports System.Data.SqlClient

Partial Class ContactUs
    Inherits System.Web.UI.Page
    Public domainName, map As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function Locations() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Locations where Status=1  order by List_Locations.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><h6>" & reader("Title").ToString() & "</h6><p>" & reader("Address1").ToString() & " <br/> " & reader("Address2") & "<br/>"
            retstr += "Tel: " & reader("Phone").ToString() & " <br/>"
            If IsDBNull(reader("Fax")) = False Then
                retstr += "Fax: " & reader("Fax").ToString() & " <br/>"
            End If
            If IsDBNull(reader("Email")) = False Then
                retstr += "Email: " & reader("Email").ToString() & " <br/>"
            End If
            retstr += "</p>"
            retstr += Utility.showEditButton(Request, domainName & "Admin/A-Location/LocationEdit.aspx?locid=" & reader("LocationID").ToString()) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function IsExist(ByVal tablename As String, ByVal emailid As String) As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM " & tablename & " where Email=@Email"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 100).Value = emailid
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return True
        Else
            conn.Close()
            Return False
        End If
    End Function
    Protected Sub btnEnquery_Click(sender As Object, e As EventArgs) Handles btnEnquery.Click
      
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now()
            Dim msg As String = ""
            '  Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            'msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">First Name</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail1.Text & "</td></tr>"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Mobile</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtMobile.Text & "</td></tr>"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Address</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtAddress.Text & "</td></tr>"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Country</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & ddlCountry.SelectedValue & "</td></tr>"
            'msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Message</td>"
            'msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtmsg.Text & "</td></tr>"
            'msg += "</table></td></tr></table>"
            msg = GetRegistrationInfoMessage()
            If sdsContact.Insert() > 0 Then
                '  zainumry@ gmail.com

                Utility.SendHTTPRequestToSendgrid(txtName.Text, txtEmail1.Text, "headoffice@iddesign.ae;headoffice@iddesignuae.com", "zainu@iddesign.ae", "mayedul@digitalnexa.com", "Contact Us Request", msg)
                Response.Redirect(domainName & "Thanks")
                
            End If
        End If
    End Sub
    Private Function GetRegistrationInfoMessage() As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
"<html lang='en'>" & _
"<head>" & _
"    <meta charset='utf-8'>" & _
"    <title>Royal Rose</title>" & _
"    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" & _
"    <body style='margin:0px; padding:0px;'>" & _
"        <table bgcolor='#ac7e14' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>" & _
"            <tr>" & _
"                <td style='padding:2px 0;'>" & _
"                    <table bgcolor='#fff' width='610' cellspacing='0' cellpadding='0' border='0' align='center' style='background:#ffffff;'>" & _
"                        <tr>" & _
"                            <td align='center'>" & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>&nbsp;</p>" & _
"                                <table width='90%' cellspacing='0' cellpadding='0' align='center' border='1' style='border:1px solid #eee; border-collapse: collapse; margin:0 0 20px;'>" & _
"                                    <tr bgcolor='#eee'>" & _
"                                        <td colspan='2'>" & _
"                          <div style=""text-align: center;""><a href=""http://" & Request.Url.Host & """><img src=""http://" & Request.Url.Host & "/ui/media/dist/logo/logo.png"" width=""62px""  alt=''></a></div>" & _
"                                            <h3 style='font-family:arial; color:#490109; font-size:24px; text-align:center; margin:10px 0;'>Contact Details</h3>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                   <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtName.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                   <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Mobile</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtMobile.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Email</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtEmail1.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Address</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtAddress.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Country</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & ddlCountry.SelectedValue & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                                             <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Message</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtmsg.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                </table>                                " & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>© Copyright " & Date.Now.Year & ". All rights reserved by ~<a href='http://" & Request.Url.Host & "'>IDdesign UAE</a>~.</p>" & _
"                            </td>" & _
"                        </tr>  " & _
"                    </table>" & _
"                </td>" & _
"            </tr>" & _
"        </table>" & _
"    </body>" & _
"</html>"



        Return retVal
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim sConn As String
            Dim selectString1 As String = "Select * from HTML where HTMLID=@HTMLID "
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("HTMLID", Data.SqlDbType.Int, 32).Value = 11
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.Read Then
                map = reader("SmallDetails").ToString()
            End If
            cn.Close()
        End If
    End Sub
End Class
