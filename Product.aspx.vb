﻿Imports System.Data.SqlClient

Partial Class Product
    Inherits System.Web.UI.Page

    Public domainName, title, breadcrumbpre As String
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List1_Child where ChildID=@ChildID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-List1/List1ChildEdit.aspx?l1cid=" & reader("ChildID"))
            hdnCollectionID.Value = reader("ListID").ToString()
            hdnMasterID.Value = reader("MasterID").ToString()
            breadcrumbpre = GetCollection(reader("ListID").ToString())
            DynamicSEO.PageID = listid
            '  lblPCode.Text = reader("ProductID").ToString()
            '   lblprice.Text = reader("Price").ToString()
            lblSocial.Text = socialShare(reader("Title").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), reader("SmallDetails").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), If(reader("SmallImage").ToString = "" Or IsDBNull(reader("SmallImage").ToString), "Content/Noimages.jpg", reader("SmallImage").ToString))
            If IsDBNull(reader("BigDetails")) = False Then
                lblDetails.Text = reader("BigDetails").ToString()
            End If
            '   lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-List1/List1ChildEdit.aspx?l1cid=" & reader("ChildID"))
        End If
        cn.Close()
    End Sub
    Public Function GetCollection(ByVal id As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from List1 where ListID=@ListID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += id & "/" & reader("Title").ToString()
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function Gallery() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from CommonGallery where TableMasterID=@TableMasterID and TableName=@TableName"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int, 32).Value = hdnMasterID.Value
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 100).Value = "List1_Child"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt=""""></div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function OtherProducts() As String
        Dim sConn As String
        Dim retstr As String = ""
        Dim selectString1 As String = "Select top 4 *,List1_Child.Title as ProductTitle  from dbo.CommonGallery INNER JOIN   dbo.List1_Child ON dbo.CommonGallery.TableMasterID = dbo.List1_Child.MasterID where List1_Child.ListID=@ListID and List1_Child.ChildID<>@ChildID  and List1_Child.Status=1 order by NEWID()"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = hdnCollectionID.Value
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li class=""col-md-3 col-sm-3""><div class=""imgHold"">"
            retstr += "<a class=""viewDetail"" href=""" & domainName & "Product/" & reader("ChildID").ToString() & "/" & Utility.EncodeTitle(reader("Title"), "-") & """><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""""></div>"
            retstr += "<h2>" & reader("ProductTitle").ToString() & "</h2></a>"
            retstr += "</li>"
        End While
        cn.Close()
        Return retstr
    End Function
    Public Function socialShare(ByVal Sharetitle As String, ByVal ShareDesc As String, ByVal ShareImage As String) As String
        Dim M As String = ""

        Dim title As String = Utility.EncodeTitle(Sharetitle, "-")
        Dim imgUrl As String = domainName & "Admin/" & ShareImage
        Dim desc As String = ShareDesc
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk
        Dim linkinurl As String = "http://www.linkedin.com/shareArticle?mini=true&url=" & lnk & "& title=" & title & "&summary=" & desc
        Dim fblikecode As String = "http://www.facebook.com/plugins/like.php?href=" & lnk & "&width&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=1567479410148486"



        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIcons"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet-iner.png""> </a>"

        M += "  </li>"


        'M += "<li>"
        'M += " <a href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & linkinurl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')"" > "
        'M += "<img src=""" & domainName & "ui/media/dist/icons/linkdib-share.png"" alt="""">"
        'M += " </a>"

        'M += " </li>"

        M += "<li><div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button_count"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div></li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/gplus-count.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function
End Class
