﻿Imports System.Data.SqlClient

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetCollections() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  * FROM List1 where List1.Status=1  order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Collection/" & reader("ListID") & "/" & reader("Title") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetFooterCollections() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 4. * FROM List1 where List1.Status=1  order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Collection/" & reader("ListID") & "/" & reader("Title") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetFooterStyleGroups() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 4. * FROM List where List.Status=1  order by List.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "StyleGroup/" & reader("ListID") & "/" & reader("Title").ToString() & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetFooterPromotions() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5. * FROM SpecialOffer where SpecialOffer.Status=1  order by SpecialOffer.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Promotion/" & reader("SpecialOfferID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetProjectDivisions() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5. * FROM List_Project where List_Project.Status=1 and ServiceID=14  order by List_Project.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Project/" & reader("ProjectID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetServices() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5. * FROM List_Service where List_Service.Status=1   order by List_Service.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li><a href=""" & domainName & "Service/" & reader("ServiceID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title").ToString() & "</a></li>"
        End While
        conn.Close()
        Return retstr
    End Function


    Public Function Locations(ByVal city As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Locations where Status=1 and City=@City order by List_Locations.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("City", Data.SqlDbType.NVarChar, 50).Value = city
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            retstr += "<li class=""col-md-4 col-sm-4""><div class=""content""><h2>" & reader("Title").ToString() & "</h2><p>" & reader("Address1").ToString() & " , " & reader("Address2") & "</p>"
            retstr += "<p class=""norMarginB""> Tel:   " & reader("Phone").ToString() & "&nbsp;&nbsp; "
            '|<a class=""getDirection"" target=""_blank"" href=""" & reader("MapCode").ToString() & """>Get Directions "
            'retstr += "<span><img src=""" & domainName & "ui/media/dist/icons/readmore-arrow.png" & """ alt=""""></span></a>"

            retstr += "</p>"
            retstr += " <a class=""getDirection map fancybox.iframe"" href=""" & domainName & "Map/" & reader("LocationID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ >Get Directions </a>"
            retstr += "</div>"
            retstr += Utility.showEditButton(Request, domainName & "Admin/A-Location/LocationEdit.aspx?locid=" & reader("LocationID").ToString()) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getNewsNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from List_News where Status=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        'cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 100).Value = "General News"

        totalRows = cmd.ExecuteScalar
        cn.Close()
        Return totalRows
    End Function
    Public Function getTestimonialNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from List_Testimonial where Status=1 "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        totalRows = cmd.ExecuteScalar
        cn.Close()
        Return totalRows
    End Function

    Public Function GetInstagram() As String
        Dim M As String = String.Empty
        Dim retstr As String = ""
        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT InstagramImage.* FROM InstagramImage where InstagramImage.Status=1 order by ImageID desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try



            While reader.Read()
                i = i + 1
                M += "<li>"
                M += "<a class=""fancybox"" href=""" & domainName & "Admin/" & reader("SmallImage") & """  rel=""instagallery"" >"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                M += "</a>"
                M += "</li>"

                If i Mod 6 = 0 Then
                    If i = 6 Then
                        retstr += "<div class=""item active"">"
                    Else
                        retstr += "<div class=""item"">"
                    End If
                    retstr += "<ul class=""instagramList"">" & M & "</ul></div>"
                    M = ""
                End If

            End While


            conn.Close()
            If M <> "" Then
                retstr += "<div class=""item""><ul class=""instagramList"">" & M & "</ul></div>"
            End If
            Return retstr
        Catch ex As Exception
            Return retstr
        End Try



    End Function
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Response.Redirect(domainName & "Search/" & txtSearch.Text.Trim)
    End Sub
End Class


