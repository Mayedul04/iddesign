﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Service.aspx.vb" Inherits="Service" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href='<%= domainName & "Services" %>'>Services</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;"><%= title %></a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2><%= title %></h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <h2>OUR SERVICES</h2>

            <div class="row">

                <div class="col-md-3 col-sm-4">
                    
                    <!-- Left Panel Navigation -->
                    <ul class="leftPanelNavigationSection">

                       <%= GetServices() %>

                    </ul>
                    <!-- Left Panel Navigation -->

                </div>

                <div class="col-md-9 col-sm-8">
                                     
                          <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
          <uc1:DynamicSEO runat="server" ID="DynamicSEO" PageType="Service" />
                </div>

            </div>

        </section>

        <!-- You May Also Like -->
        <section class="youMayLikContentWrapper">
            
            <h2>Service Gallery</h2>
<asp:HiddenField ID="hdnMasterID" runat="server" />
            
            <!-- New Arrival Slider -->
            <div class="newArrivalSlider youMayLike">

                <ul class="slides">

                  <%= Gallery() %>

                </ul>

            </div>
            <!-- New Arrival Slider -->

        </section>
        <!-- You May Also Like -->


    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

