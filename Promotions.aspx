﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Promotions.aspx.vb" Inherits="Promotions" %>

<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%=fbUrl%>

  <meta property="fb:app_id" content="1567479410148486" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;"> > </a></li>
                <li><a href="javascript:;">Promotions</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Promotions</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">
        
        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-12">

                    <!-- Text Listing Section -->    
                    <ul class="proMotionListings">

                      <%= Promotions() %>

                    </ul>
                    <!-- Text Listing Section -->

                </div>

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">
               
                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                     <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="35" />
                   <%-- <%= socialShare() %>--%>
                    <!-- Social Icons -->
                </div>

                <div class="col-md-6 col-sm-6">
                    
                    <!-- Pagination -->    
                   <%= PageList %>
                    <!-- Pagination -->

                </div>

            </div>
            <!-- Social and Pagination -->

        </section>

        

    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

