﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="StyleGroup.aspx.vb" Inherits="StyleGroup" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="448722205295374" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href='<%= domainName & "StyleGroups" %>'>Style Groups</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;"><%= title %></a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <div class="row">

                <div class="col-md-5 col-sm-5">
                    <h2><%= title %></h2>
                </div>

                <div class="col-md-7 col-sm-7">
                    <div class="row filterSerchDropdown">
                        <div class="col-md-5">
                            <h3>Other Style Groups</h3>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <!-- Normal Input -->
                            <div class="form-group">
                                <asp:DropDownList ID="ddlStyles" class="form-control" AutoPostBack="True" AppendDataBoundItems="True" runat="server" DataSourceID="sdsStyles" DataTextField="Title" DataValueField="ListID">
                                    <asp:ListItem>Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsStyles" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [ListID], [Title] FROM [List] WHERE ([Status] = @Status)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                            <!-- Normal Input -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Inner Banner Section -->



    <!-- Style Group Section -->
    <section class="styleGroupsSectionDetail">

        <section class="container">

            <div class="contentContainer">

                <div class="row">

                    <div class="col-md-4 col-sm-6">

                        <!-- Style Group Section -->
                        <div class="styleGroupTitleSection">

                            <span class="colorBg"></span>

                            <h2><%= title %></h2>
                            <asp:Literal ID="lblDetails" runat="server"></asp:Literal>


                        </div>
                        <!-- Style Group Section -->

                    </div>

                    <!-- Slider Section -->
                    <div class="col-md-8 col-sm-6">
                        <div class="productPreviewSliderContainer">

                            <div class="productPreviewSlider">
                                <ul class="slides">
                                    <asp:Literal ID="lblGallery" runat="server"></asp:Literal>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!-- Slider Section -->

                </div>

            </div>

        </section>
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
    </section>
    <!-- Style Group Section -->

    <!-- Inner Main Wrapper Section -->
    <section class="container">

       <%-- <section class="innerMainContentWrapper">

            <div class="row">

                <!-- Slider Image Text Listing -->
                <%= Products() %>
                <!-- Slider Image Text Listing -->

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">

                <div class="col-md-6 col-sm-6">
                    <!-- Social Icons -->
                     <asp:Literal ID="lblSocial" runat="server"></asp:Literal>
                    <!-- Social Icons -->
                </div>

                <div class="col-md-6 col-sm-6">

                    <!-- Pagination -->
                    <%= PageList %>
                    <!-- Pagination -->

                </div>

            </div>
            <!-- Social and Pagination -->

        </section>--%>



    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

