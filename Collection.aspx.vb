﻿Imports System.Data.SqlClient

Partial Class Collection
    Inherits System.Web.UI.Page
    Public domainName, title, PageList As String
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List1 where ListID=@ListID "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString()
            DynamicSEO.PageID = listid
            ' lblSocial.Text = socialShare(reader("Title").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), reader("SmallDetails").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), If(reader("SmallImage").ToString = "" Or IsDBNull(reader("SmallImage").ToString), "Content/Noimages.jpg", reader("SmallImage").ToString))
        End If
        cn.Close()
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String

        Dim selectString1 As String = ""
        '"Select COUNT(0) from List1_Child where ListID=@ListID and Status=1"
        'dbo.CommonGallery INNER JOIN   dbo.List1_Child ON dbo.CommonGallery.TableMasterID = dbo.List1_Child.MasterID
        selectString1 = "Select COUNT(ChildID) from List1_Child where List1_Child.ListID=@ListID and List1_Child.Status=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        totalRows = Math.Ceiling(cmd.ExecuteScalar / 4)
        cn.Close()
        Return totalRows
    End Function
    Public Function Products() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "Collection/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 3, searchCriteria)
        End If
        Dim NW As String = "<ul class=""sliderImageTextListing"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = ""
        'dbo.CommonGallery INNER JOIN   dbo.List1_Child ON dbo.CommonGallery.TableMasterID = dbo.List1_Child.MasterID
        selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 4; Select * from ( select  ROW_NUMBER()   over(ORDER BY List1_Child.ChildID DESC) AS RowNum , List1_Child.ChildID, List1_Child.ListID , List1_Child.StyleID , List1_Child.MasterID, List1_Child.Title, List1_Child.SmallDetails, List1_Child.SmallImage, List1_Child.BigImage, List1_Child.ImageAltText from dbo.List1_Child where List1_Child.ListID=@ListID and List1_Child.Status=1 "

        '  "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 4; Select * from ( select  ROW_NUMBER()   over(ORDER BY List1_Child.ChildID DESC) AS RowNum , ChildID, ListID , StyleID , MasterID, Title, SmallDetails, SmallImage, BigImage, ImageAltText from List1_Child where ListID=@ListID "
        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        Dim reader As SqlDataReader = cmd.ExecuteReader()

        While reader.Read
            NW = NW & "<li><div class=""row""><div class=""col-md-6 col-sm-6""><div class=""productPreviewSliderContainer""><div class=""productPreviewSlider""><ul class=""slides"">"
            Dim galimage As String = Gallery(reader("MasterID"))
            If galimage <> "" Then
                NW += galimage & "</ul></div></div></div>"
            Else
                NW += "<li><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""" width=""433""></div></li></ul></div></div></div>"
            End If
            '<a  href=""" & domainName & "Product/" & reader("ChildID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """></a>
            NW += "<div class=""col-md-6 col-sm-6""><h2>" & reader("Title").ToString() & "</h2>"
            If IsDBNull(reader("SmallDetails")) = False Then
                NW += "<p>" & reader("SmallDetails").ToString() & "</p>"
            End If
            '<h3>Style: " & GetStylename(reader("StyleID").ToString()) & "</h3><p>" & reader("SmallDetails").ToString() & "</p>
            'NW += "<a class=""readMoreBttn"" href=""" & domainName & "Product/" & reader("ChildID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
            'NW += "explore options <span><img src=""" & domainName & "ui/media/dist/icons/readmore-white-arrow.png"" /></span></a>"
            NW += "</div></div>" & Utility.showEditButton(Request, domainName & "Admin/A-List1/List1ChildEdit.aspx?l1cid=" & reader("ChildID").ToString()) & "</li>"
        End While
        cn.Close()
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination right"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
    Public Function Gallery(ByVal masterid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from CommonGallery where TableMasterID=@TableMasterID and TableName=@TableName"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int, 32).Value = masterid
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 100).Value = "List1_Child"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" width=""433""></div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function GetStylename(ByVal id As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from List where ListID=@ListID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += reader("Title").ToString()
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function socialShare(ByVal Sharetitle As String, ByVal ShareDesc As String, ByVal ShareImage As String) As String
        Dim M As String = ""

        Dim title As String = Utility.EncodeTitle(Sharetitle, "-")
        Dim imgUrl As String = domainName & "Admin/" & ShareImage
        Dim desc As String = ShareDesc
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk
        Dim linkinurl As String = "http://www.linkedin.com/shareArticle?mini=true&url=" & lnk & "& title=" & title & "&summary=" & desc
        Dim fblikecode As String = "http://www.facebook.com/plugins/like.php?href=" & lnk & "&width&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=1567479410148486"



        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIcons"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & domainName & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & domainName & "ui/media/dist/icons/tweet-iner.png""> </a>"

        M += "  </li>"


        'M += "<li>"
        'M += " <a href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & linkinurl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=600,height=400')"" > "
        'M += "<img src=""" & domainName & "ui/media/dist/icons/linkdib-share.png"" alt="""">"
        'M += " </a>"

        'M += " </li>"

        M += "<li><div class=""fb-like"" data-href=""" & lnk & """ data-layout=""button_count"" data-action=""like"" data-show-faces=""false"" data-share=""false""></div></li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & domainName & "ui/media/dist/icons/gplus-count.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function
End Class
