﻿
Partial Class Thanks
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Page.RouteData.Values("st") = "0" Then
                lblMsg.Text = "Sorry! You already have Subscribed."
            Else
                lblMsg.Text = "We've received your message, a representative will be in touch within 24 hours."
            End If
        End If
    End Sub
End Class
