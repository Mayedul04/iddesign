﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Bread Crumbs Section -->
    <section class="breadCrumbsContainer">
        <div class="container">

            <ul class="breadCrumbList">
                <li><a href='<%= domainName %>'>Home</a></li>
                <li><a href="javascript:;">> </a></li>
                <li><a href="javascript:;">Search Result</a></li>
            </ul>

        </div>
    </section>
    <!-- Bread Crumbs Section -->


    <!-- Inner Banner Section -->
    <section class="innerBannerSection">
        <div class="container">
            <h2>Search Result</h2>
        </div>
    </section>
    <!-- Inner Banner Section -->


    <!-- Inner Main Wrapper Section -->
    <section class="container">

        <section class="innerMainContentWrapper">

            <div class="row">

                <div class="col-md-12">

                    <h3 class="borderedTitle">
                        <asp:Literal ID="ltrTitle" runat="server" Text="Search"></asp:Literal>

                    </h3>
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                        <asp:TextBox ID="txtSearch" type="search" runat="server" placeholder="Search..." ValidationGroup="Search" CssClass="form-control"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" CssClass="searchButton" ValidationGroup="Search" Text="Search"></asp:Button>
                    </asp:Panel>
                    <!-- Text Listing Section -->
                   

                        <asp:Literal ID="lblData" runat="server"></asp:Literal>


                   
                    <!-- Text Listing Section -->

                </div>

            </div>

            <!-- Social and Pagination -->
            <div class="row marginTop30">

                <div class="col-md-6 col-sm-6">
                </div>

                <div class="col-md-6 col-sm-6">

                    <!-- Pagination -->
                    <asp:Literal ID="lbPagingBottom" runat="server"></asp:Literal>
                    <!-- Pagination -->

                </div>

            </div>
            <!-- Social and Pagination -->

        </section>



    </section>
    <!-- Inner Main Wrapper Section -->
</asp:Content>

